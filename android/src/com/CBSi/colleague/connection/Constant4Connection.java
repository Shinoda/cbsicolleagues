package com.CBSi.colleague.connection;

public class Constant4Connection {
//	public static final String CBSi_ProductionEnvServerURL = "http://10.19.12.47:8080/CBSiCContactWebService/";
	public static final String CBSi_ProductionEnvServerURL = "http://192.168.12.148:7070/CBSiCContactWebService/";
//	public static final String CBSi_ProductionEnvServerURL = "http://123.56.131.79/CBSiCContactWebService/";
	public static final String CBSi_DevelopmentHomeEnvServerURL = "http://192.168.1.106:8080/CBSiCContactWebService/";
	public static final String CBSi_ProductionHomeEnvServerURL = "http://192.168.1.119:8080/CBSiCContactWebService/";
	public static final String CBSi_TestEnvServerURL = "http://10.19.12.82:8080/CBSiCContactWebService/";
	
	public static final String CBSi_ServerURL = CBSi_ProductionEnvServerURL;
	
	/**
	 * 定义接口部分的url
	 */
	public static final String URL_SERVER_API_LOGIN = CBSi_ServerURL + "loginAction";
	public static final String URL_SERVER_API_LOADCONTACT = CBSi_ServerURL + "loadContact";
	public static final String URL_SERVER_API_LOADSECTOR = CBSi_ServerURL + "loadSector";
	public static final String URL_SERVER_API_MODIFYUSERINFO = CBSi_ServerURL + "modifyUserInfo";
	public static final String URL_SERVER_API_UPDALOAD_PORTRAIT = CBSi_ServerURL + "uploadPortrait";
	public static final String URL_SERVER_API_GET_PORTRAIT = CBSi_ServerURL + "getUserPortrait";
	public static final String URL_SERVER_API_SEND_SMS = CBSi_ServerURL + "sendsms";
	public static final String URL_SERVER_API_GET_CORPORATION = CBSi_ServerURL + "getCorporation";
}
