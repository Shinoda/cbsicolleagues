package com.CBSi.colleague.connection;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class CBSiServerHttpRequest implements Listener<String>, ErrorListener{
	private static final String tag = "CBSiServerHttpRequest";
	private RequestQueue mQueue;
	
	private HttpResponseListener responseListener;
	
	private Map<String, String> httpBodyDataParams;
	
	private static CBSiServerHttpRequest instance;
	
	public CBSiServerHttpRequest(Context context) {
		mQueue = Volley.newRequestQueue(context);
	}
	
	public static CBSiServerHttpRequest getInstance(Context context) {
		if (instance == null) {
			instance = new CBSiServerHttpRequest(context);
		}
		return instance;
	}
	
	public void postRequest(String serviceUrl, Map<String, String> params, HttpResponseListener listener) {
		httpBodyDataParams = params;
		responseListener = listener;
		mQueue.add(new PostResquest(Method.POST, serviceUrl,
				this, this));
		Log.v(tag, "CBSIHttp postRequest params:"+params);
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		responseListener.onResponseSuccess(arg0);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		responseListener.onResponseFailed(arg0);
	}
	
	public interface HttpResponseListener{
		void onResponseSuccess(String content);
		void onResponseFailed(VolleyError error);
	}
	
	// 继承类，Post里面放一些服务器需要的参数
		class PostResquest extends StringRequest {

			public PostResquest(int method, String url, Listener<String> listener,
					ErrorListener errorListener) {
				super(method, url, listener, errorListener);
			}

			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = httpBodyDataParams;
				return params;
			}
		}
}
