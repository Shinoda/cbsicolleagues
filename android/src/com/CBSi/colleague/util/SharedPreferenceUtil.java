package com.CBSi.colleague.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceUtil {

	public static void saveCorperationId(Context context, String corpid){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		Editor editor = settings.edit();
		editor.putString("corpid", corpid);
		editor.commit();
	}
	
	public static String getCorperationId(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		String corpid = settings.getString("corpid", "");
		return corpid;
	}
	
	/**
	 * 记录是否已经登录
	 * @param context
	 * @param isLogin
	 */
	public static void saveLoginSuccess(Context context, boolean isLogin){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		Editor editor = settings.edit();
		editor.putBoolean("isLogin", isLogin);
		editor.commit();
	}
	
	/**
	 * 获取是否已经登陆过
	 * @param context
	 * @return
	 */
	public static boolean isLoginSuccess(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		boolean isLogin = settings.getBoolean("isLogin", false);
		return isLogin;
	}
	
	/**
	 * 保存用户userid
	 * @param context
	 * @param isLogin
	 */
	public static void saveUserId(Context context, long userId){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		Editor editor = settings.edit();
		editor.putLong("userId", userId);
		editor.commit();
	}
	
	/**
	 * 获取用户userid
	 * @param context
	 * @return
	 */
	public static long getUserId(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		long userId = settings.getLong("userId", -1);
		return userId;
	}
	
	/**
	 * 是否是首次登录
	 * @param context
	 * @param isLogin
	 */
	public static void saveFirstLaunch(Context context, boolean isFirst){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		Editor editor = settings.edit();
		editor.putBoolean("isFirst", isFirst);
		editor.commit();
	}
	
	/**
	 * 获取首次登录标志
	 * @param context
	 * @return
	 */
	public static boolean isFirstLaunch(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		boolean isFirst = settings.getBoolean("isFirst", false);
		return isFirst;
	}
	
	/**
	 * 保存最近登录的用户账号和密码
	 * @param context
	 * @param corpid
	 */
	public static void saveLatestUserInfo(Context context, String userNum, String password){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		Editor editor = settings.edit();
		editor.putString("userNum", userNum);
		editor.putString("password", password);
		editor.commit();
	}
	
	/**
	 * 获取最近登录过的用户账号
	 * @param context
	 * @return
	 */
	public static String getLatestUserNum(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		String userNum = settings.getString("userNum", "");
		return userNum;
	}
	
	/**
	 * 获取最近登录过的用户密码
	 * @param context
	 * @return
	 */
	public static String getLatestUserPwd(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		String password = settings.getString("password", "");
		return password;
	}
	
	/**
	 * 保存公司名称
	 * @param context
	 * @param corpid
	 */
	public static void saveCorperationName(Context context, String corpName){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		Editor editor = settings.edit();
		editor.putString("corpName", corpName);
		editor.commit();
	}
	
	/**
	 * 获取公司名称
	 * @param context
	 * @return
	 */
	public static String getCorperationName(Context context){
		SharedPreferences settings = context.getSharedPreferences("colleague", 0);
		String corpName = settings.getString("corpName", "");
		return corpName;
	}
}
