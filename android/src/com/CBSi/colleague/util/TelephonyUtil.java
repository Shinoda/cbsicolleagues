package com.CBSi.colleague.util;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

public class TelephonyUtil {

	/**
	 * 打电话
	 * 
	 * @param mContext
	 * @param phoneNumber
	 */
	public static void call(Context mContext, String phoneNumber) {
		if (TextUtils.isEmpty(phoneNumber)) {
			return;
		}
		Uri callToUri = Uri.parse("tel:" + phoneNumber);
		Intent intent = new Intent(Intent.ACTION_CALL, callToUri);
		mContext.startActivity(intent);
	}

	/**
	 * 发短信
	 * 
	 * @param mContext
	 * @param phoneNumber
	 */
	public static void sendSmsMessage(Context mContext, String phoneNumber) {
		if (TextUtils.isEmpty(phoneNumber)) {
			return;
		}
		Uri smsToUri = Uri.parse("smsto:" + phoneNumber);
		Intent intent = new Intent(Intent.ACTION_SENDTO,
				smsToUri);
		mContext.startActivity(intent);
	}

	/**
	 * 发邮件
	 * 无附件的发送 
	 * @param mContext
	 * @param emailAddress
	 */
	public static void sendMail(Context mContext, String emailAddress){
		if(TextUtils.isEmpty(emailAddress)){
			return;
		}
		Uri mailToUri = Uri.parse("mailto:" + emailAddress);
		Intent intent = new Intent(Intent.ACTION_SENDTO,
				mailToUri);
		mContext.startActivity(intent);
	}
	
	/**
	 * 发送邮件给多人
	 * @param mContext
	 * @param emailAddress //接收者
	 * @param ccAddress    //抄送
	 * @param bccAddress   //秘送
	 */
	public static void sendMailMul(Context mContext, List<String> emailAddressList, List<String> ccAddressList, List<String> bccAddressList){
		Uri mailToUri = Uri.parse("mailto:");
		Intent intent = new Intent(Intent.ACTION_SENDTO, mailToUri);
		if(emailAddressList != null && emailAddressList.size() > 0){
			String[] emailAddressArray = emailAddressList.toArray(new String[emailAddressList.size()]);  
			intent.putExtra(Intent.EXTRA_EMAIL, emailAddressArray);
		}
		if(ccAddressList != null && ccAddressList.size() > 0){
			String[] ccAddressArray = ccAddressList.toArray(new String[ccAddressList.size()]);  
			intent.putExtra(Intent.EXTRA_CC, ccAddressArray); 
		} 
		if(bccAddressList != null && bccAddressList.size() > 0){
			String[] bccAddressArray = bccAddressList.toArray(new String[bccAddressList.size()]);  
			intent.putExtra(Intent.EXTRA_BCC, bccAddressArray); 
		}
		mContext.startActivity(intent);
	}
}
