package com.CBSi.colleague.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.CBSi.colleague.common.CommonDefination;
import com.CBSi.colleague.connection.CBSiServerHttpRequest;
import com.CBSi.colleague.connection.CBSiServerHttpRequest.HttpResponseListener;
import com.CBSi.colleague.connection.Constant4Connection;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.data.SectorDataModel;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.db.ContactDao;
import com.CBSi.colleague.db.SectorDao;
import com.android.volley.VolleyError;

/**
 * Desc: This class handle image(Bitmap), and used for uploading portrait of
 * user.
 * 
 * @author: Hequn
 * @Date: 2015年1月13日 上午11:34:03
 */
@SuppressLint("UseValueOf")
public class ImageHandleUtil {

	private static String tag = "ImageHandleUtil";
	private static final float SCALE_SIZE_TO_WIDTH = 187.0f;
	private static final float SCALE_SIZE_TO_HEIGHT = 216.0f;
	private static final int SCALE_MAX_FILE_LENGTH = 100 * 1024;// 100k

	public static Bitmap scaleToLittleSize(Bitmap image) {
		return scaleToSize(image, SCALE_SIZE_TO_WIDTH, SCALE_SIZE_TO_HEIGHT);
	}

	public static Bitmap scaleToSize(Bitmap image, float toWidth, float toHeight) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		if (baos.toByteArray().length / 1024 > 1024) {// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, 50, baos);// 这里压缩50%，把压缩后的数据存放到baos中
			Log.v(tag, "The image is too large, length is more than 1M!");
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > toWidth) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / toWidth);
		} else if (w < h && h > toHeight) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / toHeight);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		isBm = new ByteArrayInputStream(baos.toByteArray());
		bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
		return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
	}

	public static Bitmap compressImage(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int options = 100;
		while (baos.toByteArray().length > SCALE_MAX_FILE_LENGTH) { // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
			options -= 10;// 每次都减少10
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
		return bitmap;
	}

	public static String defaultUserPortraitPath(Context context) {
		File environment = null;

		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			environment = Environment.getExternalStorageDirectory();
		} else {
			environment = context.getFilesDir();
		}

		File portraitPath = new File(environment,
				CommonDefination.CBSI_CLG_PORTRAIT_PATH);

		if (!portraitPath.exists()) {
			Log.i(tag, " CBSiColleague-Path is not exists");
			boolean bOk = portraitPath.mkdirs();
			if (bOk)
				Log.i(tag, "create CBSiColleague-Path success");
			else
				Log.w(tag, "CBSiColleague-Path fail");
		} else
			Log.i(tag, "Get portraitDirectory successed!");

		return portraitPath.getPath();
	}

	public static void loadPortraitWithUserID(final Context context,
			final long userID, final FetchPortraitCallback fetchPortraitCallback) {

		String getPortraitUrl = Constant4Connection.URL_SERVER_API_GET_PORTRAIT;
		Map<String, String> params = new HashMap<String, String>();

		params.put("userid", "" + userID);
		Log.v(tag, "postRequest userID:" + userID);
		CBSiServerHttpRequest.getInstance(context).postRequest(getPortraitUrl,
				params, new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {

						JSONObject jsonResponse;
						try {
							jsonResponse = new JSONObject(content);
							String stateCode = jsonResponse
									.getString("getUserPortrait");
							String responseUserId = jsonResponse
									.getString("userid");
							Log.v(tag, "Get portrait Result:" + stateCode
									+ " userID:" + responseUserId);
							if (Integer.parseInt(stateCode) == 0) {
								String imgContent = jsonResponse
										.getString("image");
								Bitmap bmp = generateBitmap(context, imgContent);
								if (bmp != null) {
									ImageHandleUtil
											.savePortraitFile(context, Long
													.parseLong(responseUserId),
													bmp);
								}
								fetchPortraitCallback.onGetPortraitSuccessed(
										Long.parseLong(responseUserId), bmp);
							} else {
								fetchPortraitCallback.onGetPortraitFailed(
										Long.parseLong(responseUserId),
										"Server response error.");
							}
						} catch (JSONException e) {
							Log.v(tag,
									"Get portrait image Error: parser json Error:");
							e.printStackTrace();
							fetchPortraitCallback.onGetPortraitFailed(-1,
									e.getMessage());
						}
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Log.v(tag, "Get portrait from server failed!");
						Toast.makeText(context, "网络请求失败，请检查网络！", (int) 2)
								.show();
						fetchPortraitCallback.onGetPortraitFailed(-1,
								error.getMessage());
					}
				});
	}

	public static Bitmap generateBitmap(Context context, String imgContent) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(imgContent, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bitmap;
	}

	public static Bitmap loadLocalBitmap(Context context, long userID) {
		Bitmap bmp = null;
		Uri uri = null;
		try {
			String portraitPath = getPortraitPathViaUserID(context, userID);
			if (portraitPath == null) {
				return null;
			}
			uri = Uri.fromFile(new File(portraitPath, "head.jpg"));

			// 读取uri所在的图片
			Bitmap bitmap = MediaStore.Images.Media.getBitmap(
					context.getContentResolver(), uri);
			bmp = bitmap;
		} catch (Exception e) {
			Log.e(tag, e.getMessage());
			Log.e(tag, "头像出错，完整路径：" + uri);
			e.printStackTrace();
			return null;
		}

		return bmp;
	}

	public static String getPortraitPathViaUserID(Context context, long userId) {
		UserDataModel userDM = ContactDao.getInstance(context)
				.findUserByUserId(userId);
		if (userDM == null) {
			return null;
		}
		SectorDataModel sector = SectorDao.getInstance(context)
				.findSectorBySectorId(userDM.getSector_id());
		String portraitPath = defaultUserPortraitPath(context);
		if (portraitPath != null && sector != null) {
			portraitPath += "/" + sector.getCorporation_id() + "/"
					+ userDM.getSector_id() + "/" + userDM.getUser_id() + "/";
			File pFilePath = new File(portraitPath);
			if (!pFilePath.exists()) {
				boolean bOk = pFilePath.mkdirs();
				if (bOk)
					Log.i(tag, "create CBSiColleague-Path success");
				else
					Log.w(tag, "CBSiColleague-Path fail");
			}
		}
		return portraitPath;
	}

	public static boolean savePortraitFile(Context context, long userId,
			Bitmap portraitBmp) {
		String portraitFileName = getPortraitPathViaUserID(context, userId)
				+ "/head.jpg";
		boolean isSuccessed = false;
		File f = new File(portraitFileName);
		if (f.exists()) {
			f.delete();
		}
		try {
			FileOutputStream out = new FileOutputStream(f);
			portraitBmp.compress(Bitmap.CompressFormat.PNG, 100, out);
			out.flush();
			out.close();
			Log.i(tag, "Save success!");
			isSuccessed = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			isSuccessed = false;
		} catch (IOException e) {
			e.printStackTrace();
			isSuccessed = false;
		}
		return isSuccessed;
	}

	public interface FetchPortraitCallback {

		void onGetPortraitSuccessed(long responseUserId, Bitmap bmp);

		void onGetPortraitFailed(long responseUserId, String error);
	}

	public static void getPortraitFromServer(final Context context,
			long userID, final FetchPortraitCallback fetchPortraitCallback) {
		String getPortraitUrl = Constant4Connection.URL_SERVER_API_GET_PORTRAIT;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("userid", "" + userID));

		HttpPost httpRequest = new HttpPost(getPortraitUrl);
		// Post运作传送变数必须用NameValuePair[]阵列储存
		// 传参数 服务端获取的方法为request.getParameter("name")
		// List <NameValuePair> params=new ArrayList<NameValuePair>();
		// params.add(new BasicNameValuePair("name","this is post"));
		try {
			// 发出HTTP request
			httpRequest.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			// 取得HTTP response
			HttpResponse httpResponse = new DefaultHttpClient()
					.execute(httpRequest);
			Log.v(tag, "Start getPortrait userID:" + userID);
			// 若状态码为200 ok
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				// 取出回应字串
				String content = EntityUtils.toString(httpResponse.getEntity());
				JSONObject jsonResponse;
				try {
					jsonResponse = new JSONObject(content);
					String stateCode = jsonResponse
							.getString("getUserPortrait");
					String responseUserId = jsonResponse.getString("userid");
					Log.v(tag, "Get portrait Result:" + stateCode + " userID:"
							+ responseUserId);
					if (Integer.parseInt(stateCode) == 0) {
						String imgContent = jsonResponse.getString("image");
						Bitmap bmp = generateBitmap(context, imgContent);
						if (bmp != null) {
							ImageHandleUtil.savePortraitFile(context,
									Long.parseLong(responseUserId), bmp);
						}
						fetchPortraitCallback.onGetPortraitSuccessed(
								Long.parseLong(responseUserId), bmp);
					} else {
						fetchPortraitCallback.onGetPortraitFailed(
								Long.parseLong(responseUserId),
								"Server response error.");
					}
				} catch (JSONException e) {
					Log.v(tag, "Get portrait image Error: parser json Error:");
					e.printStackTrace();
					fetchPortraitCallback.onGetPortraitFailed(-1,
							e.getMessage());
				}

			} else {
				Log.v(tag, "Get portrait image StateCode:"
						+ httpResponse.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fetchPortraitCallback.onGetPortraitFailed(-10, e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			fetchPortraitCallback.onGetPortraitFailed(-11, e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fetchPortraitCallback.onGetPortraitFailed(-12, e.getMessage());
		}

	}
}
