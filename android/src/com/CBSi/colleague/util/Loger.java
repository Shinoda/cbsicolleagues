package com.CBSi.colleague.util;

import android.util.Log;

public class Loger {

  private static boolean isClose = false;

  public static void i(String tag, String msg) {
    if (!isClose) {
      Log.i(tag, msg);
    }
  }

  public static void d(String tag, String msg) {
    if (!isClose) {
      Log.d(tag, msg);
    }
  }
  
  public static void d(String tag, String msg, Exception e) {
    if (!isClose) {
      Log.d(tag, msg, e);
    }
  }
  
  public static void v(String tag, String msg) {
    if (!isClose) {
      Log.v(tag, msg);
    }
  }
  
  public static void w(String tag, String msg) {
    if (!isClose) {
      Log.w(tag, msg);
    }
  }
  
  public static void e(String tag, String msg) {
    if (!isClose) {
      Log.e(tag, msg);
    }
  }
  
  public static void e(String tag, String msg, Exception e) {
    if (!isClose) {
      Log.e(tag, msg, e);
    }
  }
  
}
