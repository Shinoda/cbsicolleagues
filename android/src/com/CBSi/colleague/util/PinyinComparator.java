package com.CBSi.colleague.util;

import java.util.Comparator;

import com.CBSi.colleague.data.UserDataModel;


public class PinyinComparator implements Comparator<UserDataModel> {

	public int compare(UserDataModel o1, UserDataModel o2) {
		if (o1.getSortKey().equals("@")
				|| o2.getSortKey().equals("#")) {
			return -1;
		} else if (o1.getSortKey().equals("#")
				|| o2.getSortKey().equals("@")) {
			return 1;
		} else {
			return o1.getSortKey().compareTo(o2.getSortKey());
		}
	}

}
