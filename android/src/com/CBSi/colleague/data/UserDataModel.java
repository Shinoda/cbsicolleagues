package com.CBSi.colleague.data;

import java.io.Serializable;

public class UserDataModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6975447884560517355L;
	
	private long user_id;
	private String user_name;
	private String user_position;
	private String user_phone;
	private String user_email;
	private String user_tel;
	private String user_tel_sub;
	private long sector_id;
	private String priority_sector;
	private String db_createtime;
	private String db_updatetime;
	private Integer db_delete;
	
//	private int fakePortraitResId;	//----头像资源Id
	private String department = ""; // ----部门


	
	private String sortKey; // ----排序的关键字
	private String lookUpKey; // ----联系人检索关键字
	private String pinyin;
	
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_position() {
		return user_position;
	}
	public void setUser_position(String user_position) {
		this.user_position = user_position;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_tel() {
		return user_tel;
	}
	public void setUser_tel(String user_tel) {
		this.user_tel = user_tel;
	}
	public String getUser_tel_sub() {
		return user_tel_sub;
	}
	public void setUser_tel_sub(String user_tel_sub) {
		this.user_tel_sub = user_tel_sub;
	}
	public long getSector_id() {
		return sector_id;
	}
	public void setSector_id(long sector_id) {
		this.sector_id = sector_id;
	}
	public String getPriority_sector() {
		return priority_sector;
	}
	public void setPriority_sector(String priority_sector) {
		this.priority_sector = priority_sector;
	}
	public String getDb_createtime() {
		return db_createtime;
	}
	public void setDb_createtime(String db_createtime) {
		this.db_createtime = db_createtime;
	}
	public String getDb_updatetime() {
		return db_updatetime;
	}
	public void setDb_updatetime(String db_updatetime) {
		this.db_updatetime = db_updatetime;
	}
	public Integer getDb_delete() {
		return db_delete;
	}
	public void setDb_delete(Integer db_delete) {
		this.db_delete = db_delete;
	}
//	public int getFakePortraitResId() {
//		return fakePortraitResId;
//	}
//	public void setFakePortraitResId(int fakePortraitResId) {
//		this.fakePortraitResId = fakePortraitResId;
//	}
	public String getSortKey() {
		return sortKey;
	}
	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}
	public String getLookUpKey() {
		return lookUpKey;
	}
	public void setLookUpKey(String lookUpKey) {
		this.lookUpKey = lookUpKey;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	
}
