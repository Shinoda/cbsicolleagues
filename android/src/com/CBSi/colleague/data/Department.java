package com.CBSi.colleague.data;

import java.io.Serializable;

public class Department implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2335324878525078005L;
	private int department_id;
	private String department;
	private int person_count;
	private String description;
	public int getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public int getPerson_count() {
		return person_count;
	}
	public void setPerson_count(int person_count) {
		this.person_count = person_count;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
