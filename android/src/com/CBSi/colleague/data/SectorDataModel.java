package com.CBSi.colleague.data;

import java.io.Serializable;

public class SectorDataModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4006384721771275945L;
	
	private long sector_id;
	private String sector_name;
	private int corporation_id;
	private long parent_sector_id;
	private String db_createtime;
	private String db_updatetime;
	private Integer db_delete;
	
	public long getSector_id() {
		return sector_id;
	}
	public void setSector_id(long sector_id) {
		this.sector_id = sector_id;
	}
	public String getSector_name() {
		return sector_name;
	}
	public void setSector_name(String sector_name) {
		this.sector_name = sector_name;
	}
	public int getCorporation_id() {
		return corporation_id;
	}
	public void setCorporation_id(int corporation_id) {
		this.corporation_id = corporation_id;
	}
	public long getParent_sector_id() {
		return parent_sector_id;
	}
	public void setParent_sector_id(long parent_sector_id) {
		this.parent_sector_id = parent_sector_id;
	}
	public String getDb_createtime() {
		return db_createtime;
	}
	public void setDb_createtime(String db_createtime) {
		this.db_createtime = db_createtime;
	}
	public String getDb_updatetime() {
		return db_updatetime;
	}
	public void setDb_updatetime(String db_updatetime) {
		this.db_updatetime = db_updatetime;
	}
	public Integer getDb_delete() {
		return db_delete;
	}
	public void setDb_delete(Integer db_delete) {
		this.db_delete = db_delete;
	}
	
	
}
