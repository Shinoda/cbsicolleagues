package com.CBSi.colleague.data;

import java.io.Serializable;

import android.R.integer;

/*	Data Sample:
 * 
 "id"						:"10000001",
 "name"					:"王路",
 "department"		:"集团",
 "job_title"		:"CNET 中国区总裁",
 "cell_no"			:"15268395742",
 "extension_no"	:"1628",
 "email"				:"wang.lu@cbsi.com.cn"
 * */
public class CollegueDataModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1233945684409506653L;
	private int contact_id; // ----联系人Id
	private int department_id;
	private String name; // ----联系人姓名
	private String department; // ----部门
	private String job_title; // ----职位
	private String cell_no; // ----联系人手机号
	private String zuoji_no; // ----座机
	private String extension_no; // ----分机号
	private String email; // ----邮箱
	private String qq = "00000000"; // ----qq号
	private String weichat = "00000000"; // ----微信
	private int fakePortraitResId;	//----头像资源Id


	private String sortKey; // ----排序的关键字
	private String lookUpKey; // ----联系人检索关键字
	private String pinyin;

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeichat() {
		return weichat;
	}

	public void setWeichat(String weichat) {
		this.weichat = weichat;
	}

	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public int getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	public String getCell_no() {
		return cell_no;
	}

	public void setCell_no(String cell_no) {
		this.cell_no = cell_no;
	}

	public String getExtension_no() {
		return extension_no;
	}

	public void setExtension_no(String extension_no) {
		this.extension_no = extension_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	public String getLookUpKey() {
		return lookUpKey;
	}

	public void setLookUpKey(String lookUpKey) {
		this.lookUpKey = lookUpKey;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getZuoji_no() {
		return zuoji_no;
	}

	public void setZuoji_no(String zuoji_no) {
		this.zuoji_no = zuoji_no;
	}

	public int getFakePortraitResId() {
		return fakePortraitResId;
	}

	public void setFakePortraitResId(int fakePortraitResId) {
		this.fakePortraitResId = fakePortraitResId;
	}
}
