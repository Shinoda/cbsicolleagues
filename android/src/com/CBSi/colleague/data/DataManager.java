package com.CBSi.colleague.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.CBSi.colleague.db.ContactDao;
import com.CBSi.colleague.db.SectorDao;
import com.CBSi.colleague.util.CharacterParser;
import com.CBSi.colleague.util.PinyinComparator;

public class DataManager {

	private static ObjectMapper mapper = new ObjectMapper();

	/**
	 * 全部的部门数据
	 */
	private static List<SectorDataModel> allDepartments = null;

	/**
	 * 全部的联系人数据
	 */
	private static List<UserDataModel> allUserDataModels = null;
	private static Map<Long, UserDataModel> userMap = null;

	private static PinyinComparator pinyinComparator = new PinyinComparator();;

	public static List<SectorDataModel> getAllDepartments() {
		return allDepartments;
	}

	public static List<UserDataModel> getAllUserDataModels() {
		return allUserDataModels;
	}

	/**
	 * 根据userid从内存中获取user对象
	 * 
	 * @param userId
	 * @return
	 */
	public static UserDataModel getUserByUserId(long userId) {
		if (userMap != null) {
			return userMap.get(userId);
		}
		return null;
	}

	/**
	 * 加载联系人和部门数据到内存
	 * 
	 * @param context
	 */
	public static void loadDataFromDb(Context context) {
		loadContactDataFromDB(context);
		loadSectorDataFromDB(context);
	}

	/**
	 * 从数据库加载联系人到内存
	 * 
	 * @param context
	 */
	public static void loadContactDataFromDB(Context context) {
		ContactDao contactDao = ContactDao.getInstance(context);
		allUserDataModels = sortData(contactDao.selectPriorityContacts());
		userMap = contactDao.selectPriorityContactsMap();
	}

	/**
	 * 从数据库加载部门到内存
	 * 
	 * @param context
	 */
	public static void loadSectorDataFromDB(Context context) {
		SectorDao sectorDao = SectorDao.getInstance(context);
		allDepartments = sectorDao.getPrioritySectors();
	}

	/**
	 * 根据部门id获取联系人
	 * @param context
	 * @param departmentId
	 * @return
	 */
	public static List<UserDataModel> getUsersByDepartmentId(
			Context context, long departmentId) {
		ContactDao contactDao = ContactDao.getInstance(context);
		return sortData(contactDao.selectByDeparmentId(departmentId));
	}

	/**
	 * 保存联系人到数据库
	 * 
	 * @param jsonData
	 * @param contactDao
	 */
	public static void saveUserListToDB(String jsonData, ContactDao contactDao) {
		UserDataModel[] cDataModelsArray;

		try {
			cDataModelsArray = mapper
					.readValue(jsonData, UserDataModel[].class);
			if (cDataModelsArray != null && cDataModelsArray.length > 0) {
				for (UserDataModel cd : cDataModelsArray) {
					long index = contactDao.insert(cd);
					if (index != -1 && cd.getSector_id() > 0) {
						Log.v("DataManager",
								"save user success! id:" + cd.getUser_id());
					}
				}
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 保存部门到数据库
	 * 
	 * @param jsonData
	 * @param context
	 */
	public static void saveSectorListToDB(String jsonData, Context context) {
		ContactDao contactDao = ContactDao.getInstance(context);
		SectorDao sectorDao = SectorDao.getInstance(context);
		sectorDao.deleteAll();
		try {
			SectorDataModel[] cDataModelsArray = mapper.readValue(jsonData,
					SectorDataModel[].class);
			if (cDataModelsArray != null && cDataModelsArray.length > 0) {
				SectorDataModel itemFirst = cDataModelsArray[0];
				sectorDao.insertFirstSector(itemFirst.getCorporation_id());
				for (SectorDataModel item : cDataModelsArray) {
					long index = sectorDao.insert(item);
					if (index == -1) {
						Log.v("DataManager", "save sector failed! sector_id:"
								+ item.getSector_id());
					} else {
						Log.v("DataManager", "save sector success! sector_id:"
								+ item.getSector_id());
					}
				}
				DataManager.allDepartments = sectorDao.getPrioritySectors();
				DataManager.allUserDataModels = contactDao
						.selectPriorityContacts();
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 联系人排序
	 * 
	 * @param originalDataList
	 * @return
	 */
	@SuppressLint("DefaultLocale")
	public static List<UserDataModel> sortData(
			List<UserDataModel> originalDataList) {
		List<UserDataModel> mSortList = new ArrayList<UserDataModel>();

		CharacterParser charPinyinPaser = CharacterParser.getInstance();
		for (int i = 0; i < originalDataList.size(); i++) {
			UserDataModel sortModel = originalDataList.get(i);
			String pinyin = charPinyinPaser
					.getSelling(sortModel.getUser_name());
			String sortString = pinyin.substring(0, 1).toUpperCase();

			if (sortString.matches("[A-Z]")) {
				sortModel.setSortKey(sortString.toUpperCase());
			} else {
				sortModel.setSortKey("#");
			}

			mSortList.add(sortModel);
		}
		Collections.sort(mSortList, pinyinComparator);
		return mSortList;

	}
}
