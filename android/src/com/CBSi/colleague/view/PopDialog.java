package com.CBSi.colleague.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.CBSi.colleague.R;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.util.TelephonyUtil;

public class PopDialog extends Dialog {

	private Context mContext;
	private TextView titleTextView;
	private String[] menus;
	private UserDataModel collegue;
	private TextView phoneTextView;
	private View zuojiTextViewLayout;
	private TextView zuojiTextView;
	private TextView smsTextView;
	private TextView cancelTextView;
	private View phoneRootView;
	private View zuojiRootView;
	private View smsRootView;
	
	public PopDialog(Context context, String[] menus) {
		super(context);
		this.mContext = context;
		this.menus = menus;
	}
	
	public PopDialog(Context context, String[] menus, int theme) {
		super(context, theme);
		this.mContext = context;
		this.menus = menus;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_pop_dialog);
		initView();
		setView();
	}

	private void initView() {
		titleTextView = (TextView) findViewById(R.id.title);
		phoneTextView = (TextView) findViewById(R.id.phone_text);
		zuojiTextViewLayout = findViewById(R.id.zuoji_root_layout);
		zuojiTextView = (TextView) findViewById(R.id.zuoji_text);
		smsTextView = (TextView) findViewById(R.id.sms_text);
		cancelTextView = (TextView) findViewById(R.id.cancel);
		phoneRootView = findViewById(R.id.phone_root_layout);
		zuojiRootView = findViewById(R.id.zuoji_root_layout);
		smsRootView = findViewById(R.id.sms_root_layout);
		
		phoneRootView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				TelephonyUtil.call(mContext, collegue.getUser_phone());
				dismiss();
			}
			
		});

		zuojiRootView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				TelephonyUtil.call(mContext, collegue.getUser_tel());
				dismiss();
			}
			
		});
		
		smsRootView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				TelephonyUtil.sendSmsMessage(mContext, collegue.getUser_phone());
				dismiss();
			}
		});
		
		cancelTextView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				dismiss();
			}
		});
	}
	
	private void setView(){
		titleTextView.setText(collegue.getUser_name());
		phoneTextView.setText(menus[0]);
		if (TextUtils.isEmpty(menus[1])) {
			zuojiTextViewLayout.setVisibility(View.GONE);
		}else{
			zuojiTextViewLayout.setVisibility(View.VISIBLE);
			zuojiTextView.setText(menus[1]);
		}
		smsTextView.setText(menus[2]);
	}

	public void setCollegue(UserDataModel collegue) {
		this.collegue = collegue;
	}
	
}
