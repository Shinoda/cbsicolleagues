package com.CBSi.colleague.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.CBSi.colleague.R;

public class ErrorDialog extends Dialog {

	private Context mContext;
	private TextView titleTextView;
	private TextView messageTextView;
	private TextView okTextView;
	private String title;
	private String message;
	
	
	public ErrorDialog(Context context, String title, String message) {
		super(context);
		this.mContext = context;
		this.title = title;
		this.message = message;
	}
	
	public ErrorDialog(Context context, String title, String message, int theme) {
		super(context, theme);
		this.mContext = context;
		this.title = title;
		this.message = message;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_error_dialog);
		initView();
		setView();
	}

	private void initView() {
		titleTextView = (TextView) findViewById(R.id.title);
		messageTextView = (TextView) findViewById(R.id.message);
		okTextView = (TextView) findViewById(R.id.ok);
		okTextView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
	
	private void setView(){
		if (!TextUtils.isEmpty(title)) {
			titleTextView.setText(title);
		}
		if (!TextUtils.isEmpty(message)) {
			messageTextView.setText(message);
		}
	}

}
