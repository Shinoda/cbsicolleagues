package com.CBSi.colleague.view;

import com.CBSi.colleague.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.TextView;

public class RefreshDataViewDialog extends Dialog{

	private TextView tipTextView;
	
	public RefreshDataViewDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.setCancelable(false);
	}
	
	public RefreshDataViewDialog(Context context, int style) {
		super(context, style);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.refresh_data_view_dialog);
		initView();
	}
	
	private void initView() {
		tipTextView = (TextView)findViewById(R.id.tip_textview);
	}
	
	public void setTipContent(String tip)
	{
		tipTextView.setText(tip);
	}

	public void closeDialog (){
		setCancelable(true);
		cancel();
	}
}
