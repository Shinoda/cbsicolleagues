package com.CBSi.colleague.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.Button;

import com.CBSi.colleague.R;

public class CountDownButton extends CountDownTimer {
	public static final int TIME_COUNT_FUTURE = 60000;
	public static final int TIME_COUNT_INTERVAL = 1000;

	private Context mContext;
	private Button mButton;
	private String mOriginalText;
	private Drawable mOriginalBackground;
	private Drawable mTickBackground;
	private int mOriginalTextColor;
	private OnFinishListener mListener;

	public CountDownButton() {
		super(TIME_COUNT_FUTURE, TIME_COUNT_INTERVAL);
	}

	public CountDownButton(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);
	}

	public void init(Context context, Button button, OnFinishListener listener) {
		this.mContext = context;
		this.mButton = button;
		this.mListener = listener;
		this.mOriginalText = mButton.getText().toString();
		this.mOriginalBackground = mButton.getBackground();
		this.mTickBackground = this.mOriginalBackground;
		this.mOriginalTextColor = mButton.getCurrentTextColor();
	}

	public void setTickDrawable(Drawable tickDrawable) {
		this.mTickBackground = tickDrawable;
	}

	@Override
	public void onFinish() {
		if (mContext != null && mButton != null) {
			mButton.setText(mOriginalText);
			mButton.setTextColor(mOriginalTextColor);
			mButton.setBackgroundDrawable(mOriginalBackground);
			mButton.setClickable(true);
			mListener.onFinish();
		}
	}

	@Override
	public void onTick(long millisUntilFinished) {
		if (mContext != null && mButton != null) {
			mButton.setClickable(false);
			mButton.setBackgroundResource(R.color.count_down_background_color);
			mButton.setTextColor(mContext.getResources().getColor(R.color.count_down_text_color));
			String content = String
					.format(mContext.getResources().getString(
							R.string.count_down_text),
							millisUntilFinished / 1000);
			SpannableStringBuilder style = new SpannableStringBuilder(content);
			style.setSpan(new ForegroundColorSpan(mContext.getResources()
					.getColor(R.color.count_down_sencond_color)), 9, 11,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			mButton.setText(style);
		}
	}

	public void cancelTask() {
		this.cancel();
		mButton.setTextColor(mOriginalTextColor);
		mButton.setBackgroundDrawable(mOriginalBackground);
		mButton.setClickable(true);
	}

	public interface OnFinishListener {
		public void onFinish();
	}

}
