package com.CBSi.colleague.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.db.DbHelper.User;
import com.CBSi.colleague.util.Loger;
import com.CBSi.colleague.util.SharedPreferenceUtil;

public class ContactDao extends BaseDao {
	private final String tag = "ContactDao";

	private Context mContext;
	private static ContactDao instance = null;

	private ContactDao(Context context) {
		super(context);
		this.mContext = context;
	}

	public static ContactDao getInstance(Context context) {
		if (instance == null) {
			instance = new ContactDao(context);
		}
		return instance;
	}

	public long insert(UserDataModel contact) {
		ContentValues cv = new ContentValues();
		cv.put(DbHelper.User.FIELD_USER_ID, contact.getUser_id());
		cv.put(DbHelper.User.FIELD_SECTOR_ID, contact.getSector_id());
		cv.put(DbHelper.User.FIELD_USER_NAME, contact.getUser_name());
		cv.put(DbHelper.User.FIELD_DEPARTMENT_NAME, contact.getDepartment());
		cv.put(DbHelper.User.FIELD_USER_POSITION, contact.getUser_position());
		cv.put(DbHelper.User.FIELD_USER_PHONE, contact.getUser_phone());
		cv.put(DbHelper.User.FIELD_USER_TEL, contact.getUser_tel());
		cv.put(DbHelper.User.FIELD_USER_TEL_SUB, contact.getUser_tel_sub());
		cv.put(DbHelper.User.FIELD_USER_EMAIL, contact.getUser_email());
		cv.put(DbHelper.User.FIELD_USER_PRIORITY, contact.getPriority_sector());
		// cv.put(DbHelper.User.FIELD_QQ, contact.getQq());
		// cv.put(DbHelper.User.FIELD_WEICHAT, contact.getWeichat());

		if (!isContactExist(contact)) {
			return db.insert(DbHelper.TABLE_NAME_USER, null, cv);
		} else {
			return db.replace(DbHelper.TABLE_NAME_USER, null, cv);
		}
	}

	public long updateUser(UserDataModel contact) {
		ContentValues cv = new ContentValues();
		// cv.put(DbHelper.User.FIELD_USER_ID, contact.getUser_id());
		cv.put(DbHelper.User.FIELD_SECTOR_ID, contact.getSector_id());
		cv.put(DbHelper.User.FIELD_USER_NAME, contact.getUser_name());
		cv.put(DbHelper.User.FIELD_DEPARTMENT_NAME, contact.getDepartment());
		cv.put(DbHelper.User.FIELD_USER_POSITION, contact.getUser_position());
		cv.put(DbHelper.User.FIELD_USER_PHONE, contact.getUser_phone());
		cv.put(DbHelper.User.FIELD_USER_TEL, contact.getUser_tel());
		cv.put(DbHelper.User.FIELD_USER_TEL_SUB, contact.getUser_tel_sub());
		cv.put(DbHelper.User.FIELD_USER_EMAIL, contact.getUser_email());
		cv.put(DbHelper.User.FIELD_USER_PRIORITY, contact.getPriority_sector());

		return db.update(DbHelper.TABLE_NAME_USER, cv, "user_id=?",
				new String[] { "" + contact.getUser_id() });
	}

	public UserDataModel findUserByUserId(long userId) {
		Cursor cursor = db.rawQuery("select * from " + DbHelper.TABLE_NAME_USER
				+ " where " + User.FIELD_USER_ID + "=?", new String[] { userId
				+ "" });
		try {
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					UserDataModel contact = new UserDataModel();
					contact.setUser_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_ID)));
					contact.setSector_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.User.FIELD_SECTOR_ID)));
					contact.setUser_name(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_NAME)));
					contact.setDepartment(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_DEPARTMENT_NAME)));
					contact.setUser_position(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_POSITION)));
					contact.setUser_phone(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_PHONE)));
					contact.setUser_tel(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_TEL)));
					contact.setUser_tel_sub(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_TEL_SUB)));
					contact.setUser_email(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_EMAIL)));
					contact.setPriority_sector(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_PRIORITY)));
					return contact;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (cursor != null) {
				cursor.close();
			}
		}
		return null;
	}

	public List<UserDataModel> selectAll() {
		List<UserDataModel> list = new ArrayList<UserDataModel>();
		Cursor cursor = db.rawQuery(
				"select * from " + DbHelper.TABLE_NAME_USER, null);
		try {
			if (cursor != null) {
				while (cursor.moveToNext()) {
					UserDataModel contact = new UserDataModel();
					contact.setUser_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_ID)));
					contact.setSector_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.User.FIELD_SECTOR_ID)));
					contact.setUser_name(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_NAME)));
					contact.setDepartment(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_DEPARTMENT_NAME)));
					contact.setUser_position(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_POSITION)));
					contact.setUser_phone(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_PHONE)));
					contact.setUser_tel(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_TEL)));
					contact.setUser_tel_sub(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_TEL_SUB)));
					contact.setUser_email(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_EMAIL)));
					contact.setPriority_sector(cursor.getString(cursor
							.getColumnIndex(DbHelper.User.FIELD_USER_PRIORITY)));
					// contact.setQq(cursor.getString(cursor
					// .getColumnIndex(DbHelper.User.FIELD_QQ)));
					// contact.setWeichat(cursor.getString(cursor
					// .getColumnIndex(DbHelper.User.FIELD_WEICHAT)));
					list.add(contact);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (cursor != null) {
				cursor.close();
			}
		}
		return list;
	}

	public List<UserDataModel> selectPriorityContacts() {
		List<UserDataModel> list = new ArrayList<UserDataModel>();
		String[] priority_ids = SectorDao.getInstance(mContext)
				.getAllPriorityIds(
						ContactDao.getInstance(mContext).getSelfPriorities());
		if (priority_ids != null && priority_ids.length > 0) {
			Log.i(tag, "priority_ids.size-->" + priority_ids.length);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < priority_ids.length; i++) {
				sb.append("'" + priority_ids[i] + "',");
			}
			String sub_sql = sb.substring(0, sb.length() - 1);
			Cursor cursor = db.rawQuery(
					"select user.*,sector.sector_name from user,sector"
							+ " where user.sector_id in (" + sub_sql
							+ ") and user.sector_id=sector.sector_id", null);

			try {
				if (cursor != null) {
					while (cursor.moveToNext()) {
						UserDataModel contact = new UserDataModel();
						contact.setUser_id(cursor.getLong(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_ID)));
						contact.setSector_id(cursor.getLong(cursor
								.getColumnIndex(DbHelper.User.FIELD_SECTOR_ID)));
						contact.setUser_name(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_NAME)));
						contact.setDepartment(cursor.getString(cursor
								.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_NAME)));
						contact.setUser_position(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_POSITION)));
						contact.setUser_phone(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_PHONE)));
						contact.setUser_tel(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_TEL)));
						contact.setUser_tel_sub(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_TEL_SUB)));
						contact.setUser_email(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_EMAIL)));
						contact.setPriority_sector(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_PRIORITY)));
						// contact.setQq(cursor.getString(cursor
						// .getColumnIndex(DbHelper.User.FIELD_QQ)));
						// contact.setWeichat(cursor.getString(cursor
						// .getColumnIndex(DbHelper.User.FIELD_WEICHAT)));
						list.add(contact);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if (cursor != null) {
					cursor.close();
				}
			}
		}

		return list;
	}

	public Map<Long, UserDataModel> selectPriorityContactsMap() {
		Map<Long, UserDataModel> map = new HashMap<Long, UserDataModel>();
		String[] priority_ids = SectorDao.getInstance(mContext)
				.getAllPriorityIds(
						ContactDao.getInstance(mContext).getSelfPriorities());
		if (priority_ids != null && priority_ids.length > 0) {
			Log.i(tag, "priority_ids.size-->" + priority_ids.length);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < priority_ids.length; i++) {
				sb.append("'" + priority_ids[i] + "',");
			}
			String sub_sql = sb.substring(0, sb.length() - 1);
			Cursor cursor = db.rawQuery(
					"select user.*,sector.sector_name from user,sector"
							+ " where user.sector_id in (" + sub_sql
							+ ") and user.sector_id=sector.sector_id", null);

			try {
				if (cursor != null) {
					while (cursor.moveToNext()) {
						UserDataModel contact = new UserDataModel();
						contact.setUser_id(cursor.getLong(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_ID)));
						contact.setSector_id(cursor.getLong(cursor
								.getColumnIndex(DbHelper.User.FIELD_SECTOR_ID)));
						contact.setUser_name(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_NAME)));
						contact.setDepartment(cursor.getString(cursor
								.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_NAME)));
						contact.setUser_position(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_POSITION)));
						contact.setUser_phone(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_PHONE)));
						contact.setUser_tel(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_TEL)));
						contact.setUser_tel_sub(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_TEL_SUB)));
						contact.setUser_email(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_EMAIL)));
						contact.setPriority_sector(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_PRIORITY)));
						// contact.setQq(cursor.getString(cursor
						// .getColumnIndex(DbHelper.User.FIELD_QQ)));
						// contact.setWeichat(cursor.getString(cursor
						// .getColumnIndex(DbHelper.User.FIELD_WEICHAT)));
						map.put(contact.getUser_id(), contact);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if (cursor != null) {
					cursor.close();
				}
			}
		}

		return map;
	}

	public String[] getSelfPriorities() {
		String[] sector_ids = null;
		if (SharedPreferenceUtil.getUserId(mContext) != -1) {
			Cursor cursor = db.rawQuery("select " + User.FIELD_USER_PRIORITY
					+ " from " + DbHelper.TABLE_NAME_USER + " where "
					+ DbHelper.User.FIELD_USER_ID + "=?",
					new String[] { SharedPreferenceUtil.getUserId(mContext)
							+ "" });
			
			try {
				if (cursor != null) {
					if (cursor.moveToFirst()) {
						String priority_sector = cursor.getString(cursor
								.getColumnIndex(User.FIELD_USER_PRIORITY));
						if (!TextUtils.isEmpty(priority_sector)) {
							sector_ids = priority_sector.split(",");
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if (cursor != null) {
					cursor.close();
				}
			}
		}
		return sector_ids;
	}

	public List<UserDataModel> selectByDeparmentId(long department_id) {
		Loger.i(tag, "department_id-->" + department_id);
		List<UserDataModel> list = new ArrayList<UserDataModel>();
		String[] priority_ids = SectorDao.getInstance(mContext)
				.getAllPriorityIds(new String[] { department_id + "" });
		if (priority_ids != null && priority_ids.length > 0) {
			Log.i(tag, "priority_ids.size-->" + priority_ids.length);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < priority_ids.length; i++) {
				sb.append("'" + priority_ids[i] + "',");
			}
			String sub_sql = sb.substring(0, sb.length() - 1);
			Cursor cursor = db.rawQuery(
					"select user.*,sector.sector_name from user,sector"
							+ " where user.sector_id in (" + sub_sql
							+ ") and user.sector_id=sector.sector_id", null);
			try {
				if (cursor != null) {
					Loger.i(tag, "cursor.size-->" + cursor.getCount());
					while (cursor.moveToNext()) {
						UserDataModel contact = new UserDataModel();
						contact.setUser_id(cursor.getInt(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_ID)));
						contact.setSector_id(cursor.getInt(cursor
								.getColumnIndex(DbHelper.User.FIELD_SECTOR_ID)));
						contact.setUser_name(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_NAME)));
						contact.setDepartment(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_DEPARTMENT_NAME)));
						contact.setUser_position(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_POSITION)));
						contact.setUser_phone(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_PHONE)));
						contact.setUser_tel(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_TEL)));
						contact.setUser_tel_sub(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_TEL_SUB)));
						contact.setUser_email(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_EMAIL)));
						contact.setPriority_sector(cursor.getString(cursor
								.getColumnIndex(DbHelper.User.FIELD_USER_PRIORITY)));
						// contact.setQq(cursor.getString(cursor
						// .getColumnIndex(DbHelper.User.FIELD_QQ)));
						// contact.setWeichat(cursor.getString(cursor
						// .getColumnIndex(DbHelper.User.FIELD_WEICHAT)));
						list.add(contact);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if (cursor != null) {
					cursor.close();
				}
			}
		}
		return list;
	}

	public boolean isContactExist(UserDataModel contact) {
		boolean isExist = false;
		Cursor cursor = db.rawQuery("select * from " + DbHelper.TABLE_NAME_USER
				+ " where " + DbHelper.User.FIELD_USER_PHONE + "=?",
				new String[] { contact.getUser_phone() });
		try {
			if (cursor != null) {
				if (cursor.getCount() > 0) {
					isExist = true;
				} else {
					isExist = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if (cursor != null) {
				cursor.close();
			}
		}
		return isExist;
	}

	public void deleteAll() {
		db.delete(DbHelper.TABLE_NAME_USER, User.FIELD_USER_ID + ">-1", null);
	}
}
