package com.CBSi.colleague.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.CBSi.colleague.data.SectorDataModel;
import com.CBSi.colleague.db.DbHelper.Sector;
import com.CBSi.colleague.util.SharedPreferenceUtil;

public class SectorDao extends BaseDao {

	private final String tag = "SectorDao";

	private final long ALL_ID = -1;
	private Context mContext;
	private static SectorDao instance = null;

	public static SectorDao getInstance(Context context) {
		if (instance == null) {
			instance = new SectorDao(context);
		}
		return instance;
	}

	public SectorDao(Context context) {
		super(context);
		this.mContext = context;
	}

	public long insert(SectorDataModel sector) {
		ContentValues cv = new ContentValues();

		cv.put(DbHelper.Sector.FIELD_SECTOR_ID, sector.getSector_id());
		cv.put(DbHelper.Sector.FIELD_SECTOR_NAME, sector.getSector_name());
		cv.put(DbHelper.Sector.FIELD_CORPORATION_ID, sector.getCorporation_id());
		cv.put(DbHelper.Sector.FIELD_PARENT_SECTOR_ID,
				sector.getParent_sector_id());

		if (!isSectorExist(sector)) {
			return db.insert(DbHelper.TABLE_NAME_SECTOR, null, cv);
		} else {
			return db.replace(DbHelper.TABLE_NAME_SECTOR, null, cv);
		}
	}

	public List<SectorDataModel> selectAll() {
		List<SectorDataModel> list = new ArrayList<SectorDataModel>();
		Cursor cursor = db.rawQuery("select * from "
				+ DbHelper.TABLE_NAME_SECTOR, null);
		try {
			if (cursor != null) {
				while (cursor.moveToNext()) {
					SectorDataModel sector = new SectorDataModel();
					sector.setSector_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_ID)));
					sector.setSector_name(cursor.getString(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_NAME)));
					sector.setCorporation_id(cursor.getInt(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_CORPORATION_ID)));
					sector.setParent_sector_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_PARENT_SECTOR_ID)));
					list.add(sector);
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public SectorDataModel findSectorBySectorId(long sectorId) {
		Cursor cursor = db
				.rawQuery("select * from " + DbHelper.TABLE_NAME_SECTOR
						+ " where " + Sector.FIELD_SECTOR_ID + "=?",
						new String[] { sectorId + "" });
		try {
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					SectorDataModel sector = new SectorDataModel();
					sector.setSector_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_ID)));
					sector.setSector_name(cursor.getString(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_NAME)));
					sector.setCorporation_id(cursor.getInt(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_CORPORATION_ID)));
					sector.setParent_sector_id(cursor.getLong(cursor
							.getColumnIndex(DbHelper.Sector.FIELD_PARENT_SECTOR_ID)));
					return sector;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<SectorDataModel> getPrioritySectors() {
		List<SectorDataModel> list = new ArrayList<SectorDataModel>();
		List<SectorDataModel> sortList = new ArrayList<SectorDataModel>();
		String[] priority_ids = getAllPriorityIds(ContactDao.getInstance(
				mContext).getSelfPriorities());
		Log.i(tag, "priority_ids-->" + priority_ids);
		if (priority_ids != null && priority_ids.length > 0) {
			Log.i(tag, "priority_ids.size-->" + priority_ids.length);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < priority_ids.length; i++) {
				sb.append(priority_ids[i] + ",");
			}
			String sub_sql = sb.substring(0, sb.length() - 1);
			Log.i(tag, "sub_sql-->" + sub_sql);
			Cursor cursor = db.rawQuery("select * from "
					+ DbHelper.TABLE_NAME_SECTOR + " where "
					+ Sector.FIELD_SECTOR_ID + " in (" + sub_sql.toString()
					+ ")", null);
			try {
				if (cursor != null) {
					while (cursor.moveToNext()) {
						SectorDataModel sector = new SectorDataModel();
						sector.setSector_id(cursor.getLong(cursor
								.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_ID)));
						sector.setSector_name(cursor.getString(cursor
								.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_NAME)));
						sector.setCorporation_id(cursor.getInt(cursor
								.getColumnIndex(DbHelper.Sector.FIELD_CORPORATION_ID)));
						sector.setParent_sector_id(cursor.getLong(cursor
								.getColumnIndex(DbHelper.Sector.FIELD_PARENT_SECTOR_ID)));
						list.add(sector);
					}
					cursor.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			list =  selectAll();
		}
		
		for (int i = 0; i < list.size(); i++) {
			SectorDataModel sectorLevelOne = list.get(i);
			if (sectorLevelOne.getParent_sector_id() == 0) {
				sortList.add(sectorLevelOne);
				for (int j = 0; j < list.size(); j++) {
					SectorDataModel sectorLevelTwo = list.get(j);
					if (sectorLevelTwo.getParent_sector_id() == sectorLevelOne.getSector_id()) {
						sortList.add(sectorLevelTwo);
						for (int k = 0; k <list.size(); k++) {
							SectorDataModel sectorLevelThree = list.get(k);
							if (sectorLevelThree.getParent_sector_id() == sectorLevelTwo.getSector_id()) {
								sortList.add(sectorLevelThree);
							}
						}
					}
				}
			}
		}
		return sortList;
	}
	
	

	public String[] getAllPriorityIds(String[] priority_ids) {
		Map<String, String> all_priority_ids = new HashMap<String, String>();
		if (priority_ids != null && priority_ids.length > 0) {
			Log.i(tag, "priority_ids.size-->" + priority_ids.length);
			StringBuffer sb = new StringBuffer();
			sb.append(ALL_ID + ",");
			for (int i = 0; i < priority_ids.length; i++) {
				sb.append(priority_ids[i] + ",");
			}
			String sub_sql = sb.substring(0, sb.length() - 1);
			Log.i(tag, "sub_sql-->" + sub_sql);
			Cursor cursor = db
					.rawQuery(
							"SELECT s1.`sector_id`,s2.`sector_id`,"
									+ "s3.`sector_id` FROM sector AS s1 LEFT JOIN "
									+ "sector AS s2 ON s1.`sector_id`=s2.`parent_sector_id` LEFT JOIN sector AS s3 ON "
									+ "s2.`sector_id`=s3.`parent_sector_id` WHERE s1.`sector_id` IN ("
									+ sub_sql + ")", null);

			if (cursor != null) {
				while (cursor.moveToNext()) {
					for (int i = 0; i < cursor.getColumnCount(); i++) {
						long sector_id = cursor.getLong(i);
						Log.i(tag, "getAllPriorityIds-sector_id-->" + sector_id);
						if (sector_id != 0) {
							if (!all_priority_ids.containsKey(sector_id)) {
								all_priority_ids.put(sector_id + "", "");
							}
						}
					}
				}
				cursor.close();
			}
		} else {
			Cursor cursor = db.rawQuery("select " + Sector.FIELD_SECTOR_ID
					+ " from " + DbHelper.TABLE_NAME_SECTOR, null);
			if (cursor != null) {
				while (cursor.moveToNext()) {
					for (int i = 0; i < cursor.getColumnCount(); i++) {
						long sector_id = cursor.getLong(i);
						Log.i(tag, "getAllPriorityIds-sector_id-->" + sector_id);
						if (sector_id != 0) {
							if (!all_priority_ids.containsKey(sector_id)) {
								all_priority_ids.put(sector_id + "", "");
							}
						}

					}
				}
				cursor.close();
			}
		}
		return all_priority_ids.keySet().toArray(new String[0]);
	}

	public SectorDataModel getSectorDataModelById(long id) {
		Cursor cursor = db.rawQuery("select * from "
				+ DbHelper.TABLE_NAME_SECTOR + " where "
				+ DbHelper.Sector.FIELD_SECTOR_ID + "=?", new String[] { id
				+ "" });
		Log.i(tag, "cursor-->" + cursor);
		if (cursor != null) {
			Log.i(tag, "cursor.length-->" + cursor.getCount());
			if (cursor.moveToFirst()) {
				SectorDataModel sector = new SectorDataModel();
				sector.setSector_id(cursor.getLong(cursor
						.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_ID)));
				sector.setSector_name(cursor.getString(cursor
						.getColumnIndex(DbHelper.Sector.FIELD_SECTOR_NAME)));
				sector.setCorporation_id(cursor.getInt(cursor
						.getColumnIndex(DbHelper.Sector.FIELD_CORPORATION_ID)));
				sector.setParent_sector_id(cursor.getLong(cursor
						.getColumnIndex(DbHelper.Sector.FIELD_PARENT_SECTOR_ID)));
				cursor.close();
				return sector;
			} else {
				cursor.close();
				return null;
			}
		} else {
			return null;
		}
	}

	public boolean isSectorExist(SectorDataModel sector) {
		Cursor cursor = db.rawQuery("select * from "
				+ DbHelper.TABLE_NAME_SECTOR + " where "
				+ DbHelper.Sector.FIELD_SECTOR_ID + "=?",
				new String[] { String.valueOf(sector.getSector_id()) });
		try {
			if (cursor != null) {
				if (cursor.getCount() > 0) {
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public void insertFirstSector(int corporation_id) {
		SectorDataModel totalSector = new SectorDataModel();
		totalSector.setSector_id(ALL_ID);
		totalSector.setSector_name(SharedPreferenceUtil
				.getCorperationName(mContext));
		totalSector.setCorporation_id(corporation_id);
		insert(totalSector);
	}

	public void deleteAll() {
		db.delete(DbHelper.TABLE_NAME_SECTOR, Sector.FIELD_SECTOR_ID + ">-1",
				null);
	}
}
