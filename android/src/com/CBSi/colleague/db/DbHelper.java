package com.CBSi.colleague.db;

import com.CBSi.colleague.util.Loger;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
	private final String tag = "DbHelper";

	private final static String DATABASE_NAME = "cbsi_book.db";
	private final static int DATABASE_VERSION = 1;
	public final static String TABLE_NAME_USER = "user";
	public final static String TABLE_NAME_SECTOR = "sector";

	// table User fields
	public static class User {
		public final static String FIELD_USER_ID = "user_id";
		public final static String FIELD_SECTOR_ID = "sector_id";
		public final static String FIELD_USER_NAME = "user_name";
		public final static String FIELD_DEPARTMENT_NAME = "department";
		public final static String FIELD_USER_POSITION = "user_position";
		public final static String FIELD_USER_PHONE = "user_phone";
		public final static String FIELD_USER_TEL = "user_tel";
		public final static String FIELD_USER_TEL_SUB = "user_tel_sub";
		public final static String FIELD_USER_EMAIL = "user_email";
		public final static String FIELD_USER_PRIORITY = "priority_sector";
//		public final static String FIELD_QQ = "qq";
//		public final static String FIELD_WEICHAT = "weichat";
	}

	// table Sector fields
	public static class Sector {
		public final static String FIELD_SECTOR_ID = "sector_id";
		public final static String FIELD_SECTOR_NAME = "sector_name";
		public final static String FIELD_CORPORATION_ID = "corporation_id";
		public final static String FIELD_PARENT_SECTOR_ID = "parent_sector_id";
	}

	private static DbHelper instance = null;

	private DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static synchronized DbHelper getInstance(Context context) {
		if (instance == null) {
			instance = new DbHelper(context);
		}
		return instance;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql_contact = "create table " + TABLE_NAME_USER + "("
				+ User.FIELD_USER_ID + " integer primary key,"
				+ User.FIELD_SECTOR_ID + " integer,"
				+ User.FIELD_USER_NAME + " varchar(50) not null,"
				+ User.FIELD_DEPARTMENT_NAME + " varchar(50) not null,"
				+ User.FIELD_USER_POSITION + " varchar(100),"
				+ User.FIELD_USER_PHONE + " varchar(20),"
				+ User.FIELD_USER_TEL + " varchar(20),"
				+ User.FIELD_USER_TEL_SUB + " varchar(20),"
				+ User.FIELD_USER_EMAIL + " varchar(50),"
				+ User.FIELD_USER_PRIORITY + " varchar(50))";
		Loger.i(tag, "sql_contact -->"+sql_contact);
		db.execSQL(sql_contact);
		
		String sql_department = "create table " + TABLE_NAME_SECTOR + "("
				+ Sector.FIELD_SECTOR_ID + " integer primary key,"
				+ Sector.FIELD_SECTOR_NAME + " varchar(50) not null,"
				+ Sector.FIELD_CORPORATION_ID + " integer," 
				+ Sector.FIELD_PARENT_SECTOR_ID + " integer)";
		db.execSQL(sql_department);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = " DROP TABLE IF EXISTS " + TABLE_NAME_USER;
		db.execSQL(sql);
		onCreate(db);
	}

}
