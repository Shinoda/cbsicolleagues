package com.CBSi.colleague.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class BaseDao {
	private DbHelper dbHelper;
	public SQLiteDatabase db;
	
	public BaseDao(Context context) {
		dbHelper = DbHelper.getInstance(context);
		db = dbHelper.getWritableDatabase();
	}
	
	
}
