package com.CBSi.colleague.common;

public class CommonDefination {
	
	public  final static String COLLEAGUE_VERIFY_PHONE_NUM = "1069057059317876";
	
	public  final static String COLLEAGUE_APK_VERSION = "v1.0.0";
	public  final static String COLLEAGUE_APK_BUILD_VERSION = "build_030.201501192128";
	
	
	public  final static String COLLEAGUE_DATA_KEY = "com.cbsi.colleague.data_key";
	
	public static final String CBSI_CLG_DATA_PATH = "CBSiColleague/";
	public static final String CBSI_CLG_PORTRAIT_PATH = CBSI_CLG_DATA_PATH + "portrait/";
	
}
