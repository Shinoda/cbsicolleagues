package com.CBSi.colleague.common;

import android.content.Context;
import android.util.DisplayMetrics;

import com.CBSi.colleague.util.Loger;

public class SystemScreenInfo {
	private static final String tag = "SystemScreenInfo";
	public static int SYS_SCREEN_WIDTH;
	public static int SYS_SCREEN_HEIGHT;
	public static int CONTACT_GROUP_LABLE;
	
	public static void getSystemInfo(Context mContext){
		DisplayMetrics  dm = mContext.getResources().getDisplayMetrics();
		SYS_SCREEN_WIDTH = dm.widthPixels;
		SYS_SCREEN_HEIGHT = dm.heightPixels;
		CONTACT_GROUP_LABLE = SYS_SCREEN_WIDTH / 5 * 2;
	}
}
