package com.CBSi.colleague.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.CBSi.colleague.R;
import com.CBSi.colleague.activities.ContactDetailActivity;
import com.CBSi.colleague.activities.ContactListActivity;
import com.CBSi.colleague.common.CommonDefination;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.util.CharacterDataUtil;
import com.CBSi.colleague.util.ImageDownLoader;
import com.CBSi.colleague.util.ImageDownLoader.onImageLoaderListener;
import com.CBSi.colleague.util.ImageHandleUtil;
import com.CBSi.colleague.view.QuickAlphabeticBar;

public class ContactHomeAdapter extends BaseAdapter implements OnScrollListener{

	private static final String tag = "ContactHomeAdapter";
	private LayoutInflater inflater;
	private List<UserDataModel> list;
	private HashMap<String, Integer> alphaIndexer;
	private String[] sections; 
	private Context ctx;
	private ListView mListView;
	/**
	 * 一屏中第一个item的位置
	 */
	private int iFirstPosition;
	/**
	 * 一屏中所有item的个数
	 */
	private int iItemsShownCount;
	
	/**
	 * Image 下载器
	 */
	private ImageDownLoader mImageDownLoader;
	
	private boolean isFirstEnter = true;

	public ContactHomeAdapter(Context context, List<UserDataModel> list,
			QuickAlphabeticBar alpha, ListView listV) {

		this.ctx = context;
		this.inflater = LayoutInflater.from(context);
		this.list = list;
		this.mListView = listV;
		mListView.setOnScrollListener(this);
		mImageDownLoader = new ImageDownLoader(context);
		
		this.alphaIndexer = new HashMap<String, Integer>();
		this.sections = new String[list.size()];

		for (int i = 0; i < list.size(); i++) {
			String name = CharacterDataUtil.getAlphaBet(list.get(i).getSortKey());
			if (!alphaIndexer.containsKey(name)) {
				alphaIndexer.put(name, i);
			}
		}

		Set<String> sectionLetters = alphaIndexer.keySet();
		ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
		Collections.sort(sectionList);
		sections = new String[sectionList.size()];
		sectionList.toArray(sections);

		alpha.setAlphaIndexer(alphaIndexer);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void remove(int position) {
		list.remove(position);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.contact_home_list_item,
					null);
			
			holder = new ViewHolder();
			holder.qcb = (ImageView) convertView.findViewById(R.id.qcb);
			holder.qcbBG = (ImageView) convertView.findViewById(R.id.portrait_border);
			holder.alphabetTextView = (TextView) convertView.findViewById(R.id.alpha);
			Typeface tf = Typeface.createFromAsset(ctx.getResources().getAssets(),
		              "fonts/DroidSans.ttf");
			holder.alphabetTextView.setTypeface(tf);
			
			holder.contactNameTextView = (TextView) convertView.findViewById(R.id.name);
			Typeface tfFallBack = Typeface.createFromAsset(ctx.getResources().getAssets(),
		              "fonts/DroidSans.ttf");
			holder.contactNameTextView.setTypeface(tfFallBack);
			holder.positionTextView = (TextView) convertView
					.findViewById(R.id.number);
			holder.detailButton = (Button) convertView
					.findViewById(R.id.detailBtn);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if(position%2 == 0)
		{
			convertView.setBackgroundResource(R.drawable.contact_list_item_bg_dark_selector);
			holder.qcbBG.setImageResource(R.drawable.contact_list_item_portrait_dark_selector);
		}
		else {
			convertView.setBackgroundResource(R.drawable.contact_list_item_bg_light_selector);
			holder.qcbBG.setImageResource(R.drawable.contact_list_item_portrait_light_selector);
		}
		final UserDataModel cData = list.get(position);
		String name = cData.getUser_name();
		holder.contactNameTextView.setText(name);
		holder.positionTextView.setText(cData.getUser_position());
		holder.qcb.setTag(cData.getUser_id());
		Bitmap bitmap = mImageDownLoader.showCacheBitmap(cData.getUser_id());
		if(bitmap == null){
			bitmap = BitmapFactory.decodeResource(ctx.getResources(),
					R.drawable.default_portrait);
		}
		holder.qcb.setImageBitmap(bitmap);
		holder.detailButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				ContactListActivity cListActivity = (ContactListActivity) ctx;
				Intent intent = new Intent(cListActivity,
						ContactDetailActivity.class);
				intent.putExtra(CommonDefination.COLLEAGUE_DATA_KEY, cData);
				ctx.startActivity(intent);
				cListActivity.overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);// 右往左推出效果;

			}
		});
		String currentStr = CharacterDataUtil.getAlphaBet(cData.getSortKey());
		String previewStr = (position - 1) >= 0 ? CharacterDataUtil.getAlphaBet(list.get(
				position - 1).getSortKey()) : " ";
		if (!previewStr.equals(currentStr)) { 
			holder.alphabetTextView.setVisibility(View.VISIBLE);
			holder.alphabetTextView.setText(currentStr);
		} else {
			holder.alphabetTextView.setVisibility(View.GONE);
		}
		return convertView;
	}
	
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if(scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
			showImage(iFirstPosition, iItemsShownCount);
		}else{
			cancelTask();
		}
		
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		iFirstPosition = firstVisibleItem;
		iItemsShownCount = visibleItemCount;
		// 因此在这里为首次进入程序开启下载任务。 
		if(isFirstEnter && visibleItemCount > 0){
			showImage(iFirstPosition, iItemsShownCount);
			isFirstEnter = false;
		}
	}

	private static class ViewHolder {
		ImageView qcb;
		ImageView qcbBG;
		TextView alphabetTextView;
		TextView contactNameTextView;
		TextView positionTextView;
		Button detailButton;
	}

	private void showImage(int firstVisibleItem, int visibleItemCount){
		if((firstVisibleItem + visibleItemCount) > list.size() || mImageDownLoader==null)
		{
			return;
		}
		for(int i=firstVisibleItem; i<firstVisibleItem + visibleItemCount; i++){
			final UserDataModel cData = list.get(i);
			long userId = cData.getUser_id();
			final ImageView mImageView = (ImageView) mListView.findViewWithTag(userId);
			if(mImageView == null)
			{
				continue;
			}
			Bitmap bitmap = mImageDownLoader.downloadImage(userId, new onImageLoaderListener() {
				
				@Override
				public void onImageLoader(Bitmap bitmap, long responseUserId) {
					// TODO Auto-generated method stub
					ImageView imageView = (ImageView) mListView.findViewWithTag(responseUserId);
					if(imageView != null && bitmap != null){
						Log.v(tag, "onImageLoader bitmap has get!");
						imageView.setImageBitmap(bitmap);
						notifyDataSetChanged();
					}
				}
			});
			
			if(bitmap != null){
				mImageView.setImageBitmap(bitmap);
			}else{
				Bitmap bmp = BitmapFactory.decodeResource(ctx.getResources(),
						R.drawable.default_portrait);
				mImageView.setImageBitmap(bmp);
			}
		}
	}
	
	/**
	 * 取消下载任务
	 */
	public void cancelTask(){
		mImageDownLoader.cancelTask();
	}
	
	public void updateItemWithUserId(long userId)
	{
//		final ImageView mImageView = (ImageView) mListView.findViewWithTag(userId);
		Bitmap bitmap = ImageHandleUtil.loadLocalBitmap(ctx, userId);
		mImageDownLoader.addBitmapToMemoryCache(userId, bitmap);
		notifyDataSetChanged();
	}

//	private void setPortraitImage(final ImageView portraitIV, UserDataModel userData){
//		Bitmap bmp = ImageHandleUtil.loadLocalBitmap(ctx, userData.getUser_id());
//		if(bmp != null)
//		{
//			portraitIV.setImageBitmap(bmp);
//		}
//		else
//		{
//			ImageHandleUtil.loadPortraitWithUserID(this.ctx, userData.getUser_id(), new FetchPortraitCallback() {
//		
//			
//			@Override
//			public void onGetPortraitSuccessed(Bitmap bmp) {
//				if (bmp == null) {
////					int tmp = (int) (Math.random() * 5);
//					bmp = BitmapFactory.decodeResource(ctx.getResources(),
//							R.drawable.default_portrait /*+ tmp*/);
//				}
//				portraitIV.setImageBitmap(bmp);
//			}
//			
//			@Override
//			public void onGetPortraitFailed(String error) {
//				 Bitmap bmp = BitmapFactory.decodeResource(ctx.getResources(),
//						R.drawable.default_portrait /*+ tmp*/);
//				 portraitIV.setImageBitmap(bmp);
//			}
//		});
//		}
//	}
}
