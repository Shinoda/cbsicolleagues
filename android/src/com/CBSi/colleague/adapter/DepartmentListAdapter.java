package com.CBSi.colleague.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.CBSi.colleague.R;
import com.CBSi.colleague.data.SectorDataModel;
import com.CBSi.colleague.db.DbHelper.Sector;
import com.CBSi.colleague.db.SectorDao;

public class DepartmentListAdapter extends BaseAdapter {

	private final String tag = "DepartmentListAdapter";

	private Activity context;
	private int itemCount;
	private LayoutInflater listInflater;
	private boolean isPressed[];
	private int pressedId = 0;
	private List<SectorDataModel> list = new ArrayList<SectorDataModel>();
	private Handler handler;

	/* 一个menu item中包含一个imageView和一个TextView */
	private class Holder {
		public TextView menuText;
		public ImageView dividerImageView;
	}

	public DepartmentListAdapter(Activity context, Handler handler) {
		this.context = context;
		this.handler = handler;

	}

	public void setData(List<SectorDataModel> list) {
		if (list != null) {
			this.list = list;
			this.init();
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		return this.list.size();
	}

	@Override
	public Object getItem(int position) {
		return this.list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SectorDataModel sector = list.get(position);
		Holder holder;
		if (convertView == null) {
			holder = new Holder();
			convertView = this.listInflater.inflate(
					R.layout.department_list_item, null);
			holder.menuText = (TextView) convertView
					.findViewById(R.id.menuText);
			holder.dividerImageView = (ImageView) convertView
					.findViewById(R.id.menu_divider);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		if (position == 0) {
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
					context.getResources().getDimensionPixelSize(
							R.dimen.menu_list_first_item_height));
			convertView.setLayoutParams(params);

			holder.dividerImageView.setVisibility(View.VISIBLE);
		} else {
			holder.dividerImageView.setVisibility(View.GONE);
		}
		
		int level = 1;
		Log.i(tag, "00000getParent_sector_id-->"+sector.getParent_sector_id());
		if (sector.getParent_sector_id() != 0) {
			Log.i(tag, "1111111getParent_sector_id-->"+sector.getParent_sector_id());
			SectorDataModel parent1Sector = SectorDao.getInstance(context)
					.getSectorDataModelById(sector.getParent_sector_id());
			if (parent1Sector != null && parent1Sector.getParent_sector_id() != 0) {
				level = 3;
			}else{
				level = 2;
			}
		}
		
		switch (level) {
		case 1:
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
					context.getResources().getDimensionPixelSize(
							R.dimen.menu_list_first_item_height));
			convertView.setLayoutParams(params);
			holder.menuText.setText(sector.getSector_name());
			if (this.isPressed[position] == true) {
				holder.menuText.setTextColor(context.getResources().getColor(R.color.level_selected_color));
			} else {
				holder.menuText.setTextColor(context.getResources().getColor(R.color.level_one_color));
			}
			break;
		case 2:
			params = new LayoutParams(LayoutParams.MATCH_PARENT,
					context.getResources().getDimensionPixelSize(
							R.dimen.menu_list_first_sub_item_height));
			convertView.setLayoutParams(params);
			holder.menuText.setText("        " + sector.getSector_name());
			if (this.isPressed[position] == true) {
				holder.menuText.setTextColor(context.getResources().getColor(R.color.level_selected_color));
			} else {
				holder.menuText.setTextColor(context.getResources().getColor(R.color.level_two_color));
			}
			break;
		case 3:
			params = new LayoutParams(LayoutParams.MATCH_PARENT,
					context.getResources().getDimensionPixelSize(
							R.dimen.menu_list_first_sub_item_height));
			convertView.setLayoutParams(params);
			holder.menuText.setText("                   " + sector.getSector_name());
			if (this.isPressed[position] == true) {
				holder.menuText.setTextColor(context.getResources().getColor(R.color.level_selected_color));
			} else {
				holder.menuText.setTextColor(context.getResources().getColor(R.color.level_three_color));
			}
			break;
		default:
			break;
		}
		
		
		return convertView;
	}

	public void changeState(int position) {

		for (int i = 0; i < this.itemCount; i++) {
			isPressed[i] = false;
		}
		isPressed[position] = true;
		pressedId = position;
		notifyDataSetChanged();
	}

	private void init() {
		this.itemCount = this.list.size();
		if (itemCount > 0) {
			this.isPressed = new boolean[this.itemCount];
			for (int i = 0; i < this.itemCount; i++) {
				this.isPressed[i] = false;
			}
			if (pressedId < itemCount) {
				this.isPressed[pressedId] = true;
			}
		}

		this.listInflater = LayoutInflater.from(context);
	}
}
