package com.CBSi.colleague.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.CBSi.colleague.R;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.util.ImageHandleUtil;

public class SearchResultAdapter extends BaseAdapter {
	private List<UserDataModel> list = null;
	private Context mContext;
	private LayoutInflater inflater;

	public SearchResultAdapter(Context context, List<UserDataModel> list) {
		this.mContext = context;
		this.inflater = LayoutInflater.from(context);
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<UserDataModel> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	
	public void remove(int position) {
		list.remove(position);
	}

	@SuppressLint("InflateParams")
	public View getView(final int position, View convertView, ViewGroup arg2) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.search_result_list_item,
					null);
			
			holder = new ViewHolder();
			holder.avatar = (ImageView) convertView.findViewById(R.id.search_avatar);
			holder.avatarBG = (ImageView) convertView.findViewById(R.id.search_avatar_bg);
//			holder.alphabetTextView = (TextView) convertView.findViewById(R.id.search_1st_alphabet);
//			Typeface tf = Typeface.createFromAsset(mContext.getResources().getAssets(),
//		              "fonts/DroidSans.ttf");
//			holder.alphabetTextView.setTypeface(tf);
			
			holder.userNameTextView = (TextView) convertView.findViewById(R.id.search_item_name);
			Typeface tfFallBack = Typeface.createFromAsset(mContext.getResources().getAssets(),
		              "fonts/DroidSans.ttf");
			holder.userNameTextView.setTypeface(tfFallBack);
			holder.positionTV = (TextView) convertView
					.findViewById(R.id.search_item_position);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if(position%2 == 0)
		{
			convertView.setBackgroundResource(R.drawable.contact_list_item_bg_dark_selector);
			holder.avatarBG.setImageResource(R.drawable.contact_list_item_portrait_dark_selector);
		}
		else {
			convertView.setBackgroundResource(R.drawable.contact_list_item_bg_light_selector);
			holder.avatarBG.setImageResource(R.drawable.contact_list_item_portrait_light_selector);
		}
		final UserDataModel cData = list.get(position);
		String name = cData.getUser_name();
		// String number = cData.getPhoneNum();
		holder.userNameTextView.setText(name);
		// int rd = new Random().nextInt(3);
		holder.positionTV.setText(cData.getUser_position());

//		holder.avatar.setImageResource(R.drawable.default_portrait);
		Bitmap localBmp = ImageHandleUtil.loadLocalBitmap(mContext,
				cData.getUser_id());
		if (localBmp == null) {
			localBmp = BitmapFactory.decodeResource(mContext.getResources(),
					R.drawable.default_portrait);
		}
		holder.avatar.setImageBitmap(localBmp);

//		// --set detail button event;
//		convertView.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ContactListActivity cListActivity = (ContactListActivity) mContext;
//				Intent intent = new Intent(cListActivity,
//						ContactDetailActivity.class);
//				intent.putExtra(CommonDefination.COLLEAGUE_DATA_KEY, cData);
//				mContext.startActivity(intent);
//				cListActivity.overridePendingTransition(R.anim.push_left_in,
//						R.anim.push_left_out);// 右往左推出效果;
//
//			}
//		});
//		String currentStr = CharacterDataUtil.getAlphaBet(cData.getSortKey());
//		String previewStr = (position - 1) >= 0 ? CharacterDataUtil.getAlphaBet(list.get(
//				position - 1).getSortKey()) : " ";
		
//		if (!previewStr.equals(currentStr)) { 
//			holder.alphabetTextView.setVisibility(View.VISIBLE);
//			holder.alphabetTextView.setText(currentStr);
//		} else {
//			holder.alphabetTextView.setVisibility(View.GONE);
//		}
		return convertView;

	}

	private final static class ViewHolder {
		ImageView avatar;
		ImageView avatarBG;
//		TextView alphabetTextView;
		TextView userNameTextView;
		TextView positionTV;
	}

}