package com.CBSi.colleague.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.CBSi.colleague.R;
import com.CBSi.colleague.activities.ModifyPersonalInfoActivity.EModifyType;
import com.CBSi.colleague.connection.CBSiServerHttpRequest;
import com.CBSi.colleague.connection.CBSiServerHttpRequest.HttpResponseListener;
import com.CBSi.colleague.connection.Constant4Connection;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.data.SectorDataModel;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.db.ContactDao;
import com.CBSi.colleague.db.SectorDao;
import com.CBSi.colleague.util.Base64Coder;
import com.CBSi.colleague.util.GaussBlurAlgorithm;
import com.CBSi.colleague.util.ImageHandleUtil;
import com.CBSi.colleague.util.SharedPreferenceUtil;
import com.CBSi.colleague.util.ImageHandleUtil.FetchPortraitCallback;
import com.CBSi.colleague.view.RefreshDataViewDialog;
import com.CBSi.colleague.view.RoundImageView;
import com.android.volley.VolleyError;

public class PersonalProfileActivity extends Activity {

	private final String TAG = "PersonalProfileActivity";

	// private ImageView btnBackIv;
	private View backView;
	private ImageView personHeaderBg;
	private RoundImageView personPortraitImg;

	private View cellphoneView, mailView, telView, telSubView, settingView;

	private TextView /* nameTitle, sectorTitle, positionTitle, */cellphoneTitle,
			mailTitle, telTitle, telSubTitle;
	private TextView nameValue, sectorValue, positionValue, cellphoneValue,
			mailValue, telValue, telSubValue;

	private UserDataModel currentUser;

	private Button exitBtn;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.personal_account_layout);

		initView();
	}

	@SuppressLint("NewApi")
	private void initView() {

		/*
		 * nameTitle = (TextView)findViewById(R.id.personName); sectorTitle =
		 * (TextView)findViewById(R.id.personSector); positionTitle =
		 * (TextView)findViewById(R.id.personPosition);
		 */
		cellphoneTitle = (TextView) findViewById(R.id.cell_phone_title);
		mailTitle = (TextView) findViewById(R.id.mail_title);
		telTitle = (TextView) findViewById(R.id.tel_phone_title);
		telSubTitle = (TextView) findViewById(R.id.tel_sub_title);

		nameValue = (TextView) findViewById(R.id.personName);
		sectorValue = (TextView) findViewById(R.id.personSector);
		positionValue = (TextView) findViewById(R.id.personPosition);
		cellphoneValue = (TextView) findViewById(R.id.cell_phone_value);
		mailValue = (TextView) findViewById(R.id.mail_value);
		telValue = (TextView) findViewById(R.id.tel_value);
		telSubValue = (TextView) findViewById(R.id.tel_sub_value);

		loadDataInfoForView();

		personHeaderBg = (ImageView) findViewById(R.id.personHeader_bg);
		personPortraitImg = (RoundImageView) findViewById(R.id.personPortrait_photo);

		Bitmap localBmp = null;
		if (currentUser != null) {
			localBmp = ImageHandleUtil.loadLocalBitmap(this,
					currentUser.getUser_id());
		}
		// if (localBmp == null) {
		// localBmp = BitmapFactory.decodeResource(getResources(),
		// R.drawable.personal_default_portrait);
		// }
		// final Bitmap portraitBmp = localBmp;
		if (localBmp != null) {
			setPortraitView(localBmp);
		}

		if (currentUser != null) {
			ImageHandleUtil.loadPortraitWithUserID(this,
					currentUser.getUser_id(), new FetchPortraitCallback() {

						@Override
						public void onGetPortraitSuccessed(long responseUserId,
								Bitmap bmp) {
							// if (bmp == null) {
							// bmp = portraitBmp;
							// }
							if (bmp != null) {
								setPortraitView(bmp);
							}
						}

						@Override
						public void onGetPortraitFailed(long responseUserId,
								String error) {
							// setPortraitView(portraitBmp);
						}
					});
		}

		personPortraitImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (currentUser != null) {
					ShowPickDialog();
				}
			}
		});

		initBackBtn();
		initExitBtn();

		cellphoneView = findViewById(R.id.cellphone_layout);
		mailView = findViewById(R.id.email_root_layout);
		telView = findViewById(R.id.zuoji_root_layout);
		telSubView = findViewById(R.id.subtel_root_layout);
		// 暂时屏蔽掉修改手机号码的功能
		// setClickEventForView(cellphoneView, EModifyType.ModifyType_Phone,
		// (String) cellphoneTitle.getText(), InputType.TYPE_CLASS_PHONE);
		setClickEventForView(mailView, EModifyType.ModifyType_Mail,
				(String) mailTitle.getText(),
				InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		setClickEventForView(telView, EModifyType.ModifyType_Tel,
				(String) telTitle.getText(), InputType.TYPE_CLASS_PHONE);
		setClickEventForView(telSubView, EModifyType.ModifyType_TelSub,
				(String) telSubTitle.getText(), InputType.TYPE_CLASS_PHONE);

		settingView = findViewById(R.id.setting_item_layout);
		settingView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(PersonalProfileActivity.this,
						SettingActivity.class);
				startActivity(intent);
			}
		});
	}

	private void initBackBtn() {
		// btnBackIv = (ImageView) findViewById(R.id.pp_btn_back);
		// btnBackIv.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// finish();
		// overridePendingTransition(R.anim.push_right_in,
		// R.anim.push_right_out);
		// btnBackIv.setAlpha(0x80);
		// }
		// });

		backView = findViewById(R.id.back_root_layout);

		backView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}
		});
	}

	private void initExitBtn() {
		exitBtn = (Button) findViewById(R.id.exit_btn);
		exitBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				Intent intent = new Intent(PersonalProfileActivity.this,
						LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				CBSIColleagueApplication.getInstance().exit();
				SharedPreferenceUtil.saveLoginSuccess(getApplicationContext(),
						false);
			}
		});
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void setPortraitView(Bitmap photo) {

		personHeaderBg.setBackground(GaussBlurAlgorithm.BoxBlurFilter(photo));
		personHeaderBg.setAlpha(20);
		personPortraitImg.setImageBitmap(photo);
	}

	private void loadDataInfoForView() {
		DataManager.loadDataFromDb(getApplicationContext());
		currentUser = DataManager.getUserByUserId(SharedPreferenceUtil
				.getUserId(getApplicationContext()));
		Log.i(TAG,
				"DataManager.getCurrentUserID()-->"
						+ SharedPreferenceUtil
								.getUserId(getApplicationContext()));
		Log.i(TAG, "currentUser-->" + currentUser);

		if (currentUser != null) {
			Log.i(TAG, "currentUser.userid-->" + currentUser.getUser_id());
			nameValue.setText(currentUser.getUser_name());

			SectorDataModel sector = SectorDao.getInstance(
					getApplicationContext()).findSectorBySectorId(
					currentUser.getSector_id());
			sectorValue.setText(sector.getSector_name());

			positionValue.setText(currentUser.getUser_position());

			cellphoneValue.setText(currentUser.getUser_phone());

			mailValue.setText(currentUser.getUser_email());

			telValue.setText(currentUser.getUser_tel());

			telSubValue.setText(currentUser.getUser_tel_sub());
		}
	}

	private void setClickEventForView(View v, final EModifyType mType,
			final String title, final int inputType) {
		v.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				modifyInfoWithContent(title, inputType, mType);
			}
		});
	}

	private void modifyInfoWithContent(String title, int inputType,
			EModifyType mType) {
		Intent intent = new Intent(PersonalProfileActivity.this,
				ModifyPersonalInfoActivity.class);
		String[] inputContent = { title, "" + inputType, mType.toString() };
		intent.putExtra("ModifyInfo", inputContent);
		PersonalProfileActivity.this.startActivityForResult(intent, 0);
		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);// 右往左推出效果;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0) {
			if (data == null) {
				return;
			}
			loadDataInfoForView();
			// String[] mContent =
			// data.getStringArrayExtra("ModifyItemContent");
			// EModifyType type = EModifyType.valueOf(mContent[0]);
			// switch (type) {
			// case ModifyType_Mail:
			// mailValue.setText(mContent[1]);
			// break;
			// case ModifyType_Phone:
			// cellphoneValue.setText(mContent[1]);
			// break;
			// case ModifyType_Tel:
			// telValue.setText(mContent[1]);
			// break;
			// case ModifyType_TelSub:
			// telSubValue.setText(mContent[1]);
			// break;
			// default:
			// break;
			// }
		}

		switch (requestCode) {
		// 如果是直接从相册获取
		case 1:
			if (data != null)
				startPhotoZoom(data.getData());
			break;
		// 如果是调用相机拍照时
		case 2:
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/cbsiPortraitTest.jpg");
			startPhotoZoom(Uri.fromFile(temp));
			break;
		// 取得裁剪后的图片
		case 3:
			Bundle extras = null;
			if (data != null) {
				extras = data.getExtras();
			}
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				photo = ImageHandleUtil.scaleToLittleSize(photo);
				uploadPortraitToServer(photo);
			}
			break;
		default:
			break;

		}
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 187);
		intent.putExtra("outputY", 216);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 3);
	}

	/**
	 * 选择提示对话框
	 */
	private void ShowPickDialog() {
		new AlertDialog.Builder(this)
				.setTitle("设置头像...")
				.setNegativeButton("相册", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

						Intent intent = new Intent(Intent.ACTION_PICK, null);

						/**
						 * 下面这句话，与其它方式写是一样的效果，如果：
						 * intent.setData(MediaStore.Images
						 * .Media.EXTERNAL_CONTENT_URI);
						 * intent.setType(""image/*");设置数据类型
						 */
						intent.setDataAndType(
								MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
								"image/*");
						startActivityForResult(intent, 1);

					}
				})
				.setPositiveButton("拍照", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
						/**
						 * 下面这句还是老样子，调用快速拍照功能，至于为什么叫快速拍照，大家可以参考如下官方
						 * 文档，you_sdk_path/docs/guide/topics/media/camera.html
						 * 我刚看的时候因为太长就认真看，其实是错的，这个里面有用的太多了，所以大家不要认为
						 * 官方文档太长了就不看了，其实是错的，这个地方小马也错了，必须改正
						 */
						Intent intent = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						// 下面这句指定调用相机拍照后的照片存储的路径
						intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri
								.fromFile(new File(Environment
										.getExternalStorageDirectory(),
										"cbsiPortraitTest.jpg")));
						startActivityForResult(intent, 2);
					}
				}).show();
	}

	private void uploadPortraitToServer(final Bitmap photo) {
		if (currentUser == null) {
			return;
		}
		openUploadTipDialog();
		String uploadUrl = Constant4Connection.URL_SERVER_API_UPDALOAD_PORTRAIT;
		Map<String, String> params = new HashMap<String, String>();

		if (photo == null) {
			Log.v(TAG, "uploadPortraitToServer: Not get portrait bitmap!!!");
			return;
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] bytes = stream.toByteArray();
		// 将图片流以字符串形式存储下来

		final String picStr = new String(Base64Coder.encodeLines(bytes));

		params.put("userid", "" + currentUser.getUser_id());
		params.put("photoContent", picStr);// 参数
		params.put("photoName", "head");

		CBSiServerHttpRequest.getInstance(this).postRequest(uploadUrl, params,
				new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						Log.v(TAG, "upload portrait to server success!");
						try {
							JSONObject jsonResponse = new JSONObject(content);
							String stateCode = jsonResponse
									.getString("uploadPortrait");
							if (stateCode.equals("0")) {
								setPortraitView(photo);

								ImageHandleUtil.savePortraitFile(
										PersonalProfileActivity.this,
										currentUser.getUser_id(), photo);
								setResult(
										1,
										getIntent().putExtra("update_UserId",
												currentUser.getUser_id()));
							} else {
								Toast.makeText(PersonalProfileActivity.this,
										"由于服务器原因上传失败！", (int) 2).show();
							}
							new Handler().postDelayed(new Runnable() {
								public void run() {
									closeTipDialog();
								}
							}, 500);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Toast.makeText(PersonalProfileActivity.this,
									"上传失败：服务器json解析错误", (int) 2).show();
							closeTipDialog();
						}
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Log.v(TAG, "upload portrait to server failed!");
						Toast.makeText(PersonalProfileActivity.this,
								"网络请求失败，请检查网络！", (int) 2).show();
					}
				});
	}

	private RefreshDataViewDialog rdViewDialog;

	private void closeTipDialog() {
		rdViewDialog.closeDialog();
	}

	private void openUploadTipDialog() {
		rdViewDialog = null;
		rdViewDialog = new RefreshDataViewDialog(this,
				R.style.Dialog_Fullscreen);
		rdViewDialog.show();
		rdViewDialog.setTipContent("正在上传头像……");
	}
}
