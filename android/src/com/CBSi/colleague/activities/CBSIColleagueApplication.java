package com.CBSi.colleague.activities;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;

import com.CBSi.colleague.common.SystemScreenInfo;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.service.ContactDataAsyncLoaderService;

public class CBSIColleagueApplication extends Application {

	private List<UserDataModel> contactBeanList;

	private static CBSIColleagueApplication instance;
    
	public CBSIColleagueApplication()
    {
    }
     public static CBSIColleagueApplication getInstance()
     {
                    if(null == instance)
                  {
                     instance = new CBSIColleagueApplication();
                  }
         return instance;           
     }
	
	public List<UserDataModel> getContactDataList() {
		return contactBeanList;
	}

	public void setContactDataList(List<UserDataModel> contactBeanList) {
		this.contactBeanList = contactBeanList;
	}

	public void onCreate() {

		System.out.println("项目启动");
		SystemScreenInfo.getSystemInfo(getApplicationContext());
		Intent startService = new Intent(CBSIColleagueApplication.this,
				ContactDataAsyncLoaderService.class);
		startService(startService);

	}
	
	private List<Activity> activityList = new LinkedList<Activity>();
	
	//添加Activity到容器中
    public void addActivity(Activity activity)
    {
                   activityList.add(activity);
    }
    
    //遍历所有Activity并finish
    public void exit()
    {
		for (Activity activity : activityList) {
			activity.finish();
		}
   }

}
