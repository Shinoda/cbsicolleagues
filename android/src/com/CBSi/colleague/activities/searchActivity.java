package com.CBSi.colleague.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.CBSi.colleague.R;
import com.CBSi.colleague.adapter.SearchResultAdapter;
import com.CBSi.colleague.common.CommonDefination;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.util.CharacterParser;
import com.CBSi.colleague.util.FirstLetterUtil;
import com.CBSi.colleague.view.SearchContactDataEditText;

public class searchActivity extends Activity {
	
	private TextView noResultTV;
	private SearchContactDataEditText mSearchEditText;
	private ListView searchResultListV;
	
	private List<UserDataModel> allColleguesDataArray, allSearchDataArray;
	
	private View backView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.search_layout);
		
		allColleguesDataArray = DataManager.sortData(DataManager.getAllUserDataModels());
		
		initBackBtn();
		initSearchView();
	}
	
	private void initBackBtn() {
		backView = findViewById(R.id.back_root_layout);
		backView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
				
				((InputMethodManager) getSystemService(ContactListActivity.INPUT_METHOD_SERVICE)). 
			     hideSoftInputFromWindow(mSearchEditText.getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			}
		});
	}
	
	private void initSearchView() 
	{
		noResultTV = (TextView)findViewById(R.id.title_layout_no_result);
		
		mSearchEditText = (SearchContactDataEditText) findViewById(R.id.searchView_Editor);

		// 根据输入框输入值的改变来过滤搜索
		mSearchEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// 处理搜索事件
				searchDataByInput(s.toString());
				
				if(s.length() > 0 && (allSearchDataArray.size()==0))
				{
					noResultTV.setVisibility(View.VISIBLE);
				}
				else{
					noResultTV.setVisibility(View.GONE);
				}
				setSearchResultAdapter();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		
		//----Search view ready!
		searchResultListV = (ListView) findViewById(R.id.search_result_lv);
		searchResultListV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 这里要利用adapter.getItem(position)来获取当前position所对应的对象
				Toast.makeText(
						getApplication(),"item selected",position).show();
			}
		});
		
		searchResultListV.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				switch (scrollState) {  
	            case OnScrollListener.SCROLL_STATE_IDLE: //Scrolling is stop.  
	                break;  
	            case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL: //Is scrolling 
	            	((InputMethodManager) getSystemService(ContactListActivity.INPUT_METHOD_SERVICE)). 
				     hideSoftInputFromWindow(mSearchEditText.getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
	                break;  
	            case OnScrollListener.SCROLL_STATE_FLING:  //Begin to scroll
	                break;  
	            }  
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//----Search adapter
		allSearchDataArray = new ArrayList<UserDataModel>();
		setSearchResultAdapter();
	}
	
	private void setSearchResultAdapter() {
		SearchResultAdapter searchAdapter = new SearchResultAdapter(getApplicationContext(), this.allSearchDataArray);
		searchResultListV.setAdapter(searchAdapter);
		
		searchResultListV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(searchActivity.this,
						ContactDetailActivity.class);
				UserDataModel cData = (UserDataModel) parent.getAdapter()
						.getItem(position);
				intent.putExtra(CommonDefination.COLLEAGUE_DATA_KEY, cData);
				startActivity(intent);
				overridePendingTransition(R.anim.push_left_in,
						R.anim.push_left_out);// 右往左推出效果;
			}
		});
	}
	
	private void searchDataByInput(String inputStr) {
		if(inputStr.length() == 0)
		{
			allSearchDataArray.clear();
			return;
		}
		allSearchDataArray = search(inputStr, allColleguesDataArray, allSearchDataArray);
	}
	
	/**
	 * 按号码-拼音搜索联系人
	 * 
	 * @param str
	 */
	private List<UserDataModel> search(String str,
			List<UserDataModel> allContacts, List<UserDataModel> contactList) {
		contactList.clear();
//		// 如果搜索条件以0 1 +开头则按号码搜索
//		if (str.startsWith("0") || str.startsWith("1") || str.startsWith("+")) {
//			for (UserDataModel contact : allContacts) {
//				if (contact.getUser_phone() != null && contact.getUser_name() != null) {
//					if (contact.getUser_phone().contains(str)
//							|| contact.getUser_name().contains(str)) {
////						contact.setGroup(str);
//						contactList.add(contact);
//					}
//				}
//			}
//			return contactList;
//		}
		CharacterParser finder = CharacterParser.getInstance();

		String result = "";
		for (UserDataModel contact : allContacts) {
			// 先将输入的字符串转换为拼音
			finder.setResource(str);
			result = finder.getSpelling();
			if(chineseContain(contact, str))
			{
				contactList.add(contact);
			}
			else if (contains(contact, result) && isAlphabetString(str)) {
				contactList.add(contact);
			} 
//			else if (contact.getUser_phone().contains(str) 
//					|| contact.getUser_tel().contains(str)
//					|| contact.getUser_tel_sub().contains(str))
//			{
////				contact.setGroup(str);
//				contactList.add(contact);
//			}
		}
		return contactList;
	}

	/**
	 * 根据拼音搜索
	 * 
	 * @param str
	 *            正则表达式
	 * @param pyName
	 *            拼音
	 * @param isIncludsive
	 *            搜索条件是否大于6个字符
	 * @return
	 */
	public static boolean contains(UserDataModel contact, String search) {
		if (TextUtils.isEmpty(contact.getUser_name())) {
			return false;
		}

		boolean flag = false;

		// 简拼匹配,如果输入在字符串长度大于6就不按首字母匹配了
		if (search.length() < 6) {
			String firstLetters = FirstLetterUtil.getFirstLetter(contact.getUser_name());
			// 不区分大小写
			Pattern firstLetterMatcher = Pattern.compile(search,
					Pattern.CASE_INSENSITIVE);
			flag = firstLetterMatcher.matcher(firstLetters).find();
//			if(!flag)
//			{
//				// 部门匹配
//				String firstSectorLetters = FirstLetterUtil.getFirstLetter(contact.getDepartment());
//				Pattern firstSectorLetterMatcher = Pattern.compile(search,
//						Pattern.CASE_INSENSITIVE);
//				flag = firstSectorLetterMatcher.matcher(firstSectorLetters).find();
//			}
			if(!flag)
			{
				//Position match
				String firstPosLetters = FirstLetterUtil.getFirstLetter(contact.getUser_position());
				Pattern posMatcher = Pattern.compile(search,
						Pattern.CASE_INSENSITIVE);
				flag = posMatcher.matcher(firstPosLetters).find();
			}
			
		}

		if (!flag) { // 如果简拼已经找到了，就不使用全拼了
			// 全拼匹配
			CharacterParser finder = CharacterParser.getInstance();
			finder.setResource(contact.getUser_name());
			// 不区分大小写
			Pattern pattern2 = Pattern
					.compile(search, Pattern.CASE_INSENSITIVE);
			Matcher matcher2 = pattern2.matcher(finder.getSpelling());
			flag = matcher2.find();
			
//			if(!flag){
//				finder.setResource(contact.getDepartment());
//				Pattern patternSector = Pattern
//						.compile(search, Pattern.CASE_INSENSITIVE);
//				Matcher matcherSector = patternSector.matcher(finder.getSpelling());
//				flag = matcherSector.find();
//			}
			
			if(!flag){
				finder.setResource(contact.getUser_position());
				Pattern patternPos = Pattern
						.compile(search, Pattern.CASE_INSENSITIVE);
				Matcher matcherPos = patternPos.matcher(finder.getSpelling());
				flag = matcherPos.find();
			}
		}

		return flag;
	}
	
	public static boolean chineseContain(UserDataModel contact, String chChar) {
		boolean isContain = false;
		isContain = contact.getUser_name().contains(chChar) || contact.getUser_position().contains(chChar)
				/*|| contact.getDepartment().contains(chChar)*/;
		
		return isContain;
	}
	
	private boolean isAlphabetString(String string) {
//		for(int i=0; i<string.length(); i++)
//		{
//		if(String.valueOf(string.charAt(i)).getBytes().length==2)
//		{
//			isChinese = true;
//			break;
//		}
//		}
		 String strPattern = "^[A-Za-z]+";  
		    Pattern p = Pattern.compile(strPattern);  
		    Matcher m = p.matcher(string);  
		return m.matches(); 
	}
	
	@Override  
	public boolean onTouchEvent(MotionEvent event) {  
	  // TODO Auto-generated method stub  
		InputMethodManager manager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	  if(event.getAction() == MotionEvent.ACTION_DOWN){  
	      if(getCurrentFocus()!=null && getCurrentFocus().getWindowToken()!=null){  
	       manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
	       InputMethodManager.HIDE_NOT_ALWAYS);    }   }
	       return super.onTouchEvent(event);  
	} 
}
