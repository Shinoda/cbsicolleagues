package com.CBSi.colleague.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;

import com.CBSi.colleague.R;
import com.CBSi.colleague.common.CommonDefination;
import com.CBSi.colleague.util.SharedPreferenceUtil;

public class SplashActivity extends Activity {

	private final int SPLASH_DISPLAY_LENGHT = 3000; // 延迟三秒

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (!SharedPreferenceUtil
						.isFirstLaunch(getApplicationContext())) {
					enterUserGuide();
					SharedPreferenceUtil.saveFirstLaunch(
							getApplicationContext(), true);
				} else {
					if (SharedPreferenceUtil
							.isLoginSuccess(getApplicationContext())) {
						enterMain();
					} else {
						enterLogin();
					}
				}
			}

		}, SPLASH_DISPLAY_LENGHT);
	}

	private void enterUserGuide() {
		Intent mainIntent = new Intent(this, UserGuideActivity.class);
		startActivity(mainIntent);
		finish();
	}

	private void enterLogin() {
		Intent mainIntent = new Intent(this, LoginActivity.class);
		startActivity(mainIntent);
		finish();
	}

	private void enterMain() {
		Intent mainIntent = new Intent(this, ContactListActivity.class);
		startActivity(mainIntent);
		finish();
	}
}
