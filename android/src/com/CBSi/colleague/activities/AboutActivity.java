package com.CBSi.colleague.activities;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.CBSi.colleague.R;
import com.CBSi.colleague.common.CommonDefination;

public class AboutActivity extends Activity {

	private View backView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_layout);
		initBackBtn();
		TextView buildVer = (TextView)findViewById(R.id.about_build_version_tv);
		buildVer.setText(getVersionName());
	}
	
	private void initBackBtn() {
		backView = findViewById(R.id.back_root_layout);
		backView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
				
			}
		});
	}
	
	/**
	 * 获取版本号
	 * @return 当前应用的版本号
	 */
	private String getVersionName() {
		String version = CommonDefination.COLLEAGUE_APK_BUILD_VERSION;
//		try {
//			// 获取packagemanager的实例
//			PackageManager packageManager = getPackageManager();
//			// getPackageName()是你当前类的包名，0代表是获取版本信息
//			PackageInfo packInfo = packageManager.getPackageInfo(
//					getPackageName(), 0);
//			version = packInfo.versionName;
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}

		return version;
	}
}
