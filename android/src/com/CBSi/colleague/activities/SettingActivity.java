package com.CBSi.colleague.activities;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.CBSi.colleague.R;
import com.CBSi.colleague.connection.CBSiServerHttpRequest;
import com.CBSi.colleague.connection.Constant4Connection;
import com.CBSi.colleague.connection.CBSiServerHttpRequest.HttpResponseListener;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.db.ContactDao;
import com.CBSi.colleague.db.SectorDao;
import com.CBSi.colleague.util.SharedPreferenceUtil;
import com.CBSi.colleague.view.RefreshDataViewDialog;
import com.android.volley.VolleyError;

public class SettingActivity extends Activity {
	private static final String tag = "SettingActivity";
	private View backView;

	private View updateContactV, checkVerV, aboutV;

	private enum SETTING_OPTION {
		EType_DataUpdator, EType_CheckVersion, EType_About
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_layout);
		initSettingView();
	}

	private void initSettingView() {
		initBackBtn();

		updateContactV = findViewById(R.id.update_contact_layout);
		checkVerV = findViewById(R.id.check_ver_layout);
		aboutV = findViewById(R.id.setting_about_layout);

		setClickEventForView(updateContactV, SETTING_OPTION.EType_DataUpdator);
		setClickEventForView(checkVerV, SETTING_OPTION.EType_CheckVersion);
		setClickEventForView(aboutV, SETTING_OPTION.EType_About);
	}

	private void initBackBtn() {
		backView = findViewById(R.id.back_root_layout);
		backView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);

			}
		});
	}

	private void setClickEventForView(View v, final SETTING_OPTION option) {
		v.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (option == SETTING_OPTION.EType_DataUpdator) {
					showRefreshContactDataDialog();
					// new Handler().postDelayed(new Runnable() {
					//
					// @Override
					// public void run() {
					// // TODO Auto-generated method stub
					// closeRefreshDataDialog();
					// }
					// }, 2000);
					getAllSectorFromServer();
				} else if (option == SETTING_OPTION.EType_CheckVersion) {
					CheckLastestApkVer();
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							closeRefreshDataDialog();

							try {
								Thread.sleep(300);
								showLastestVersionTip();
								new Handler().postDelayed(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										closeRefreshDataDialog();
									}
								}, 2000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}, 2000);

				} else if (option == SETTING_OPTION.EType_About) {
					Intent intent = new Intent(SettingActivity.this,
							AboutActivity.class);
					startActivity(intent);
				}
			}
		});
	}

	private RefreshDataViewDialog rdViewDialog;

	private void showRefreshContactDataDialog() {
		rdViewDialog = null;
		rdViewDialog = new RefreshDataViewDialog(this, R.style.Dialog_Fullscreen);
		rdViewDialog.show();
	}

	private void closeRefreshDataDialog() {
		rdViewDialog.closeDialog();
	}

	private void CheckLastestApkVer() {
		rdViewDialog = null;
		rdViewDialog = new RefreshDataViewDialog(this, R.style.Dialog_Fullscreen);
		rdViewDialog.show();
		rdViewDialog.setTipContent("正在检查版本更新……");
	}

	private void showLastestVersionTip() {
		rdViewDialog = null;
		rdViewDialog = new RefreshDataViewDialog(this, R.style.Dialog_Fullscreen);
		rdViewDialog.show();
		rdViewDialog.setTipContent("当前已经是最新版本!");
	}

	private void getAllContactDataFromServer() {
		ContactDao.getInstance(SettingActivity.this).deleteAll();
		String getContactUrl = Constant4Connection.URL_SERVER_API_LOADCONTACT;
		Map<String, String> params = new HashMap<String, String>();
		params.put("corpid",
				SharedPreferenceUtil.getCorperationId(getApplicationContext()));
		CBSiServerHttpRequest.getInstance(this).postRequest(getContactUrl,
				params, new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						// 成功，在这里写处理内容的代码
						Log.v(tag,
								"Get contact from server success! reponse data: "
										+ content);
						DataManager.saveUserListToDB(content,
								ContactDao.getInstance(SettingActivity.this));
						closeRefreshDataDialog();
						// ----Notify ContactListActivity to refresh data-list.
						Intent intent = new Intent();
						intent.setAction("com.contactdata.refreshed");
						// intent.putExtra("cbsi_event", "updated");
						sendOrderedBroadcast(intent, null); // 有序广播发送
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Toast.makeText(SettingActivity.this, "网络请求失败，请检查网络！",
								(int) 2).show();
						// 失败
						Log.v(tag,
								"Get contact from server failed! reponse exception: "
										+ error);

						closeRefreshDataDialog();
					}
				});

	}

	private void getAllSectorFromServer() {
		SectorDao.getInstance(getApplicationContext()).deleteAll();
		String getSectorUrl = Constant4Connection.URL_SERVER_API_LOADSECTOR;
		Map<String, String> params = new HashMap<String, String>();
		params.put("corpid",
				SharedPreferenceUtil.getCorperationId(getApplicationContext()));
		CBSiServerHttpRequest.getInstance(this).postRequest(getSectorUrl,
				params, new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						// 成功，在这里写处理内容的代码
						Log.v(tag, "Get Sector from server success! -->"
								+ content);
						DataManager.saveSectorListToDB(content, getApplicationContext());

						getAllContactDataFromServer();
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Toast.makeText(SettingActivity.this, "网络请求失败，请检查网络！",
								(int) 2).show();
						// 失败
						Log.v(tag,
								"Get contact from server failed! reponse exception: "
										+ error);
						
					}
				});

	}
}
