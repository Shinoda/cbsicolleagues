package com.CBSi.colleague.activities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.CBSi.colleague.R;
import com.CBSi.colleague.adapter.ContactHomeAdapter;
import com.CBSi.colleague.adapter.DepartmentListAdapter;
import com.CBSi.colleague.connection.CBSiServerHttpRequest;
import com.CBSi.colleague.connection.CBSiServerHttpRequest.HttpResponseListener;
import com.CBSi.colleague.connection.Constant4Connection;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.data.SectorDataModel;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.db.ContactDao;
import com.CBSi.colleague.db.SectorDao;
import com.CBSi.colleague.util.Loger;
import com.CBSi.colleague.util.SharedPreferenceUtil;
import com.CBSi.colleague.view.PopDialog;
import com.CBSi.colleague.view.QuickAlphabeticBar;
import com.CBSi.colleague.view.RefreshDataViewDialog;
import com.CBSi.colleague.view.SlidingLayout;
import com.android.volley.VolleyError;

@SuppressLint({ "InflateParams", "HandlerLeak", "ClickableViewAccessibility" })
public class ContactListActivity extends Activity {
	private final String TAG = "ContactListActivity";

	public static final int GET_CONTACT_BY_DEPARTMENT = 0;
	public static final int INIT_COMPLETE = 1;
	public static final int GET_SERVER_USER_COMPLETE = 100;
	public static final int GET_SERVER_SECTOR_COMPLETE = 101;
	public static final int MENU_BTN_STATE_REFRESH = 2;

	private SlidingLayout scrollView;
	/**
	 * 部门列表
	 */
	private ListView departmentListView;
	private DepartmentListAdapter deptListAdapter;
	private View acbuwaPage;
	private ImageView menuBtn;
	private TextView titleBarTextView;
	private LayoutInflater inflater;
	private ContactHomeAdapter adapter;
	private ListView personList;
	private List<UserDataModel> allColleguesDataArray;
	private List<SectorDataModel> allDepartments;
	private QuickAlphabeticBar quickAlphabeticBar;
	private ContactDao contactDao;
	private ImageView searchBtn;
	private View personalBtn;
	private RefreshContactDataReceiver mContactDataReceiver;

	/**
	 * 搜索部分 属性
	 */
	// ---------------------The Search part upon.----------------------------//

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case GET_CONTACT_BY_DEPARTMENT:
				Loger.i(TAG, "GET_CONTACT_BY_DEPARTMENT");
				break;
			case INIT_COMPLETE:
				Loger.i(TAG, "INIT_COMPLETE");
				break;
			case GET_SERVER_USER_COMPLETE:
				getAllSectorFromServer();

				break;
			case GET_SERVER_SECTOR_COMPLETE:
				refreshView();
				titleBarTextView.setText(SharedPreferenceUtil
						.getCorperationName(getApplicationContext()));
				closeRefreshDataDialog();
				break;
			case MENU_BTN_STATE_REFRESH:
				setMenuBtnImageState(scrollView.isLeftLayoutVisible() ? E_MENU_BUTTON_STATE.MenuBtnSel
						: E_MENU_BUTTON_STATE.MenuBtnNormal);
				break;
			default:
				break;
			}
		}
	};

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		CBSIColleagueApplication.getInstance().addActivity(this);
		contactDao = ContactDao.getInstance(this);

		inflater = LayoutInflater.from(this);
		setContentView(R.layout.maincontactlist_layout);
		scrollView = (SlidingLayout) findViewById(R.id.slidingLayout);
		scrollView.setHandler(handler);
		deptListAdapter = new DepartmentListAdapter(this, handler);

		departmentListView = (ListView) findViewById(R.id.menuList);

		departmentListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (allDepartments == null) {
					return;
				}
				if (scrollView != null && scrollView.isLeftLayoutVisible()) {
					scrollView.scrollToRightLayout();
				}
				deptListAdapter.changeState(position);

				SectorDataModel sector = allDepartments.get(position);
				long department_id = sector.getSector_id();
				if (department_id == -1) {
					allColleguesDataArray = DataManager.getAllUserDataModels();
				} else {
					allColleguesDataArray = DataManager.getUsersByDepartmentId(
							getApplicationContext(), sector.getSector_id());
				}
				setColleagueListAdapter(allColleguesDataArray);
				titleBarTextView.setText(sector.getSector_name());
			}
		});

		acbuwaPage = findViewById(R.id.contact_page);
		menuBtn = (ImageView) findViewById(R.id.menuBtn);
		titleBarTextView = (TextView) findViewById(R.id.topbar_title);
		personList = (ListView) this.acbuwaPage.findViewById(R.id.acbuwa_list);

		View headerView = inflater.inflate(R.layout.contact_list_header, null);
		personList.addHeaderView(headerView, null, false);
		// 将监听滑动事件绑定在contentListView上
		scrollView.setScrollEvent(personList);
		quickAlphabeticBar = (QuickAlphabeticBar) this.acbuwaPage
				.findViewById(R.id.fast_scroller);

		menuBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (scrollView.isLeftLayoutVisible()) {
					scrollView.scrollToRightLayout();
				} else {
					scrollView.scrollToLeftLayout();
				}
			}
		});

		menuBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				setMenuBtnImageState((event.getAction() != MotionEvent.ACTION_UP) ? E_MENU_BUTTON_STATE.MenuBtnSel
						: E_MENU_BUTTON_STATE.MenuBtnNormal);
				return false;
			}
		});

		searchBtn = (ImageView) findViewById(R.id.topbar_searchBtn);
		searchBtn.setOnTouchListener(new OnTouchListener() {

			@SuppressWarnings("deprecation")
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				searchBtn.setAlpha((event.getAction() == MotionEvent.ACTION_UP) ? 255
						: 128);
				return false;
			}
		});
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(ContactListActivity.this,
						searchActivity.class));
			}
		});

		personalBtn = findViewById(R.id.personnal);
		personalBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ContactListActivity.this,
						PersonalProfileActivity.class);
				ContactListActivity.this.startActivityForResult(intent, 1);
			}
		});

		new Thread() {
			public void run() {
				init();
			}
		}.start();
		showRefreshContactDataDialog();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1) {
			long userId = data.getExtras().getLong("update_UserId");
			adapter.updateItemWithUserId(userId);
		}
	}

	public enum E_MENU_BUTTON_STATE {
		MenuBtnNormal, MenuBtnSel
	};

	@SuppressWarnings("deprecation")
	public void setMenuBtnImageState(E_MENU_BUTTON_STATE state) {
		switch (state) {
		case MenuBtnNormal:
			menuBtn.setAlpha(255);
			break;
		case MenuBtnSel:
			menuBtn.setAlpha(128);
			break;
		default:
			break;
		}
	}

	public View getAcbuwaPage() {
		return acbuwaPage;
	}

	private void init() {
		// 网络拉取数据
		getAllContactDataFromServer();

		// 发送网络拉取请求之后，到数据库中获取数据先显示出来，等网络数据拉取成功之后再刷新
		DataManager.loadContactDataFromDB(getApplicationContext());
		allDepartments = DataManager.getAllDepartments();
		allColleguesDataArray = DataManager.getAllUserDataModels();
	}

	private void setColleagueListAdapter(List<UserDataModel> list2) {
		adapter = new ContactHomeAdapter(this, allColleguesDataArray,
				quickAlphabeticBar, personList);
		personList.setAdapter(adapter);
		quickAlphabeticBar.init(ContactListActivity.this);
		quickAlphabeticBar.setListView(personList);
		quickAlphabeticBar.setVisibility(View.VISIBLE);
		personList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				UserDataModel cData = (UserDataModel) parent.getAdapter()
						.getItem(position);
				String[] callingOption = new String[] { "拨打电话", "发送短信", "取消" };

				if (cData.getUser_phone().length() > 0) {
					callingOption = new String[] { cData.getUser_phone(),
							cData.getUser_tel(), "发送短信" };
				}
				showContactDialog(callingOption, cData, position);
			}
		});
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (scrollView.isLeftLayoutVisible()) {
				scrollView.scrollToRightLayout();
			} else
				this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshView();
	}

	@Override
	protected void onStart() {
		super.onStart();
		registerReceiver();
	}

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver();
	}

	private void registerReceiver() {
		if (mContactDataReceiver == null) {
			mContactDataReceiver = new RefreshContactDataReceiver();
		}
		IntentFilter intentFilter = new IntentFilter(
				"com.contactdata.refreshed");
		registerReceiver(mContactDataReceiver, intentFilter);
	}

	private void unregisterReceiver() {
		if (mContactDataReceiver != null) {
			unregisterReceiver(mContactDataReceiver);
		}
	}

	// 群组联系人弹出页
	private void showContactDialog(final String[] arg, final UserDataModel cdm,
			final int position) {
		final PopDialog dialog = new PopDialog(this, arg, R.style.PopDialog);
		dialog.setCollegue(cdm);
		dialog.show();
	}

	private void getAllContactDataFromServer() {
		ContactDao.getInstance(getApplicationContext()).deleteAll();
		String getContactUrl = Constant4Connection.URL_SERVER_API_LOADCONTACT;

		Map<String, String> params = new HashMap<String, String>();
		params.put("corpid",
				SharedPreferenceUtil.getCorperationId(getApplicationContext()));

		CBSiServerHttpRequest.getInstance(this).postRequest(getContactUrl,
				params, new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						// 成功，在这里写处理内容的代码
						Log.v(TAG,
								"Get contact from server success! reponse data: "
										+ content);
						DataManager.saveUserListToDB(content,
								ContactDao.getInstance(getApplicationContext()));
						Message msg = handler.obtainMessage();
						msg.what = GET_SERVER_USER_COMPLETE;
						handler.sendMessage(msg);
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Toast.makeText(getApplicationContext(),
								"网络请求失败，请检查网络！", (int) 2).show();
						// 失败
						Log.v(TAG,
								"Get contact from server failed! reponse exception: "
										+ error);
						closeRefreshDataDialog();
					}
				});

	}

	private void getAllSectorFromServer() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("corpid",
				SharedPreferenceUtil.getCorperationId(getApplicationContext()));
		CBSiServerHttpRequest.getInstance(this).postRequest(
				Constant4Connection.URL_SERVER_API_LOADSECTOR, params,
				new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						// 成功，在这里写处理内容的代码
						Log.v(TAG,
								"Get Sector from server success! reponse data: \n"
										+ content);
						DataManager.saveSectorListToDB(content,
								getApplicationContext());

						Message msg = handler.obtainMessage();
						msg.what = GET_SERVER_SECTOR_COMPLETE;
						handler.sendMessage(msg);
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Toast.makeText(getApplicationContext(),
								"网络请求失败，请检查网络！", (int) 2).show();
						closeRefreshDataDialog();
					}
				});

	}

	private RefreshDataViewDialog rdViewDialog;

	private void showRefreshContactDataDialog() {
		rdViewDialog = new RefreshDataViewDialog(this,
				R.style.Dialog_Fullscreen);
		rdViewDialog.show();
	}

	private void closeRefreshDataDialog() {
		rdViewDialog.cancel();
	}

	private void refreshView() {
		DataManager.loadDataFromDb(getApplicationContext());
		allDepartments = DataManager.getAllDepartments();
		if (allDepartments != null) {
			deptListAdapter.setData(allDepartments);
			departmentListView.setAdapter(deptListAdapter);
		}
		allColleguesDataArray = DataManager.getAllUserDataModels();
		if (allColleguesDataArray != null) {
			setColleagueListAdapter(allColleguesDataArray);
		}
	}

	public class RefreshContactDataReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equalsIgnoreCase("com.contactdata.refreshed")) {
				abortBroadcast(); // Receiver1接收到广播后中断广播
				refreshView();
			}
		}

	}
}
