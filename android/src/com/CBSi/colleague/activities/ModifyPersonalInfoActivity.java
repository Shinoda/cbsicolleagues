package com.CBSi.colleague.activities;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.CBSi.colleague.R;
import com.CBSi.colleague.connection.CBSiServerHttpRequest;
import com.CBSi.colleague.connection.CBSiServerHttpRequest.HttpResponseListener;
import com.CBSi.colleague.connection.Constant4Connection;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.db.ContactDao;
import com.CBSi.colleague.util.SharedPreferenceUtil;
import com.CBSi.colleague.view.RefreshDataViewDialog;
import com.android.volley.VolleyError;

public class ModifyPersonalInfoActivity extends Activity {

	private final String TAG = "ModifyPersonalInfoActivity";
	private TextView backBtn, saveBtn;
	private TextView titleTV;
	private String[] inputContent;
	private ImageView deleteBtn;
	private EditText modifyContentEditor;
	private UserDataModel currentUser;

	public enum EModifyType {
		ModifyType_Mail, ModifyType_Phone, ModifyType_Tel, ModifyType_TelSub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		currentUser = ContactDao
				.getInstance(getApplicationContext())
				.findUserByUserId(
						SharedPreferenceUtil.getUserId(getApplicationContext()));
		setContentView(R.layout.personal_info_modify);

		inputContent = getIntent().getStringArrayExtra("ModifyInfo");
		initModifiyInfoView();

	}

	private void initModifiyInfoView() {
		if (inputContent != null && currentUser != null) {
			deleteBtn = (ImageView) findViewById(R.id.delete);
			deleteBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					modifyContentEditor.setText("");
				}
			});
			
			titleTV = (TextView) findViewById(R.id.pm_title);
			titleTV.setText(inputContent[0]);

			modifyContentEditor = (EditText) findViewById(R.id.pm_content);
			int inputType = Integer.parseInt(inputContent[1]);
			modifyContentEditor.setInputType(inputType);

			EModifyType mType = EModifyType.valueOf(inputContent[2]);
			String content = "";
			switch (mType) {
			case ModifyType_Mail:
				content = currentUser.getUser_email();
				break;
			case ModifyType_Phone:
				modifyContentEditor
						.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
								11) });
				modifyContentEditor.setInputType(EditorInfo.TYPE_CLASS_PHONE);
				content = currentUser.getUser_phone();
				break;
			case ModifyType_Tel:
				modifyContentEditor
						.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
								20) });
				modifyContentEditor.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
				content = currentUser.getUser_tel();
				break;
			case ModifyType_TelSub:
				modifyContentEditor
						.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
								20) });
				modifyContentEditor.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
				content = currentUser.getUser_tel_sub();
				break;
			default:
				break;
			}
			modifyContentEditor.setText(content);
		}

		backBtn = (TextView) findViewById(R.id.pm_btn_back);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
			}
		});

		saveBtn = (TextView) findViewById(R.id.pm_btn_save);
		saveBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputmanger.hideSoftInputFromWindow(
						modifyContentEditor.getWindowToken(), 0);
				saveModifiedInfo();
			}
		});
	}

	private void saveModifiedInfo() {
		EModifyType mType = EModifyType.valueOf(inputContent[2]);

		String modifyKey = null;
		switch (mType) {
		case ModifyType_Mail:
			String emailAddress = modifyContentEditor.getText().toString();
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(emailAddress);
			boolean isMatched = matcher.matches();
			if (isMatched || TextUtils.isEmpty(emailAddress)) { //允许保存空邮箱
				currentUser.setUser_email(modifyContentEditor.getText()
						.toString());
				modifyKey = "user_email";
			} else {
				Toast.makeText(getApplicationContext(),
						getString(R.string.email_address_error),
						Toast.LENGTH_LONG).show();
				return;
			}
			break;
		case ModifyType_Phone:
			currentUser.setUser_phone(modifyContentEditor.getText().toString());
			modifyKey = "user_phone";
			break;
		case ModifyType_Tel:
			currentUser.setUser_tel(modifyContentEditor.getText().toString());
			modifyKey = "user_tel";
			break;
		case ModifyType_TelSub:
			currentUser.setUser_tel_sub(modifyContentEditor.getText()
					.toString());
			modifyKey = "user_tel_sub";
			break;
		default:
			break;
		}
		showSaveContactInfoDataDialog();
		modifyUserInfoToServer(modifyKey, modifyContentEditor.getText()
				.toString());
		String[] modifyItemStr = { inputContent[2],
				modifyContentEditor.getText().toString() };
		setResult(0, getIntent().putExtra("ModifyItemContent", modifyItemStr));
	}

	private void modifyUserInfoToServer(String mKey, String value) {
		Log.i(TAG, "value-->" + value);
		String modifyUrl = Constant4Connection.URL_SERVER_API_MODIFYUSERINFO;
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", "" + currentUser.getUser_id());
		params.put(mKey, value);// 参数

		CBSiServerHttpRequest.getInstance(this).postRequest(modifyUrl, params,
				new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						Log.v(TAG, "Modify to server result:" + content);
						try {
							closeSaveContactInfoDataDialog();
							JSONObject jsonResponse = new JSONObject(content);
							String stateCode = jsonResponse
									.getString("StateCode");
							if (stateCode.equals("1")) {
								long rt = ContactDao.getInstance(
										ModifyPersonalInfoActivity.this)
										.updateUser(currentUser);
								Log.v(TAG, "Update local user info result: "
										+ rt);
								finish();
							}else if(stateCode.equals("-1")){
								Toast.makeText(ModifyPersonalInfoActivity.this,
										"保存失败！", Toast.LENGTH_LONG).show();
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						// TODO Auto-generated method stub
						Log.v(TAG, "Modify to server failed!");
						closeSaveContactInfoDataDialog();
						Toast.makeText(ModifyPersonalInfoActivity.this,
								"网络请求失败，请检查网络！", (int) 2).show();
					}
				});
	}

	private RefreshDataViewDialog saveDialog;

	private void showSaveContactInfoDataDialog() {
		saveDialog = new RefreshDataViewDialog(this, R.style.Dialog_Fullscreen);
		saveDialog.show();
		saveDialog.setTipContent("正在保存...");
	}

	private void closeSaveContactInfoDataDialog() {
		if (saveDialog != null) {
			saveDialog.cancel();
		}
	}
}
