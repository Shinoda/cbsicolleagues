package com.CBSi.colleague.activities;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.CBSi.colleague.R;
import com.CBSi.colleague.common.CommonDefination;
import com.CBSi.colleague.connection.CBSiServerHttpRequest;
import com.CBSi.colleague.connection.CBSiServerHttpRequest.HttpResponseListener;
import com.CBSi.colleague.connection.Constant4Connection;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.util.SharedPreferenceUtil;
import com.CBSi.colleague.view.CountDownButton;
import com.CBSi.colleague.view.CountDownButton.OnFinishListener;
import com.CBSi.colleague.view.ErrorDialog;
import com.CBSi.colleague.view.RefreshDataViewDialog;
import com.CBSi.colleague.view.SmsContent;
import com.android.volley.VolleyError;

public class LoginActivity extends Activity {

	private final String TAG = "LoginActivity";
	private Button loginBtn, dynamicCodeButton;
	private EditText phoneNOInputEditor;
	private EditText dynamicCodeEditor;
	private CountDownButton cDownButton = new CountDownButton();
	private SmsContent content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.login_layout);

		initLoginView();
	}

	private void initLoginView() {
		phoneNOInputEditor = (EditText) findViewById(R.id.phoneNumberEditText);
		checkPhoneNumberInput();
		dynamicCodeEditor = (EditText) findViewById(R.id.dynamicCodeEditText);
		dynamicCodeButton = (Button) findViewById(R.id.fetchDynamicCodeButton);

		String phoneNum = getNativePhoneNumber();
		if (!TextUtils.isEmpty(phoneNum)) {
			phoneNOInputEditor.setText(phoneNum);
		}

		dynamicCodeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sendSmsRequest();
				cDownButton.start();
				phoneNOInputEditor.setEnabled(false);
			}
		});

		cDownButton.init(getApplicationContext(), dynamicCodeButton,
				new OnFinishListener() {

					@Override
					public void onFinish() {
						dynamicCodeButton
								.setText(getString(R.string.reobtain_verify_code));
						phoneNOInputEditor.setEnabled(true);
					}
				});

		loginBtn = (Button) findViewById(R.id.loginButton);
		loginBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// －－－调试用，暂时改为直接登录。－－－<Hequn>
				loginRequestPost();
			}
		});

		content = new SmsContent(LoginActivity.this, new Handler(),
				dynamicCodeEditor);
		getContentResolver().registerContentObserver(
				Uri.parse("content://sms/"), true, content);
	}

	private final int charMaxNum = 11;

	private void checkPhoneNumberInput() {
		phoneNOInputEditor.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String content = s.toString();
				if (TextUtils.isEmpty(content)) {
					dynamicCodeButton
							.setBackgroundResource(R.color.count_down_background_color);
					dynamicCodeButton.setClickable(false);
				} else {
					dynamicCodeButton
							.setBackgroundResource(R.drawable.btn_fetch_verifycode_bg_color_selector);
					dynamicCodeButton.setClickable(true);
				}
			}
		});
	}

	// private boolean isValidPhoneNumberSimpleM(String phoneNO) {
	// boolean ret = false;
	// int phoneNOLength = phoneNO.length();
	// if (phoneNOLength == 0) {
	// ret = true;
	// } else if (phoneNOLength > 0) {
	// ret = phoneNO.substring(0, 1).equals("1");
	// } else {
	// ret = false;
	// }
	// return ret;
	// }

	private RefreshDataViewDialog rdViewDialog;

	private void showStartLoginDialog() {
		rdViewDialog = null;
		rdViewDialog = new RefreshDataViewDialog(this,
				R.style.Dialog_Fullscreen);
		rdViewDialog.show();
		rdViewDialog.setTipContent("正在登录，请稍后……");
	}

	private void closeLoginDialog() {
		rdViewDialog.closeDialog();
	}

	private void loginRequestPost() {// 调用Server接口，Post请求
		String loginUrl = Constant4Connection.URL_SERVER_API_LOGIN;
		Map<String, String> params = new HashMap<String, String>();
		params.put("userPhone", phoneNOInputEditor.getText().toString());// 参数
		params.put("dynamicCode", dynamicCodeEditor.getText().toString());// 参数
		showStartLoginDialog();
		CBSiServerHttpRequest.getInstance(this).postRequest(loginUrl, params,
				new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						// 成功，在这里写处理内容的代码
						Log.v(TAG, "Login success! reponse data: " + content);
						try {

							JSONObject jsonResponse = new JSONObject(content);
							String stateCode = jsonResponse
									.getString("StateCode");
							if (stateCode.equals("0")) {
								String corpid = jsonResponse
										.getString("corpid");
								String userID = jsonResponse
										.getString("userid");
								setSystemConfig(corpid, userID);
								new Handler().postDelayed(new Runnable() {
									public void run() {
										loginSuccessToEnter();
									}
								}, 1800);

								// 请求公司信息
								getCorpRequst(corpid);
								//
							} else if (stateCode.equals("-204")) {
								Toast.makeText(LoginActivity.this, "用户不存在",
										Toast.LENGTH_LONG).show();
							} else if (stateCode.equals("-202")) {
								Toast.makeText(LoginActivity.this, "验证码错误",
										Toast.LENGTH_LONG).show();
							} else {
								Toast.makeText(LoginActivity.this, "登录失败，未知错误",
										Toast.LENGTH_LONG).show();
							}
							new Handler().postDelayed(new Runnable() {
								public void run() {
									closeLoginDialog();
								}
							}, 1500);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Toast.makeText(LoginActivity.this,
									"登录失败--json error", (int) 2).show();
							new Handler().postDelayed(new Runnable() {
								public void run() {
									closeLoginDialog();
								}
							}, 1500);
						}
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Toast.makeText(LoginActivity.this, "网络请求失败，请检查网络！",
								(int) 2).show();
						new Handler().postDelayed(new Runnable() {
							public void run() {
								closeLoginDialog();
								;
							}
						}, 1500);
						// 失败
						Log.v(TAG, "Login failed! reponse exception: " + error);
					}
				});

	}

	private void getCorpRequst(String corpid) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("corpid", corpid);// 参数
		CBSiServerHttpRequest.getInstance(this).postRequest(
				Constant4Connection.URL_SERVER_API_GET_CORPORATION, params,
				new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						Log.i(TAG, "getCorpRequst-content-->" + content);
						try {
							JSONObject jsonResponse = new JSONObject(content);
							String corporation_name = jsonResponse
									.getString("corporation_name");
							if (!TextUtils.isEmpty(corporation_name)) {
								SharedPreferenceUtil.saveCorperationName(
										getApplicationContext(),
										corporation_name);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onResponseFailed(VolleyError error) {

					}
				});
	}

	private void sendSmsRequest() {// 调用Server接口，Post请求
		Log.i(TAG, "sendSmsRequest");
		String senSmsUrl = Constant4Connection.URL_SERVER_API_SEND_SMS;
		Map<String, String> params = new HashMap<String, String>();
		Log.i(TAG, "phoneNOInputEditor.getText().toString()-->"
				+ phoneNOInputEditor.getText().toString());
		params.put("phone", phoneNOInputEditor.getText().toString());// 参数
		params.put("type", "1");// 参数
		CBSiServerHttpRequest.getInstance(this).postRequest(senSmsUrl, params,
				new HttpResponseListener() {

					@Override
					public void onResponseSuccess(String content) {
						Log.i(TAG, "content-->" + content);
						try {
							JSONObject jsonResponse = new JSONObject(content);
							String result = jsonResponse.getString("result");
							if (result.equals("-1")) {
								cDownButton.cancelTask();
								dynamicCodeButton
										.setText(getString(R.string.reobtain_verify_code));
								phoneNOInputEditor.setEnabled(true);
								String error = jsonResponse.getString("err");
								Log.i(TAG, "error-->" + error);
								if (error.equals("-1") || error.equals("-2")
										|| error.equals("-3")
										|| error.equals("-4")) {

								} else if (error.equals("-101")) {
									showErrorDialog(getString(R.string.dialog_no_user));
									return;
								} else if (error.equals("-102")
										|| error.equals("-103")
										|| error.equals("-200")
										|| error.equals("-201")) {

								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onResponseFailed(VolleyError error) {
						Log.i(TAG, "error.getMessage-->" + error.getMessage());
						cDownButton.cancelTask();
						dynamicCodeButton
								.setText(getString(R.string.reobtain_verify_code));
						phoneNOInputEditor.setEnabled(true);
						Toast.makeText(getApplicationContext(),
								"获取验证码失败，请重试：" + error.getMessage(),
								Toast.LENGTH_LONG).show();
					}
				});

	}

	private void showErrorDialog(String content) {
		new ErrorDialog(this, getString(R.string.note), content,
				R.style.PopDialog).show();
	}

	private void loginSuccessToEnter() {
		Intent mainIntent = new Intent(LoginActivity.this,
				ContactListActivity.class);
		this.startActivity(mainIntent);
		this.finish();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.v(TAG, "Back system-Key pressed, ready for exit!");
			this.finish();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void setSystemConfig(String corpid, String userID) {
		SharedPreferenceUtil.saveCorperationId(getApplicationContext(), corpid);
		SharedPreferenceUtil.saveUserId(getApplicationContext(),
				Long.parseLong(userID));
		SharedPreferenceUtil.saveLatestUserInfo(getApplicationContext(),
				phoneNOInputEditor.getText().toString(), dynamicCodeEditor
						.getText().toString());
		SharedPreferenceUtil.saveLoginSuccess(getApplicationContext(), true);
	}

	private void defaultUserInfo() {
		phoneNOInputEditor.setText(SharedPreferenceUtil
				.getLatestUserNum(getApplicationContext()));
		dynamicCodeEditor.setText(SharedPreferenceUtil
				.getLatestUserPwd(getApplicationContext()));
	}

	/**
	 * 获取当前设置的电话号码
	 */
	public String getNativePhoneNumber() {
		String nativePhoneNumber = null;
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		nativePhoneNumber = telephonyManager.getLine1Number();
		return nativePhoneNumber;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		InputMethodManager manager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (getCurrentFocus() != null
					&& getCurrentFocus().getWindowToken() != null) {
				manager.hideSoftInputFromWindow(getCurrentFocus()
						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
		return super.onTouchEvent(event);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cDownButton.cancelTask();
	}
}
