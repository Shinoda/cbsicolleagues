package com.CBSi.colleague.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.CBSi.colleague.R;
import com.CBSi.colleague.common.CommonDefination;
import com.CBSi.colleague.data.DataManager;
import com.CBSi.colleague.data.SectorDataModel;
import com.CBSi.colleague.data.UserDataModel;
import com.CBSi.colleague.db.SectorDao;
import com.CBSi.colleague.util.ImageHandleUtil;
import com.CBSi.colleague.util.ImageHandleUtil.FetchPortraitCallback;
import com.CBSi.colleague.util.TelephonyUtil;

public class ContactDetailActivity extends Activity {

	/**
	 * 姓名
	 */
	private TextView nameTv;
	/**
	 * 部门
	 */
	private TextView departmentTv;
	/**
	 * 职位
	 */
	private TextView positionTv;
	/**
	 * 状态
	 */
	private TextView statusTv;
	/**
	 * 头像
	 */
	private ImageView photoIv;
	/**
	 * 手机
	 */
	private TextView cellPhoneTv;
	/**
	 * 固话
	 */
	private TextView zuoPhoneTv;
	/**
	 * 固话分机
	 */
	private TextView zuoPhoneBranchTv;
	/**
	 * 邮箱
	 */
	private TextView mailTv;
	private View backView;
	private ImageView shareBtnIv;
	private ImageView cellphoneCallBtn;
	private ImageView cellphoneSMSBtn;
	private ImageView zuojiCallBtn;
	private ImageView mailtoBtn;
	private View zuojiRootView;
	private View emailRootView;
//	private TextView backTextView;
	private TextView sectorValueTV;

	private UserDataModel curContact;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_detail_layout);
		curContact = (UserDataModel) getIntent().getSerializableExtra(
				CommonDefination.COLLEAGUE_DATA_KEY);
		initView();
		setView();
	}

	private void initView() {
		nameTv = (TextView) findViewById(R.id.name);
		departmentTv = (TextView) findViewById(R.id.department);
		positionTv = (TextView) findViewById(R.id.position);
		statusTv = (TextView) findViewById(R.id.status);
		photoIv = (ImageView) findViewById(R.id.photo);
		cellPhoneTv = (TextView) findViewById(R.id.cell_phone_value);
		zuoPhoneTv = (TextView) findViewById(R.id.zuo_phone_value);
		zuoPhoneBranchTv = (TextView) findViewById(R.id.zuo_phone_branch_value);
		mailTv = (TextView) findViewById(R.id.mail_value);
		sectorValueTV = (TextView) findViewById(R.id.detail_sector_value);
		shareBtnIv = (ImageView) findViewById(R.id.detail_share_btn);
		cellphoneCallBtn = (ImageView) findViewById(R.id.call);
		cellphoneSMSBtn = (ImageView) findViewById(R.id.sms);
		zuojiCallBtn = (ImageView) findViewById(R.id.zuo_call);
		mailtoBtn = (ImageView) findViewById(R.id.mailTo);
		zuojiRootView = findViewById(R.id.zuoji_root_layout);
		emailRootView = findViewById(R.id.email_root_layout);
		backView = findViewById(R.id.back_root_layout);

		backView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.push_right_in,
						R.anim.push_right_out);
				
			}
		});

		shareBtnIv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showShare();
				// startActivity(new
				// Intent(ContactDetailActivity.this,ShareViewActivity.class));
			}
		});

		cellphoneCallBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				TelephonyUtil.call(ContactDetailActivity.this,
						curContact.getUser_phone());

			}
		});

		cellphoneSMSBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TelephonyUtil.sendSmsMessage(ContactDetailActivity.this,
						curContact.getUser_phone());

			}
		});

		zuojiRootView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				TelephonyUtil.call(ContactDetailActivity.this,
						curContact.getUser_tel());
			}
		});

		emailRootView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				List<String> emailAddressList = new ArrayList<String>();
				emailAddressList.add(curContact.getUser_email());
				TelephonyUtil.sendMailMul(ContactDetailActivity.this,
						emailAddressList, null, null);
			}
		});

	}

	private void setView() {
		SectorDataModel sector = SectorDao.getInstance(getApplicationContext()).findSectorBySectorId(curContact
				.getSector_id());
		nameTv.setText(curContact.getUser_name());
		if (sector != null) {
			departmentTv.setText(sector.getSector_name());
			sectorValueTV.setText(sector.getSector_name());
		}
		// departmentTv.setText(curContact.getDepartment()); //----Need get data
		// from department table---<hequn>
		positionTv.setText(curContact.getUser_position());
		cellPhoneTv.setText(curContact.getUser_phone());
		zuoPhoneTv.setText(curContact.getUser_tel());
//		photoIv.setImageResource(R.drawable.default_portrait_detail);
		Bitmap bmp = ImageHandleUtil.loadLocalBitmap(this, curContact.getUser_id());
		if(bmp != null)
		{
			photoIv.setImageBitmap(bmp);
		}
		else
		{
			ImageHandleUtil.loadPortraitWithUserID(this, curContact.getUser_id(), new FetchPortraitCallback() {
		
			
			@Override
			public void onGetPortraitSuccessed(long responseUserId, Bitmap bmp) {
				if (bmp == null) {
//					int tmp = (int) (Math.random() * 5);
					bmp = BitmapFactory.decodeResource(getResources(),
							R.drawable.default_portrait /*+ tmp*/);
				}
				photoIv.setImageBitmap(bmp);
			}
			
			@Override
			public void onGetPortraitFailed(long responseUserId, String error) {
				 Bitmap bmp = BitmapFactory.decodeResource(getResources(),
						R.drawable.default_portrait /*+ tmp*/);
				 photoIv.setImageBitmap(bmp);
			}
		});
		}
		zuoPhoneBranchTv.setText(String.format(
				getResources().getString(R.string.branch_phone),
				curContact.getUser_tel_sub()));
		mailTv.setText(curContact.getUser_email());
		// qqTv.setText(curContact.getQq());
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 如果按下的是返回键，并且没有重复
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			finish();
			overridePendingTransition(R.anim.push_right_in,
					R.anim.push_right_out/*
										 * R.anim.slide_up_in,
										 * R.anim.slide_down_out
										 */);
			return false;
		}
		return false;
	}

	private void showShare() {
		ShareSDK.initSDK(this);
		OnekeyShare oks = new OnekeyShare();
		// 关闭sso授权
		oks.disableSSOWhenAuthorize();
		// 分享时Notification的图标和文字
//		oks.setNotification(R.drawable.ic_launcher,
//				getString(R.string.app_name));
		// title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		oks.setTitle(getString(R.string.share));
		// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		oks.setTitleUrl("http://www.yuntongshi.cn");
		// text是分享文本，所有平台都需要这个字段
		StringBuffer sb = new StringBuffer();
		sb.append("联系人："+curContact.getUser_name());
		sb.append("\n手机："+curContact.getUser_phone());
		if (!TextUtils.isEmpty(curContact.getUser_email())) {
			sb.append("\n邮件："+curContact.getUser_email());
		}
		if (!TextUtils.isEmpty(curContact.getUser_tel())) {
			sb.append("\n固话："+curContact.getUser_tel());
		}
		if (!TextUtils.isEmpty(curContact.getUser_tel_sub())) {
			sb.append("\n分机："+curContact.getUser_tel_sub());
		}
		
		oks.setText(sb.toString());
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		oks.setImagePath("/sdcard/test.jpg");// 确保SDcard下面存在此张图片
		
		// url仅在微信（包括好友和朋友圈）中使用
		oks.setUrl("http://www.yuntongshi.cn");
		// comment是我对这条分享的评论，仅在人人网和QQ空间使用
		oks.setComment("我是测试评论文本");
		// site是分享此内容的网站名称，仅在QQ空间使用
		oks.setSite(getString(R.string.app_name));
		// siteUrl是分享此内容的网站地址，仅在QQ空间使用
//		oks.setSiteUrl("http://sharesdk.cn");

		// 启动分享GUI
		oks.show(this);
	}
}
