package com.CBSi.colleague.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.CBSi.colleague.R;

public class UserGuideActivity extends Activity implements OnClickListener,
		OnPageChangeListener {

	private final String TAG = "UserGuideActivity";
	private ViewPager vp;

	private ViewPagerAdapter vpAdapter;

	private List<View> views;
	
	private int endCondition = 0;
	
	private ImageView enterBtn;

	// Count of view pages.----hequn
	private final int MAX_VIEW_COUNT = 3;

	// 底部小店图片

	private ImageView[] dots;

	// 记录当前选中位置

	private int currentIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.user_guide_layout);

		views = new ArrayList<View>();

		initUserGuidePage();

		vp = (ViewPager) findViewById(R.id.viewpager);

		// 初始化Adapter

		vpAdapter = new ViewPagerAdapter(views);

		vp.setAdapter(vpAdapter);

		// 绑定回调

		vp.setOnPageChangeListener(this);

		// 初始化底部小点

		initDots();

	}
	
	private void initUserGuidePage() {
		View rLayout1 = LayoutInflater.from(this).inflate(R.layout.user_guide_page1, null);
		views.add(rLayout1);
		
		View rLayout2 = LayoutInflater.from(this).inflate(R.layout.user_guide_page2, null);
		views.add(rLayout2);
		
		View rLayout3 = LayoutInflater.from(this).inflate(R.layout.user_guide_page3, null);
		views.add(rLayout3);
		
		enterBtn = (ImageView)rLayout3.findViewById(R.id.userguide_enterBtn);
		
		enterBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				enterLogin();
			}
		});
		
		enterBtn.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				enterBtn.setAlpha((event.getAction()==MotionEvent.ACTION_UP)?255:128);
				return false;
			}
		});
	}

	private void initDots() {

		LinearLayout dot_layout = (LinearLayout) findViewById(R.id.dot_layout);

		dots = new ImageView[MAX_VIEW_COUNT];

		// 循环取得小点图片

		for (int i = 0; i < MAX_VIEW_COUNT; i++) {

			// 得到一个LinearLayout下面的每一个子元素

			dots[i] = (ImageView) dot_layout.getChildAt(i);

			dots[i].setEnabled(true);// 都设为灰色

			dots[i].setOnClickListener(this);

			dots[i].setTag(i);// 设置位置tag，方便取出与当前位置对应

		}

		currentIndex = 0;

		dots[currentIndex].setEnabled(false);// 设置为白色，即选中状态

	}

	private void setCurView(int position)

	{

		if (position < 0 || position >= MAX_VIEW_COUNT) {

			return;

		}

		vp.setCurrentItem(position);

	}

	private void setCurDot(int positon)

	{

		if (positon < 0 || positon > MAX_VIEW_COUNT - 1 || currentIndex == positon) {

			return;

		}

		dots[positon].setEnabled(false);

		dots[currentIndex].setEnabled(true);

		currentIndex = positon;

	}

	private void enterLogin() {
		Intent mainIntent = new Intent(UserGuideActivity.this, LoginActivity.class); 
		UserGuideActivity.this.startActivity(mainIntent); 
		UserGuideActivity.this.finish();
	}
	// 当滑动状态改变时调用

	@Override
	public void onPageScrollStateChanged(int arg0) {

		Log.v(TAG, "onPageScrollStateChanged arg0="+arg0);
		if(arg0==0)
		{
			if(endCondition==0 && currentIndex==2)
			{
				Log.v(TAG, "I'll jump arg0="+arg0);
				
				new Handler().postDelayed(new Runnable(){ 

			         @Override 
			         public void run() { 
			        	 enterLogin();
			         } 
			            
			        }, 20); 
			}
			endCondition = 0;
		}
	}

	// 当当前页面被滑动时调用

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

		Log.v(TAG, "arg0="+arg0+" ,arg1="+arg1+" ,arg2="+arg2);
		endCondition += arg2; 
	}

	// 当新的页面被选中时调用

	@Override
	public void onPageSelected(int arg0) {

		// 设置底部小点选中状态
		
		setCurDot(arg0);
		Log.v(TAG, "onPageSelected arg0="+arg0 + "  currentIndex:"+currentIndex);
	}

	@Override
	public void onClick(View v) {

		int position = (Integer) v.getTag();

		setCurView(position);

		setCurDot(position);

	}

	public class ViewPagerAdapter extends PagerAdapter {

		// 界面列表

		private List<View> views;

		public ViewPagerAdapter(List<View> views) {

			this.views = views;

		}

		// 销毁arg1位置的界面

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {

			((ViewPager) arg0).removeView(views.get(arg1));

		}

		@Override
		public void finishUpdate(View arg0) {


		}

		// 获得当前界面数

		@Override
		public int getCount() {

			if (views != null)

			{

				return views.size();

			}

			return 0;

		}

		// 初始化arg1位置的界面

		@Override
		public Object instantiateItem(View arg0, int arg1) {

			((ViewPager) arg0).addView(views.get(arg1), 0);

			return views.get(arg1);

		}

		// 判断是否由对象生成界面

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {

			return (arg0 == arg1);

		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {


		}

		@Override
		public Parcelable saveState() {


			return null;

		}

		@Override
		public void startUpdate(View arg0) {


		}

	}

}
