//
//  SectorDAO.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/11.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "SectorDAO.h"
#import "Utils.h"

@implementation SectorDAO

+(BOOL)saveToDataBase:(NSMutableArray*)dataArray
{
    BOOL bResult = NO;
    if(dataArray == nil || dataArray.count==0)
    {
        return NO;
    }
    NSString* sql = [NSString stringWithFormat:@"REPLACE INTO sector (%@,%@,%@,%@) VALUES (?,?,?,?)",
                     FIELD_SECTOR_ID,
                     FIELD_SECTOR_NAME,
                     FIELD_CORPORATION_ID,
                     FIELD_PARENT_SECTOR_ID];
    for (SectorDataModel *s in dataArray) {
        bResult = [[CBSiDBHelper shareDB].db executeUpdate:sql,
                   s.sector_id,
                   s.sector_name,
                   s.corporation_id,
                   s.parent_sector_id];
    }
    return bResult;
}

+(SectorDataModel*)findSectorFromDB:(NSString*)sectorId
{
    SectorDataModel *sector = nil;
    NSString* sql = [NSString stringWithFormat:@"SELECT * from sector where %@ = ?", FIELD_SECTOR_ID];
    FMResultSet* rs = [[[CBSiDBHelper shareDB] db] executeQuery:sql, sectorId];
    if([rs next])
    {
        sector = [[SectorDataModel alloc] init];
        sector.sector_id = sectorId;
        sector.sector_name = [rs stringForColumn:FIELD_SECTOR_NAME];
        sector.corporation_id = [rs stringForColumn:FIELD_CORPORATION_ID];
        sector.parent_sector_id = [rs stringForColumn:FIELD_PARENT_SECTOR_ID];
    }
    return sector;
}

#if IS_PRIORITY_DISABLE
+(NSMutableArray*)loadAllSectorFromDB
{
    NSMutableArray *allSector = [[NSMutableArray alloc] init];
    NSString* corpId = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
    NSString* sql = [NSString stringWithFormat:@"SELECT * from sector where %@ = ?", FIELD_CORPORATION_ID];
    FMResultSet* rs = [[[CBSiDBHelper shareDB] db] executeQuery:sql, corpId];
    while ([rs next]) {
        SectorDataModel *s = [[SectorDataModel alloc] init];
        s.sector_id = [rs stringForColumn:FIELD_SECTOR_ID];
        s.sector_name = [rs stringForColumn:FIELD_SECTOR_NAME];
        s.corporation_id = [rs stringForColumn:FIELD_CORPORATION_ID];
        s.parent_sector_id = [rs stringForColumn:FIELD_PARENT_SECTOR_ID];
        if(s)
           [allSector addObject:s];
    }
    return allSector;
}
#endif//IS_PRIORITY_DISABLE


+(NSMutableArray*)getAllSubSectorIDs:(NSString*)sId
{
    NSString* sql = [NSString stringWithFormat:@"SELECT s1.sector_id,s2.sector_id,s3.sector_id FROM sector AS s1 LEFT JOIN sector AS s2 ON s1.sector_id=s2.parent_sector_id LEFT JOIN sector AS s3 ON s2.sector_id=s3.parent_sector_id WHERE s1.sector_id IN (%@)",sId];
    FMResultSet* rs = [[[CBSiDBHelper shareDB] db] executeQuery:sql];
    NSMutableArray* allSectorIds = [[NSMutableArray alloc] init];
    while([rs next])
    {
    for(int i=0;i<rs.columnCount;i++)
    {
        NSString* sectorId = [rs stringForColumnIndex:i];
        if(sectorId && ![allSectorIds containsObject:sectorId])
        {
            [allSectorIds addObject:sectorId];
        }
    }
    }
    return allSectorIds;
}


+(NSMutableArray*)getSectorsArrayByPriority:(NSString*)prioritySector
{
    NSString* sql = [NSString stringWithFormat:@"SELECT s1.sector_id,s2.sector_id,s3.sector_id FROM sector AS s1 LEFT JOIN sector AS s2 ON s1.sector_id=s2.parent_sector_id LEFT JOIN sector AS s3 ON s2.sector_id=s3.parent_sector_id WHERE s1.sector_id IN (%@)",prioritySector];
    FMResultSet* rs = [[[CBSiDBHelper shareDB] db] executeQuery:sql];
    NSMutableArray* allSubSectors = [[NSMutableArray alloc] init];
    NSMutableArray *allSectorIds = [[NSMutableArray alloc] init];
    while([rs next])
    {
        for(int i=0;i<rs.columnCount;i++)
        {
            NSString* sectorId = [rs stringForColumnIndex:i];
            if(sectorId && ![allSectorIds containsObject:sectorId])
            {
                [allSectorIds addObject:sectorId];
                SectorDataModel *s = [SectorDAO findSectorFromDB:sectorId];
                if(s)
                {
                    s.sLevel = i+1;
                    [allSubSectors addObject:s];
                }
            }
        }
    }
    return allSubSectors;
}
@end
