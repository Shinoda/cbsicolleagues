//
//  UserDAO.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/11.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBSiDBHelper.h"
#import "UserDataModel.h"

@interface UserDAO : NSObject

+(BOOL)saveUserToDB:(UserDataModel*)user;

+(BOOL)saveToDataBase:(NSMutableArray*)dataArray;

+(UserDataModel*)findUserFromDB:(NSString*)userId;

+(NSMutableArray*)loadUsersBySectorId:(NSString*)sectorId;
@end
