//
//  UserDAO.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/11.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "UserDAO.h"
#import "SectorDAO.h"
#import "Utils.h"
#import "CBSiDataManager.h"

@implementation UserDAO

+(BOOL)saveToDataBase:(NSMutableArray*)dataArray
{
    BOOL bResult = NO;
    if(dataArray == nil || dataArray.count==0)
    {
        return NO;
    }
    
    for (UserDataModel *u in dataArray) {
        bResult = [UserDAO saveUserToDB:u];
        NSLog(@"Save user: 'id=%@, name=%@'is %@!",u.user_id, u.user_name,bResult?@"success":@"fail");
    }
    if(bResult)
    {
        [[CBSiDataManager sharedDataManager] reloadCurrentUser];
        [[CBSiDataManager sharedDataManager] reloadAllContactsData];
    }
    return bResult;
}

+(BOOL)saveUserToDB:(UserDataModel*)user
{
    NSString* sql = [NSString stringWithFormat:@"REPLACE INTO user (%@,%@,%@,%@,%@,%@,%@,%@,%@,%@) VALUES (?,?,?,?,?,?,?,?,?,?)",
                     FIELD_USER_ID,
                     FIELD_USER_NAME,
                     FIELD_DEPARTMENT_NAME,
                     FIELD_USER_POSITION,
                     FIELD_USER_PHONE,
                     FIELD_USER_EMAIL,
                     FIELD_USER_TEL,
                     FIELD_USER_TEL_SUB,
                     FIELD_SECTOR_ID,
                     FIELD_USER_PRIORITY];
    BOOL bResult = [[CBSiDBHelper shareDB].db executeUpdate:sql
               ,user.user_id,
               user.user_name,
               user.departmentName,
               user.user_position,
               user.user_phone,
               user.user_email,
               user.user_tel,
               user.user_tel_sub,
               user.sector_id,
               user.priority_sector];
    return bResult;
}

+(UserDataModel*)findUserFromDB:(NSString*)userId
{
    UserDataModel *user = nil;
    if(userId == nil)
    {
        return user;
    }
    NSString* sql = @"SELECT * from user where user_id = ?";
    FMResultSet* rs = [[[CBSiDBHelper shareDB] db] executeQuery:sql, userId];
    if([rs next])
    {
        user = [[UserDataModel alloc] init];
        user.user_id = userId;
        user.user_name = [rs stringForColumn:FIELD_USER_NAME];
        user.departmentName = [rs stringForColumn:FIELD_DEPARTMENT_NAME];
        user.user_position = [rs stringForColumn:FIELD_USER_POSITION];
        user.user_phone = [rs stringForColumn:FIELD_USER_PHONE];
        user.user_email = [rs stringForColumn:FIELD_USER_EMAIL];
        user.user_tel = [rs stringForColumn:FIELD_USER_TEL];
        user.user_tel_sub = [rs stringForColumn:FIELD_USER_TEL_SUB];
        user.sector_id = [rs stringForColumn:FIELD_SECTOR_ID];
        user.priority_sector = [rs stringForColumn:FIELD_USER_PRIORITY];
    }
    return user;
}

+(NSMutableArray*)loadUsersBySectorId:(NSString*)sectorId
{
    NSMutableArray *allUserData = [[NSMutableArray alloc] init];
    NSMutableArray *allSectorIDstr = [SectorDAO getAllSubSectorIDs:sectorId];
    NSString *allSubSectorID = [allSectorIDstr componentsJoinedByString:@","];//@"";
//    for(NSString *sid in allSectorIDstr)
//    {
//        allSubSectorID = [allSubSectorID stringByAppendingFormat:@"%@,",sid];
//    }
//    allSubSectorID = [allSubSectorID substringWithRange:NSMakeRange(0, allSubSectorID.length-1)];
    NSString* sql = [NSString stringWithFormat:@"select user.*,sector.sector_name from user,sector where user.sector_id in (%@) and user.sector_id=sector.sector_id", allSubSectorID];
    FMResultSet* rs = [[[CBSiDBHelper shareDB] db] executeQuery:sql];
    while ([rs next]) {
        UserDataModel *user = [[UserDataModel alloc] init];
        user.user_id = [rs stringForColumn:FIELD_USER_ID];
        user.user_name = [rs stringForColumn:FIELD_USER_NAME];
        user.departmentName = [rs stringForColumn:FIELD_DEPARTMENT_NAME];
        user.user_position = [rs stringForColumn:FIELD_USER_POSITION];
        user.user_phone = [rs stringForColumn:FIELD_USER_PHONE];
        user.user_email = [rs stringForColumn:FIELD_USER_EMAIL];
        user.user_tel = [rs stringForColumn:FIELD_USER_TEL];
        user.user_tel_sub = [rs stringForColumn:FIELD_USER_TEL_SUB];
        user.sector_id = [rs stringForColumn:FIELD_SECTOR_ID];
        user.priority_sector = [rs stringForColumn:FIELD_USER_PRIORITY];
        if(user)
        {
            [allUserData addObject:user];
        }
    }
    
    return allUserData;
}

@end
