//
//  CBSiDBHelper.h
//  CBSiDBHelper
//
//  Created by Heq.Shinoda on 15/3/24.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

static NSString *TABLE_NAME_USER = @"user";
static NSString *TABLE_NAME_SECTOR = @"sector";

static NSString *FIELD_USER_ID = @"user_id";
static NSString *FIELD_SECTOR_ID = @"sector_id";
static NSString *FIELD_USER_NAME = @"user_name";
static NSString *FIELD_DEPARTMENT_NAME = @"department";
static NSString *FIELD_USER_POSITION = @"user_position";
static NSString *FIELD_USER_PHONE = @"user_phone";
static NSString *FIELD_USER_TEL = @"user_tel";
static NSString *FIELD_USER_TEL_SUB = @"user_tel_sub";
static NSString *FIELD_USER_EMAIL = @"user_email";
static NSString *FIELD_USER_PRIORITY = @"priority_sector";

//static NSString *FIELD_SECTOR_ID = @"sector_id";
static NSString *FIELD_SECTOR_NAME = @"sector_name";
static NSString *FIELD_CORPORATION_ID = @"corporation_id";
static NSString *FIELD_PARENT_SECTOR_ID = @"parent_sector_id";


@interface CBSiDBHelper : NSObject
@property (nonatomic, retain) FMDatabase *db;

+(CBSiDBHelper*)shareDB;

-(BOOL)loadDB;

//----Default table-structure in DBFile.
-(BOOL)createDefaultTable;
-(BOOL)createTable:(NSString*)tableName sqlCMD:(NSString*)createTableSQL;

@end
