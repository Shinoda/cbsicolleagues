//
//  SectorDAO.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/11.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBSiDBHelper.h"
#import "SectorDataModel.h"


@interface SectorDAO : NSObject

+(BOOL)saveToDataBase:(NSMutableArray*)dataArray;

+(SectorDataModel*)findSectorFromDB:(NSString*)sectorId;
#if IS_PRIORITY_DISABLE
+(NSMutableArray*)loadAllSectorFromDB;
#endif//IS_PRIORITY_DISABLE
+(NSMutableArray*)getAllSubSectorIDs:(NSString*)sId;

+(NSMutableArray*)getSectorsArrayByPriority:(NSString*)prioritySector;
@end
