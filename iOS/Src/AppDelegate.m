//
//  AppDelegate.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/2/28.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "AppDelegate.h"
#import "CBSiDBHelper.h"
#import "CBSiHttpConnection.h"
#import "MMDrawerController.h"
#import "LeftDeptViewController.h"
#import "ContactListViewController.h"
#import "LoginViewController.h"
#import "UserDAO.h"
#import "Utils.h"
#import "GuideViewController.h"

#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>

#import <dlfcn.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self initShareSDK];
    [[CBSiDBHelper shareDB] loadDB];
    
    [self appLaunched];
#if ENABLE_TEST_CODE
    [self loadReveal];
#endif//ENABLE_TEST_CODE
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}

-(void)appLaunched
{
#if ENABLE_TEST_CODE
    [self initMainView];
#else
    if([[Utils loadDefaultConfigWith:CONFIG_APP_IS_FIRST_RUN] isEqualToString:@"1"])
    {
        [self appLaunchedByLogin];
    }
    else
    {
        [self initGuideView];
    }
#endif//ENABLE_TEST_CODE
}

-(void)appLaunchedByLogin
{
    [self initLoginView];
    [self performSelector:@selector(loginWithLastestUser) withObject:nil afterDelay:0.1];
//    UserDataModel* lastUser = [UserDAO findUserFromDB:[Utils loadDefaultConfigWith:CONFIG_INFO_USERID_KEY]];
//    if(lastUser)
//    {
//        [self initMainView];
//    }
//    else
//    {
//        [self initLoginView];
//    }
}

-(void)initLoginView {
    UIViewController* loginV = nil;
    if(IS_IPHONE4 || IS_IPHONE3)
    {
        loginV = [[LoginViewController alloc] initWithNibName:@"LoginViewController_ip4" bundle:nil];
    }
    else
        loginV = [[LoginViewController alloc] init];
    loginVC = (LoginViewController*)loginV;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:loginV];
    navigationController.navigationBarHidden = YES;
    [self.window setRootViewController:navigationController];

    [self.window makeKeyAndVisible];
}

-(void)loginWithLastestUser
{
    UserDataModel* lastUser = [UserDAO findUserFromDB:[Utils loadDefaultConfigWith:CONFIG_INFO_USERID_KEY]];
    if(lastUser!=nil && loginVC!=nil)
    {
        [loginVC loginToServer:lastUser.user_phone verifyCode:@"6666"];
    }
}

-(void)initMainView{
    
    UIViewController * leftDeptVC = [[LeftDeptViewController alloc] init];
    
    UIViewController * contactListVC = [[ContactListViewController alloc] init];
    
//    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:contactListVC];
    
    MMDrawerController * drawerController = [[MMDrawerController alloc]
                                             initWithCenterViewController:contactListVC
                                             leftDrawerViewController:leftDeptVC
                                             rightDrawerViewController:nil];
    [drawerController setMaximumRightDrawerWidth:200.0];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    //    [drawerController
    //     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
    //         MMDrawerControllerDrawerVisualStateBlock block;
    //         block = [[MMExampleDrawerVisualStateManager sharedManager]
    //                  drawerVisualStateBlockForDrawerSide:drawerSide];
    //         if(block){
    //             block(drawerController, drawerSide, percentVisible);
    //         }
    //     }];
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:drawerController];
    
    [self.window setRootViewController:navigationController];
    
    // Override point for customization after application launch.
//    self.window.backgroundColor = [UIColor whiteColor];
    [navigationController setNavigationBarHidden:YES];
    [self.window makeKeyAndVisible];
}

-(void)initShareSDK
{
    //1.初始化ShareSDK应用,字符串是http://www.mob.com/后台申请的ShareSDK应用的Appkey
    [ShareSDK registerApp:@"531d4db590e5"];
    
    
    //连接短信分享
    [ShareSDK connectSMS];
    
    /**
     连接微信应用以使用相关功能，此应用需要引用WeChatConnection.framework和微信官方SDK
     http://open.weixin.qq.com上注册应用，并将相关信息填写以下字段
     **/
    //    [ShareSDK connectWeChatWithAppId:@"wx4868b35061f87885" wechatCls:[WXApi class]];
    [ShareSDK connectWeChatSessionWithAppId:@"wx92eae0b9d4f65732"
                           appSecret:@"1c59ef7828bdd23d16769eb0554de162"
                           wechatCls:[WXApi class]];
    /**
     连接QQ应用以使用相关功能，此应用需要引用QQConnection.framework和QQApi.framework库
     http://mobile.qq.com/api/上注册应用，并将相关信息填写到以下字段
     **/
    //旧版中申请的AppId（如：QQxxxxxx类型），可以通过下面方法进行初始化
    //    [ShareSDK connectQQWithAppId:@"QQ075BCD15" qqApiCls:[QQApi class]];
//    AppId="1104473599"
//    AppKey="icSCcRkpw6Wk9RkK"

    [ShareSDK connectQQWithQZoneAppKey:@"1104473599"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //连接邮件
    [ShareSDK connectMail];
    
    //连接打印
//    [ShareSDK connectAirPrint];
    
    //连接拷贝
    [ShareSDK connectCopy];
}

-(void)initGuideView
{
    UIViewController *guide = nil;//[[GuideViewController alloc] init];
    if (IS_IPHONE4 || IS_IPHONE3) {
        guide = [[GuideViewController alloc] initWithNibName:@"GuideViewController_ip4" bundle:nil];
    }
    else if(IS_IPHONE5 || iOS7)
    {
        guide = [[GuideViewController alloc] initWithNibName:@"GuideViewController_ip5" bundle:nil];
    }
    else
    {
        guide = [[GuideViewController alloc] init];
    }
//    UIViewController* loginV = nil;
//    if(IS_IPHONE4)
//    {
//        loginV = [[LoginViewController alloc] initWithNibName:@"LoginViewController_ip4" bundle:nil];
//    }
//    else
//        loginV = [[LoginViewController alloc] init];
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:guide];
    navigationController.navigationBarHidden = YES;
    [self.window setRootViewController:navigationController];
    
    [self.window makeKeyAndVisible];
}

#pragma mark - Reveal


- (void)loadReveal
{
#if ENABLE_REVEAL_DEBUG
    if (NSClassFromString(@"IBARevealLoader") == nil)
    {
        NSString *revealLibName = @"libReveal";
        NSString *revealLibExtension = @"dylib";
        NSString *error;
        NSString *dyLibPath = [[NSBundle mainBundle] pathForResource:revealLibName ofType:revealLibExtension];
        if (dyLibPath != nil)
        {
            NSLog(@"Loading dynamic library: %@", dyLibPath);
            void *revealLib = dlopen([dyLibPath cStringUsingEncoding:NSUTF8StringEncoding], RTLD_NOW);
            if (revealLib == NULL)
            {
                error = [NSString stringWithUTF8String:dlerror()];
            }
        }
        else
        {
            error = @"File not found.";
        }
        
        if (error != nil)
        {
            NSString *message = [NSString stringWithFormat:@"%@.%@ failed to load with error: %@", revealLibName, revealLibExtension, error];
            [[[UIAlertView alloc] initWithTitle:@"Reveal library could not be loaded" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
#endif//ENABLE_REVEAL_DEBUG
}
@end
