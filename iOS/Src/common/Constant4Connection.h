//
//  Constant4Connection.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/19.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#ifndef CBSiColleague_Constant4Connection_h
#define CBSiColleague_Constant4Connection_h

/**
 Server environment defined.
 */

//#define CBSI_SERVER_PRODUCTION @"192.168.17.131:8080" //----Home product environment
//#define CBSI_SERVER_DEVELOPMENT @"192.168.1.106:8080"//----Home develop environment
#define CBSI_CO_SERVER_PRODUCTION @"10.19.12.47:8080"//----Company product environment
#define CBSI_CO_SERVER_YLY_PRODUCTION @"192.168.12.148:7070"//----Company product environment
#define CBSI_CO_SERVER_DEVELOPMENT @"10.19.8.101:8080"//----Company develop environment
#define CBSI_CO_SERVER_ONLINE @"123.56.131.79"//----Aliyun online environment

#define CBSI_SERVER_URL [NSString stringWithFormat:@"%@/CBSiCContactWebService", CBSI_CO_SERVER_ONLINE]

#define CBSI_SERVER_INTERFACE_LOGIN         @"loginAction"
#define CBSI_SERVER_INTERFACE_LOADCONTACT   @"loadContact"
#define CBSI_SERVER_INTERFACE_LOADSECTOR    @"loadSector"
#define CBSI_SERVER_INTERFACE_MODIFYUSERINFO    @"modifyUserInfo"
#define CBSI_SERVER_INTERFACE_UPDALOAD_PORTRAIT @"uploadPortrait"
#define CBSI_SERVER_INTERFACE_GET_PORTRAIT      @"getUserPortrait"
#define CBSI_SERVER_INTERFACE_SEND_SMS          @"sendsms"
#define CBSI_SERVER_INTERFACE_GET_CORPATION     @"getCorporation"


#endif
