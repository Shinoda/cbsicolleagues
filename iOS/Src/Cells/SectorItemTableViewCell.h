//
//  SectorTableViewCell.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/10.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectorItemTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *sectorNameLabel;
@property (assign, nonatomic) int iLevel;
@end
