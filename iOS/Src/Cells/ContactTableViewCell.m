//
//  ContactTableViewCell.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/3.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "ContactTableViewCell.h"

@implementation ContactTableViewCell

@synthesize portrait, portraitBg, detailBtn, userNameLabel, userTitleLabel,isCellDarkBg;

- (void)awakeFromNib {
    // Initialization code
}

-(void)updateViewForCell:(NSString*)userName userTitle:(NSString*)userTitle
{
    if(userName)
        self.userNameLabel.text = userName;
    if(userTitle)
        self.userTitleLabel.text = userTitle;
}

- (IBAction)enterUserDetail:(id)sender {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if(isCellDarkBg)
    {
//        [self.portraitBg setImage:[UIImage imageNamed:(highlighted?@"portrait_dark_select.png":@"portrait_dark_normal.png")]];
        [self performSelectorOnMainThread:@selector(updateDarkPortraitBg:) withObject:[NSString stringWithFormat:@"%d", (int)highlighted] waitUntilDone:NO];
    }
    else
    {
//        [self.portraitBg setImage:[UIImage imageNamed:(highlighted?@"portrait_light_select.png":@"portrait_light_normal.png")]];
        [self performSelectorOnMainThread:@selector(updateLightPortraitBg:) withObject:[NSString stringWithFormat:@"%d", (int)highlighted] waitUntilDone:NO];
    }
}


-(void)updateDarkPortraitBg:(NSString*)isSelected
{
    self.backgroundColor = ([isSelected integerValue]==1)?HEXCOLOR(0x323238):HEXCOLOR(0x3e3e46);
    [self.portraitBg setImage:[UIImage imageNamed:([isSelected integerValue]==1?@"portrait_dark_select.png":@"portrait_dark_normal.png")]];
}

-(void)updateLightPortraitBg:(NSString*)isSelected
{
    self.backgroundColor = ([isSelected integerValue]==1)?HEXCOLOR(0x41424D):HEXCOLOR(0x53545f);
    [self.portraitBg setImage:[UIImage imageNamed:([isSelected integerValue]==1?@"portrait_light_select.png":@"portrait_light_normal.png")]];
}
@end
