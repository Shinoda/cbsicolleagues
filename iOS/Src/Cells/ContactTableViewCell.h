//
//  ContactTableViewCell.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/3.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *portrait;
@property (strong, nonatomic) IBOutlet UIButton *detailBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *userTitleLabel;

@property (strong, nonatomic) IBOutlet UIImageView* portraitBg;

@property (assign, nonatomic) BOOL isCellDarkBg;
-(void)updateViewForCell:(NSString*)userName userTitle:(NSString*)userTitle;
@end
