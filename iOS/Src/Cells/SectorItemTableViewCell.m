//
//  SectorTableViewCell.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/10.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "SectorItemTableViewCell.h"

@implementation SectorItemTableViewCell
@synthesize sectorNameLabel;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected)
    {
        [sectorNameLabel setTextColor:HEXCOLOR(0x6a424c)];
    }
    else
    {
        [sectorNameLabel setTextColor:HEXCOLOR(0xffffff)];
    }
}

@end
