//
//  AppDelegate.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/2/28.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LoginViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    LoginViewController *loginVC;
}

@property (strong, nonatomic) UIWindow *window;
-(void)appLaunchedByLogin;
-(void)initLoginView;
-(void)initMainView;
@end

