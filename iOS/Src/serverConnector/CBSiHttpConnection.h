//
//  CBSiHttpConnection.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/2.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKNetworkKit.h"

typedef void (^RequestComplete)(NSMutableDictionary *responseContent);
typedef void (^RequestError)(NSString *errorInfo);


@interface CBSiHttpConnection : NSObject
{
    MKNetworkEngine *engine;
}

@property(nonatomic, assign) BOOL hasError;
@property (nonatomic, assign) NSInteger statusCode;

+(CBSiHttpConnection*)sharedInstance;
-(void)cancelRequest;

-(void)httpRequestViaPOST:(NSMutableDictionary*)params apiUrl:(NSString*)apiUrl completion:(RequestComplete)completion error:(RequestError)error;
-(void)httpRequestViaGET:(NSMutableDictionary*)params apiUrl:(NSString*)apiUrl  completion:(RequestComplete)completion error:(RequestError)error;

@end