//
//  CBSiHttpConnection.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/2.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "CBSiHttpConnection.h"


static CBSiHttpConnection* pInstance;

@implementation CBSiHttpConnection
@synthesize
hasError,
statusCode;

+(CBSiHttpConnection*)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(pInstance == nil)
        {
            pInstance = [[CBSiHttpConnection alloc] init];
        }
    });
    return pInstance;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.hasError = NO;
        self.statusCode = 0;
        engine = [[MKNetworkEngine alloc] initWithHostName:CBSI_SERVER_URL customHeaderFields:nil];
    }
    return self;
}

-(void)cancelRequest
{
    [engine cancelAllOperations];
}

-(void)httpRequestViaGET:(NSMutableDictionary*)params apiUrl:(NSString*)apiUrl completion:(RequestComplete)completion error:(RequestError)resError
{
    MKNetworkOperation *op = [engine operationWithPath:apiUrl params:params httpMethod:@"GET" ssl:NO];
    [op addCompletionHandler:^(MKNetworkOperation *operation) {
        self.hasError = NO;
        self.statusCode = operation.HTTPStatusCode;
        NSLog(@"GET Request from server Url:%@\n statusCode:%d",operation.readonlyRequest.URL.absoluteString, (int)self.statusCode);
        NSError *error = nil;
        NSMutableDictionary* dict = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:kNilOptions error:&error];
        if(dict)
        {
            completion(dict);
        }
        else
        {
            resError(@"Get server-data error!");
        }
    }errorHandler:^(MKNetworkOperation *errorOp, NSError* err) {
        self.hasError = YES;
        NSLog(@"MKNetwork GET request error : %@", [err localizedDescription]);
        resError([err localizedDescription]);
    }];
    [engine enqueueOperation:op];
}

-(void)httpRequestViaPOST:(NSMutableDictionary*)params apiUrl:(NSString*)apiUrl completion:(RequestComplete)completion error:(RequestError)resError
{
    MKNetworkOperation *op = [engine operationWithPath:apiUrl params:params httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *operation) {
        self.hasError = NO;
        self.statusCode = operation.HTTPStatusCode;
        NSLog(@"POST Request from server Url:%@\n statusCode:%d",operation.readonlyRequest.URL.absoluteString, (int)self.statusCode);
        NSError *error = nil;
        NSMutableDictionary* dict = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:kNilOptions error:&error];
        //-----Connect to server success----//
        if(dict)
        {
            completion(dict);
        }
        else
        {
            resError(@"Post server-data error!");
        }
    }errorHandler:^(MKNetworkOperation *errorOp, NSError* err) {
        self.hasError = YES;
        NSLog(@"MKNetwork POST request error : %@", [err localizedDescription]);
        resError([err localizedDescription]);
    }];
    [engine enqueueOperation:op];
}

@end
