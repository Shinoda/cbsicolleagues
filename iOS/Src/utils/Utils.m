//
//  Utils.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/19.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "Utils.h"
#import "PinYinForObjc.h"
#import "ChineseInclude.h"
//#import <objc/runtime.h>

static const char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

@implementation Utils

+(void) saveLoginInfo:(NSString*)userId corpId:(NSString*)corpId hasLogin:(BOOL)hasLogin phone:(NSString*)phone
{
    NSUserDefaults *defaultSaver = [NSUserDefaults standardUserDefaults];
    [defaultSaver setObject:userId forKey:CONFIG_INFO_USERID_KEY];
    [defaultSaver setObject:corpId forKey:CONFIG_INFO_CORPID_KEY];
    [defaultSaver setObject:phone forKey:CONFIG_INFO_USER_PHONE_KEY];
    [defaultSaver setObject:[NSString stringWithFormat:@"%d",(hasLogin?1:0)] forKey:CONFIG_INFO_HAS_LOGINED_KEY];
    [defaultSaver synchronize];
}


+(void)saveDefaultConfigWith:(NSString*)key value:(NSString*)value
{
    NSUserDefaults *defaultSaver = [NSUserDefaults standardUserDefaults];
    [defaultSaver setObject:value forKey:key];
    [defaultSaver synchronize];
}

+(NSString*)loadDefaultConfigWith:(NSString*)key
{
    NSUserDefaults *defaultSaver = [NSUserDefaults standardUserDefaults];
    NSString* value = [defaultSaver objectForKey:key];
    return value;
}

/******************************************************************************
 函数名称 : + (NSData *)dataWithBase64EncodedString:(NSString *)string
 函数描述 : base64格式字符串转换为文本数据
 输入参数 : (NSString *)string
 输出参数 : N/A
 返回参数 : (NSData *)
 备注信息 :
 ******************************************************************************/
+ (NSData *)dataWithBase64EncodedString:(NSString *)string
{
    if (string == nil)
        [NSException raise:NSInvalidArgumentException format:nil];
    if ([string length] == 0)
        return [NSData data];
    
    static char *decodingTable = NULL;
    if (decodingTable == NULL)
    {
        decodingTable = malloc(256);
        if (decodingTable == NULL)
            return nil;
        memset(decodingTable, CHAR_MAX, 256);
        NSUInteger i;
        for (i = 0; i < 64; i++)
            decodingTable[(short)encodingTable[i]] = i;
    }
    
    const char *characters = [string cStringUsingEncoding:NSASCIIStringEncoding];
    if (characters == NULL)     //  Not an ASCII string!
        return nil;
    char *bytes = malloc((([string length] + 3) / 4) * 3);
    if (bytes == NULL)
        return nil;
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while (YES)
    {
        char buffer[4];
        short bufferLength;
        for (bufferLength = 0; bufferLength < 4; i++)
        {
            if (characters[i] == '\0')
                break;
            if (isspace(characters[i]) || characters[i] == '=')
                continue;
            buffer[bufferLength] = decodingTable[(short)characters[i]];
            if (buffer[bufferLength++] == CHAR_MAX)      //  Illegal character!
            {
                free(bytes);
                return nil;
            }
        }
        
        if (bufferLength == 0)
            break;
        if (bufferLength == 1)      //  At least two characters are needed to produce one byte!
        {
            free(bytes);
            return nil;
        }
        
        //  Decode the characters in the buffer to bytes.
        bytes[length++] = (buffer[0] << 2) | (buffer[1] >> 4);
        if (bufferLength > 2)
            bytes[length++] = (buffer[1] << 4) | (buffer[2] >> 2);
        if (bufferLength > 3)
            bytes[length++] = (buffer[2] << 6) | buffer[3];
    }
    
    bytes = realloc(bytes, length);
    return [NSData dataWithBytesNoCopy:bytes length:length];
}

/******************************************************************************
 函数名称 : + (NSString *)base64EncodedStringFrom:(NSData *)data
 函数描述 : 文本数据转换为base64格式字符串
 输入参数 : (NSData *)data
 输出参数 : N/A
 返回参数 : (NSString *)
 备注信息 :
 ******************************************************************************/
+ (NSString *)base64EncodedStringFrom:(NSData *)data
{
    if ([data length] == 0)
        return @"";
    
    char *characters = malloc((([data length] + 2) / 3) * 4);
    if (characters == NULL)
        return nil;
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while (i < [data length])
    {
        char buffer[3] = {0,0,0};
        short bufferLength = 0;
        while (bufferLength < 3 && i < [data length])
            buffer[bufferLength++] = ((char *)[data bytes])[i++];
        
        //  Encode the bytes in the buffer to four characters, including padding "=" characters if necessary.
        characters[length++] = encodingTable[(buffer[0] & 0xFC) >> 2];
        characters[length++] = encodingTable[((buffer[0] & 0x03) << 4) | ((buffer[1] & 0xF0) >> 4)];
        if (bufferLength > 1)
            characters[length++] = encodingTable[((buffer[1] & 0x0F) << 2) | ((buffer[2] & 0xC0) >> 6)];
        else characters[length++] = '=';
        if (bufferLength > 2)
            characters[length++] = encodingTable[buffer[2] & 0x3F];
        else characters[length++] = '=';
    }
    NSString* base64Str = [[NSString alloc] initWithBytesNoCopy:characters length:length encoding:NSASCIIStringEncoding freeWhenDone:YES];
    return base64Str;
}

+ (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize

{
    
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

+ (UIImage*)imageByScalingAndCropSize:(UIImage *)image targetSize:(CGSize)targetSize
{
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(scaledWidth, scaledHeight)); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 *  等比例缩放
 *
 *  @param size   尺寸
 *  @param origin 原图
 *
 *  @return 缩放后的图片
 */
+(UIImage*)scaleToSize:(CGSize)size original:(UIImage*)origin
{
    CGFloat width = CGImageGetWidth(origin.CGImage);
    CGFloat height = CGImageGetHeight(origin.CGImage);
    
    float verticalRadio = size.height*1.0/height;
    float horizontalRadio = size.width*1.0/width;
    
    float radio = 1;
    if(verticalRadio>1 && horizontalRadio>1)
    {
        radio = verticalRadio > horizontalRadio ? horizontalRadio : verticalRadio;
    }
    else
    {
        radio = verticalRadio < horizontalRadio ? verticalRadio : horizontalRadio;
    }
    
    width = width*radio;
    height = height*radio;
    
    int xPos = (size.width - width)/2;
    int yPos = (size.height-height)/2;
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    
    // 绘制改变大小的图片
    [origin drawInRect:CGRectMake(xPos, yPos, width, height)];
    
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    
    // 返回新的改变大小后的图片
    return scaledImage;
}

+ (NSData *)compressedImageWithMaxDataLength:(UIImage*)image maxDataLength:(CGFloat)maxDataLength {
    
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    NSUInteger dataLength = [data length];
    
    if(dataLength > maxDataLength)
    {
        return UIImageJPEGRepresentation(image, maxDataLength / dataLength);
    } else {
        return data;
    }
}

+ (NSData*)compressedImageAndScalingSize:(UIImage*)image targetSize:(CGSize)targetSize maxDataLen:(CGFloat)maxDataLen
{
    UIImage* reScaledImage = [Utils imageByScalingAndCropSize:image targetSize:targetSize];
    
    
    return [Utils compressedImageWithMaxDataLength:reScaledImage maxDataLength:maxDataLen];
}

+ (NSData*)compressedImageAndScalingSize:(UIImage*)image targetSize:(CGSize)targetSize percent:(CGFloat)percent
{
    
    UIImage* reScaledImage = [Utils imageByScalingAndCropSize:image targetSize:targetSize];
    return UIImageJPEGRepresentation(reScaledImage, percent);
}

+(NSMutableArray*)sortContactData:(NSMutableArray*)allData
{
//    NSMutableArray* sortArray = [[NSMutableArray alloc] init];
    for(UserDataModel* u in allData)
    {
        if ([ChineseInclude isIncludeChineseInString:u.user_name]) {
            NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:[u.user_name substringToIndex:1]];
            u.pinyinKey = [tempPinYinHeadStr uppercaseString];
        }
        else
        {
            u.pinyinKey = [[u.user_name substringToIndex:1] uppercaseString];
        }
    }
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinyinKey" ascending:YES]];
    [allData sortUsingDescriptors:sortDescriptors];
    

    return allData;
}

static MBProgressHUD *HUD;
+(void)showTips:(NSString*)tips view:(UIView*)curView
{
    [Utils closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:curView];
    [curView addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
//    HUD.delegate = self;
    HUD.labelText = tips;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.4];
}

+(void)showLoadingTips:(NSString*)tips view:(UIView*)curView
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:curView];
    [curView addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.dimBackground = YES;
    
//    HUD.delegate = self;
    HUD.labelText = tips;
    
    [HUD show:YES];
}

+(void)closeHud
{
    if(HUD)
    {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}
@end
