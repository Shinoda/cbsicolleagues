//
//  PortraitImageHandler.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/27.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^GetPortraitImageCompletion)(NSString* userId, UIImage* portrait);

@interface PortraitImageHandler : NSObject
{
    NSString* portraitRootPath;
}


+(PortraitImageHandler*)sharedPIHandler;
-(void)getPortraitImageWithUserId:(NSString*)userId sectorId:(NSString*)sectorId complete:(GetPortraitImageCompletion)complete;

-(void)cleanAllPortraitCache;

-(void)removePortraitWithUserId:(NSString*)userId sectorId:(NSString*)sectorId;

-(void)savePortraitToCacheWithUserId:(NSString *)userId sectorId:(NSString *)sectorId portrait:(UIImage*)portrait;
@end
