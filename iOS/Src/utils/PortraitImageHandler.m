//
//  PortraitImageHandler.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/27.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "PortraitImageHandler.h"
#import "CBSiHttpConnection.h"
#import "Utils.h"
#import "CBSICache.h"

#define PATH_USER_PORTRAIT_DIRECTORY @"CBSiPortrait"
#define DEFAULT_PORTRAIT_IMG_NAME @"head.jpg"

static PortraitImageHandler* pInstance = nil;


@implementation PortraitImageHandler

+(PortraitImageHandler*)sharedPIHandler
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(pInstance == nil)
        {
            pInstance = [[PortraitImageHandler alloc] init];
        }
    });
    return pInstance;
}

-(void)getPortraitImageWithUserId:(NSString *)userId sectorId:(NSString*)sectorId complete:(GetPortraitImageCompletion)complete
{
    UIImage* localImg = [[CBSICache currentCache] imageForKey:[sectorId stringByAppendingPathComponent:userId]];//[self getLocalDiskImage:userId sectorId:sectorId];
    if(localImg)
    {
        complete(userId, localImg);
        return;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:userId forKey:@"userid"];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_GET_PORTRAIT completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"getPortraitImageWithUserId responsed: %@", responseContent);
        if([responseContent isKindOfClass:[NSMutableDictionary class]])
        {
            NSString* uId = [responseContent objectForKey:@"userid"];
            if([[responseContent objectForKey:@"getUserPortrait"] integerValue]==0)
            {
                NSString* base64Str = [responseContent objectForKey:@"image"];//[Utils dataWithBase64EncodedString:base64Str]
                UIImage* pImage = [UIImage imageWithData:[NSData dataFromBase64String:base64Str]];
                complete(uId, pImage);
                [[CBSICache currentCache] setImage:pImage forKey:[sectorId stringByAppendingPathComponent:uId]];
            }
            else
            {
                complete(uId, nil);
            }
        }
    } error:^(NSString *errorInfo) {
        NSLog(@"getPortraitImageWithUserId error: %@",errorInfo);
        complete(userId, nil);
    }];
}

-(void)cleanAllPortraitCache
{
    [[CBSICache globalCache] clearCache];
}

-(void)removePortraitWithUserId:(NSString *)userId sectorId:(NSString *)sectorId
{
    NSString* cacheKey = [sectorId stringByAppendingPathComponent:userId];
    [[CBSICache globalCache] removeCacheForKey:cacheKey];
}

-(void)savePortraitToCacheWithUserId:(NSString *)userId sectorId:(NSString *)sectorId portrait:(UIImage*)portrait
{
    NSString* cacheKey = [sectorId stringByAppendingPathComponent:userId];
    [[CBSICache globalCache] setImage:portrait forKey:cacheKey];
}

//-(UIImage*)getLocalDiskImage:(NSString*)userId sectorId:(NSString*)sectorId
//{
//    NSString* portraitPath = [[[portraitRootPath stringByAppendingPathComponent:sectorId] stringByAppendingPathComponent:userId] stringByAppendingPathComponent:DEFAULT_PORTRAIT_IMG_NAME];
//    UIImage* localImg = [UIImage imageWithContentsOfFile:portraitPath];
//    return localImg;
//}
//
//-(void)createPortraitPath//:(NSString*)corpid
//{
//    NSString* cachesDirectory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
//    NSString* corpid = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
//    NSString* portraitDirectory = [[[cachesDirectory stringByAppendingPathComponent:[[NSProcessInfo processInfo] processName]] stringByAppendingPathComponent:PATH_USER_PORTRAIT_DIRECTORY] copy];
//    portraitDirectory = [[portraitDirectory stringByAppendingPathComponent:corpid] copy];
//    
//    if([[NSFileManager defaultManager] fileExistsAtPath:portraitDirectory]) {
//        [[NSFileManager defaultManager] removeItemAtPath:portraitDirectory error:NULL];
//    }
//    portraitRootPath = portraitDirectory;
//}

@end