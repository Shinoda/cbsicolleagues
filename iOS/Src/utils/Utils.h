//
//  Utils.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/19.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDAO.h"
#import "MBProgressHUD.h"

@interface Utils : NSObject
+(void) saveLoginInfo:(NSString*)userId corpId:(NSString*)corpId hasLogin:(BOOL)hasLogin phone:(NSString*)phone;

+(void)saveDefaultConfigWith:(NSString*)key value:(NSString*)value;
+(NSString*)loadDefaultConfigWith:(NSString*)key;

//Base64 Encode & Decode
/******************************************************************************
 函数名称 : + (NSData *)dataWithBase64EncodedString:(NSString *)string
 函数描述 : base64格式字符串转换为文本数据
 输入参数 : (NSString *)string
 输出参数 : N/A
 返回参数 : (NSData *)
 备注信息 :
 ******************************************************************************/
+ (NSData *)dataWithBase64EncodedString:(NSString *)string;
/******************************************************************************
 函数名称 : + (NSString *)base64EncodedStringFrom:(NSData *)data
 函数描述 : 文本数据转换为base64格式字符串
 输入参数 : (NSData *)data
 输出参数 : N/A
 返回参数 : (NSString *)
 备注信息 :
 ******************************************************************************/
+ (NSString *)base64EncodedStringFrom:(NSData *)data;


+ (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize;
+ (UIImage*)imageByScalingAndCropSize:(UIImage *)image targetSize:(CGSize)targetSize;
+ (NSData *)compressedImageWithMaxDataLength:(UIImage*)image maxDataLength:(CGFloat)maxDataLength;
+ (NSData*)compressedImageAndScalingSize:(UIImage*)image targetSize:(CGSize)targetSize maxDataLen:(CGFloat)maxDataLen;
+ (NSData*)compressedImageAndScalingSize:(UIImage*)image targetSize:(CGSize)targetSize percent:(CGFloat)percent;

+(NSMutableArray*)sortContactData:(NSMutableArray*)allData;

+(void)showTips:(NSString*)tips view:(UIView*)curView;
+(void)showLoadingTips:(NSString*)tips view:(UIView*)curView;
+(void)closeHud;
@end
