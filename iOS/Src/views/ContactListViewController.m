//
//  ContactListViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/2.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "ContactListViewController.h"
#import "ContactTableViewCell.h"
#import "CBSiHttpConnection.h"
#import "UserDataModel.h"
#import "UIViewController+MMDrawerController.h"
#import "UserDAO.h"
#import "SectorDAO.h"
#import "DetailViewController.h"
#import "LeftDeptViewController.h"
#import "SearchViewController.h"
#import "UIViewController+LewPopupViewController.h"
#import "LewPopupViewAnimationSpring.h"
#import "LewPopupViewAnimationFade.h"
#import "Utils.h"
#import "ChineseInclude.h"
#import "PinYinForObjc.h"
#import "CBSiDataManager.h"
#import "PortraitImageHandler.h"

@interface ContactListViewController ()

@end

@implementation ContactListViewController
@synthesize allContactArray, sectionHeadsKeys;
@synthesize titleNameLabel;

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.contactListTableView setBackgroundColor:HEXCOLOR(0x53545f)];
    if ([self.contactListTableView respondsToSelector:@selector(setSectionIndexColor:)]) {
        self.contactListTableView.sectionIndexColor = HEXCOLOR(0x898989);
        self.contactListTableView.sectionIndexBackgroundColor = [UIColor clearColor];
        self.contactListTableView.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    }
    [self setContactListHeader];
    
    allContactArray = [[NSMutableArray alloc] initWithCapacity:0];
    sectionHeadsKeys= [[NSMutableArray alloc] initWithCapacity:0];

    [self initNavigationBar];

    [self performSelector:@selector(loadDB_RefreshSectorAndContact) withObject:nil afterDelay:0.1];
    [self updateByDrawState];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCell:) name:KNotificationUpdateContactCell object:nil];
}

-(void)setContactListHeader
{
    UIView* v = [[[NSBundle mainBundle] loadNibNamed:@"ContactListHeaderView" owner:self options:nil] objectAtIndex:0];
    float headerHeight = 210;
    if(IS_IPHONE4||IS_IPHONE5||IS_IPHONE3)
    {
        headerHeight = 165;
        v = [[[NSBundle mainBundle] loadNibNamed:@"ContactListHeaderView_ip4" owner:self options:nil] objectAtIndex:0];
    }

    UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.contactListTableView.frame), headerHeight)];
    [header addSubview:v];
    [self.contactListTableView setTableHeaderView:header];
}

-(void)loadDB_RefreshSectorAndContact
{
#if ENABLE_TEST_CODE
    LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
    [lVC refreshDeptView];
    [self refreshAllContactsList];
#else
    [self getSectorFromServer];
#endif

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initNavigationBar{
    
//    [self.navigationController setNavigationBarHidden:YES];
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        bar.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navigation_title.png"]];
    } else if(NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1){
        // Load resources for iOS 7 or later
        [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    }
    else{
        [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setFrame:CGRectMake(0, 2, 28, 28)];
    [leftBtn addTarget:self action:@selector(leftDrawerClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setFrame:CGRectMake(0, 2, 28, 28)];
    [rightBtn addTarget:self action:@selector(rightSearchClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [item setRightBarButtonItem:rightItem];
    [item setLeftBarButtonItem:leftButton];
    
//    if(NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1)
//    {
//        [leftBtn setImage:[UIImage imageNamed:@"nav_drawer@3x.png"] forState:UIControlStateNormal];
//        [leftBtn setImage:[UIImage imageNamed:@"nav_drawer_sel@3x.png"] forState:UIControlStateSelected];
//        [leftBtn setContentMode:UIViewContentModeScaleAspectFill];
//        [rightBtn setImage:[UIImage imageNamed:@"nav_search_icon@3x.png"] forState:UIControlStateNormal];
//        [rightBtn setImage:[UIImage imageNamed:@"nav_search_icon_sel@3x.png"] forState:UIControlStateSelected];
//    }
//    else
    {
        [leftBtn setImage:[UIImage imageNamed:@"nav_drawer.png"] forState:UIControlStateNormal];
        [leftBtn setImage:[UIImage imageNamed:@"nav_drawer_sel.png"] forState:UIControlStateSelected];
        [rightBtn setImage:[UIImage imageNamed:@"nav_search_icon.png"] forState:UIControlStateNormal];
        [rightBtn setImage:[UIImage imageNamed:@"nav_search_icon_sel.png"] forState:UIControlStateSelected];
    }
    
    titleNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 30)];
    titleNameLabel.textColor = [UIColor whiteColor];
    [titleNameLabel setTextAlignment:NSTextAlignmentCenter];
    [item setTitleView:titleNameLabel];
    
    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)leftDrawerClick:(UIButton*)sender{
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        [sender setSelected:!sender.isSelected];
    }];
}

-(void)rightSearchClick:(UIButton*)sender
{
    SearchViewController *srhVC = [[SearchViewController alloc] init];
    srhVC.allContactArray = [Utils sortContactData:[CBSiDataManager sharedDataManager].allContactsArray];
    [self.mm_drawerController.navigationController pushViewController:srhVC animated:YES];
}

-(void)updateByDrawState
{
    __block UIButton* lBtn = leftBtn;
    [self.mm_drawerController setGestureCompletionBlock:^(MMDrawerController *drawerController, UIGestureRecognizer *gesture) {

        if(drawerController.openSide == MMDrawerSideNone)
        {
            [lBtn setSelected:NO];
        }
        else
        {
            [lBtn setSelected:YES];
        }
    }];
}

#pragma mark UITableViewDelegate Methods
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    return 70.0f;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [[self.allContactArray objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.allContactArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sectionHeadsKeys objectAtIndex:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = HEXCOLOR(0x2f2525);
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 22)];
    titleLabel.textColor=HEXCOLOR(0x66525b);
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text=[self.sectionHeadsKeys objectAtIndex:section];
    [myView addSubview:titleLabel];
    return myView;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionHeadsKeys;
}
static int iCellNumber = 0;
static bool needRefreshList = YES;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserDataModel *u = nil;
    NSArray *rowArr = nil;
    if ([self.allContactArray count] > indexPath.section) {
        rowArr = [self.allContactArray objectAtIndex:indexPath.section];
        if ([rowArr count] > indexPath.row) {
            u = (UserDataModel *) [rowArr objectAtIndex:indexPath.row];
        }
    }
    
    NSString *identifier = u.user_id;
    
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(needRefreshList)
    {
        cell = nil;
    }
    if(indexPath.section == (self.allContactArray.count-1) && indexPath.row==(rowArr.count-1))
    {
        needRefreshList = NO;
    }
    
    if (!cell)
    {
        BOOL nibsRegistered = NO;
        if (!nibsRegistered) {
            UINib *nib = [UINib nibWithNibName:NSStringFromClass([ContactTableViewCell class]) bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:identifier];
            nibsRegistered = YES;
        }
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.isCellDarkBg = (iCellNumber%2==0);
        [cell setBackgroundColor:(cell.isCellDarkBg?HEXCOLOR(0x3e3e46):HEXCOLOR(0x53545f))];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(u)
        {
            [cell updateViewForCell:u.user_name userTitle:u.user_position];
            [cell.detailBtn setTag:[u.user_id integerValue]];
            [cell.detailBtn addTarget:self action:@selector(openDetail:) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.portrait setImage:[UIImage imageNamed:@"portrait_default.png"]];
            [[PortraitImageHandler sharedPIHandler] getPortraitImageWithUserId:u.user_id sectorId:u.sector_id complete:^(NSString *userId, UIImage *portrait) {
                if([u.user_id isEqualToString:userId] && portrait!=nil)
                {
                    [cell.portrait setImage:portrait];
                }
            }];
            iCellNumber++;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSArray* contactInSecArr = [self.allContactArray objectAtIndex:indexPath.section];
    selectedUser = [contactInSecArr objectAtIndex:indexPath.row];
    [self popMenu];
}

-(void)getContactFromServer
{
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    NSString* corpId = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
    [params setObject:corpId forKey:@"corpid"];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_LOADCONTACT completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"getContactFromServer response content: %@",responseContent);
        [self closeHud];
        [self getCorprationInfo];
        
        if([responseContent isKindOfClass:[NSArray class]])
        {
            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:(NSArray*)responseContent];
            for(NSDictionary* d in array)
            {
                UserDataModel* u = [[UserDataModel alloc] initWithDict:d];
                if(u)
                {
                    SectorDataModel* s = [SectorDAO findSectorFromDB:u.sector_id];
                    u.departmentName = s.sector_name;
                    [allContactArray addObject:u];
                }
            }
            BOOL isSaved = [UserDAO saveToDataBase:allContactArray];
            NSLog(@"Save contacts to db: %@",isSaved?@"success":@"failed");
            [self buildContactArray:[CBSiDataManager sharedDataManager].allContactsArray];
            [self.contactListTableView reloadData];

            [[CBSiDataManager sharedDataManager] reloadAllSectorsData];
            LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
            [lVC refreshDeptView];
            return;
        }
        
        NSString* state = [responseContent objectForKey:@"StateCode"];
        if(state != nil && state.intValue !=0)
        {
            [self refreshAllContactsList];
            LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
            [lVC refreshDeptView];
            return;
        }
        
    } error:^(NSString *errorInfo) {
        NSLog(@"getContactFromServer response Failed: %@",errorInfo);
        [self showTips:@"服务器连接失败！"];
        [self refreshAllContactsList];
        LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
        [lVC refreshDeptView];
    }];
}


-(void)getSectorFromServer
{
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    NSString* corpId = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
    [params setObject:corpId forKey:@"corpid"];
    [self showDownloadingState];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_LOADSECTOR completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"getSectorFromServer response content: %@",responseContent);
        if([responseContent isKindOfClass:[NSArray class]])
        {
            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:(NSArray*)responseContent];
            NSMutableArray *allSectorsArr = [[NSMutableArray alloc] init];
            for(NSDictionary* d in array)
            {
                SectorDataModel* s = [[SectorDataModel alloc] initWithDict:d];
                if(s)
                    [allSectorsArr addObject:s];
            }
            [SectorDAO saveToDataBase:allSectorsArr];
            [self getContactFromServer];
            return;
        }
        
        [self closeHud];
        NSString* state = [responseContent objectForKey:@"StateCode"];
        if(state != nil && state.intValue !=0)
        {
            LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
            [lVC refreshDeptView];
            [self refreshAllContactsList];
            return;
        }
        
    } error:^(NSString *errorInfo) {
        NSLog(@"getSectorFromServer response Failed: %@",errorInfo);
        [self showTips:@"服务器连接失败！"];
        LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
        [lVC refreshDeptView];
        [self refreshAllContactsList];
    }];
}

-(void)getCorprationInfo
{
    NSString* corpId = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:corpId forKey:@"corpid"];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_GET_CORPATION completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"getCorprationInfo successful! Data: %@", responseContent);
        if([responseContent isKindOfClass:[NSDictionary class]])
        {
            NSString* corpName = [responseContent objectForKey:@"corporation_name"];
            [Utils saveDefaultConfigWith:CONFIG_INFO_CORPNAME_KEY value:corpName];
            titleNameLabel.text = corpName;
            LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
            [lVC updateCorporationName:corpName];
        }
        else
        {
            titleNameLabel.text = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPNAME_KEY];
            LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
            [lVC updateCorporationName:titleNameLabel.text];
        }
    } error:^(NSString *errorInfo) {
        NSLog(@"getCorprationInfo has error: %@", errorInfo);
        titleNameLabel.text = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPNAME_KEY];
        LeftDeptViewController *lVC = (LeftDeptViewController*)self.mm_drawerController.leftDrawerViewController;
        [lVC updateCorporationName:titleNameLabel.text];
    }];
}

-(void)refreshAllContactsList
{
    iCellNumber = 0;
    needRefreshList = YES;
    [leftBtn setSelected:NO];
    [self buildContactArray:[CBSiDataManager sharedDataManager].allContactsArray];
    [self.contactListTableView reloadData];
}

-(void)refreshAllSectorContactList:(NSString*)sectorId
{
    iCellNumber = 0;
    needRefreshList = YES;
    [self buildContactArray:[UserDAO loadUsersBySectorId:sectorId]];
    [self.contactListTableView reloadData];
    [leftBtn setSelected:NO];
}

-(void)openDetail:(UIButton*)sender
{
    UserDataModel* user = [UserDAO findUserFromDB:[NSString stringWithFormat:@"%d",(int)sender.tag]];
    DetailViewController* detailVC = [[DetailViewController alloc] init];
    detailVC.detailUser = user;
    [self.mm_drawerController.navigationController pushViewController:detailVC animated:YES];
}

-(void)showTips:(NSString*)tips
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
    HUD.delegate = self;
    HUD.labelText = tips;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.4];
}

-(void)showDownloadingState
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.dimBackground = YES;
    
    HUD.delegate = self;
    HUD.labelText = @"下载通讯录...";
    
    [HUD show:YES];
    //----Avoid drawerController was drag and sliding!----<by Hequn>----//
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
}

-(void)closeHud
{
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    if(HUD)
    {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

-(void)popMenu
{
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"ContactPopView" owner:self options:nil] objectAtIndex:0];
    UILabel* namelab = (UILabel*)[view viewWithTag:101];
    namelab.text = selectedUser.user_name;
    UILabel* phonelab = (UILabel*)[view viewWithTag:102];
    phonelab.text = selectedUser.user_phone;
    UILabel* tellab = (UILabel*)[view viewWithTag:103];
    tellab.text = selectedUser.user_tel;
    
    UIButton* phoneBtn = (UIButton*)[view viewWithTag:201];
    [phoneBtn addTarget:self action:@selector(phoneCallAction:) forControlEvents:UIControlEventTouchUpInside];
    [phoneBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xfdf8f1)) forState:UIControlStateNormal];
    [phoneBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xe1d3c2)) forState:UIControlStateHighlighted];
    UIButton* telBtn = (UIButton*)[view viewWithTag:202];
    [telBtn addTarget:self action:@selector(telCallAction:) forControlEvents:UIControlEventTouchUpInside];
    [telBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xfdf8f1)) forState:UIControlStateNormal];
    [telBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xe1d3c2)) forState:UIControlStateHighlighted];
    UIButton* smsBtn = (UIButton*)[view viewWithTag:203];
    [smsBtn addTarget:self action:@selector(sendSMSAction:) forControlEvents:UIControlEventTouchUpInside];
    [smsBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xfdf8f1)) forState:UIControlStateNormal];
    [smsBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xe1d3c2)) forState:UIControlStateHighlighted];
    UIButton* cancelBtn = (UIButton*)[view viewWithTag:204];
    [cancelBtn addTarget:self action:@selector(cancelPopMenu:) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xc2c2c2)) forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x9b9b9b)) forState:UIControlStateHighlighted];

    
    
    [self lew_presentPopupView:view animation:[LewPopupViewAnimationFade new] dismissed:^{
        NSLog(@"动画结束");
    }];
}

-(void)phoneCallAction:(UIButton*)sender
{
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@", selectedUser.user_phone];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)telCallAction:(UIButton*)sender
{
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",selectedUser.user_tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)sendSMSAction:(UIButton*)sender
{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    
    if (messageClass != nil) {
        // Check whether the current device is configured for sending SMS messages
        if ([messageClass canSendText]) {
            [self displaySMSComposerSheet];
        }
        else {
            [self alertWithMessage:@"设备没有短信功能"];
        }
    }
    else {
        [self alertWithMessage:@"iOS版本过低,iOS4.0以上才支持程序内发送短信"];
    }
}

-(void)cancelPopMenu:(UIButton*)sender
{
    [self lew_dismissPopupView];
}


-(void)displaySMSComposerSheet
{
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.recipients = [NSArray arrayWithObject:selectedUser.user_phone];
    
//    [self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    
    switch (result)
    {
        case MessageComposeResultCancelled:
            NSLog(@"Result: SMS sending canceled");
            break;
        case MessageComposeResultSent:
            NSLog(@"Result: SMS sent");
            break;
        case MessageComposeResultFailed:
        {
            [self alertWithMessage:@"短信发送失败"];
        }
            break;
        default:
            NSLog(@"Result: SMS not sent");
            break;
    }
    //    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)alertWithMessage:(NSString*)msg
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)buildContactArray:(NSMutableArray*)allData
{
    [self.allContactArray removeAllObjects];
    [sectionHeadsKeys removeAllObjects];

    NSMutableArray* sortArray = [Utils sortContactData:allData];
    
    
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [sortArray count]; index++)
    {
        UserDataModel *user = (UserDataModel *)[sortArray objectAtIndex:index];
        NSMutableString *strchar= [NSMutableString stringWithString:user.pinyinKey];
        if(![sectionHeadsKeys containsObject:[strchar uppercaseString]])        {
            [sectionHeadsKeys addObject:[strchar uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([sectionHeadsKeys containsObject:[strchar uppercaseString]])
        {
            [TempArrForGrouping addObject:[sortArray objectAtIndex:index]];
            if(checkValueAtIndex == NO)
            {
                [self.allContactArray addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
            }
        }
    }

}

-(void)updateCell:(NSNotification*)noti
{
    NSString* userId = [noti object];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@", userId];
    NSArray *filteredArray = [[CBSiDataManager sharedDataManager].allContactsArray filteredArrayUsingPredicate:predicate];
    if(filteredArray.count == 0)
    {
        NSLog(@"UpdatePortrait for Cell failed! UserId is not exist in local DB~");
        return;
    }
    UserDataModel* user = [filteredArray objectAtIndex:0];
    if(user == nil)
    {
        NSLog(@"UpdatePortrait for Cell failed! user not found~");
        return;
    }
    int section = 0, row=-1;
    for(;section<self.allContactArray.count; section++)
    {
        NSArray* userArr = [self.allContactArray objectAtIndex:section];
        if([userArr containsObject:user])
        {
            row = (int)[userArr indexOfObject:user];
            break;
        }
    }
    if(row == -1)//----not found----//
    {
        NSLog(@"UpdatePortrait for Cell failed! user position is lost!!!");
        return;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    ContactTableViewCell *cell = (ContactTableViewCell*)[self.contactListTableView cellForRowAtIndexPath:indexPath];
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell updateViewForCell:user.user_name userTitle:user.user_position];
        [[PortraitImageHandler sharedPIHandler] getPortraitImageWithUserId:user.user_id sectorId:user.sector_id complete:^(NSString *userId, UIImage *portrait) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([user.user_id isEqualToString:userId] && portrait!=nil)
                {
                    [cell.portrait setImage:portrait];
                    [self.contactListTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//                    [cell setNeedsDisplay];
                }
            });
            
        }];
    });
        

}
@end
