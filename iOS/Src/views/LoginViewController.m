//
//  LoginViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/6.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "LoginViewController.h"
#import "CBSiHttpConnection.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "UserDAO.h"
#import <QuartzCore/QuartzCore.h>

#define PHONE_INPUT_TAG 1001
#define VCODE_INPUT_TAG 1002

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize verifyCodeBtn, verifyCodeTextField, phoneNoTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initView];
    
    [self loadLastUserInfo];
    isStartingLogin = NO;
}

-(void)initView
{
    self.phoneNoTextField.tag = PHONE_INPUT_TAG;
    self.phoneNoTextField.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNoTextField.layer.borderColor = HEXACOLOR(0xffffff, 0.5).CGColor;
    self.phoneNoTextField.layer.borderWidth= 1.0f;
    [self.phoneNoTextField setValue:HEXACOLOR(0xffffff, 0.1f) forKeyPath:@"_placeholderLabel.textColor"];
    
    self.verifyCodeTextField.tag = VCODE_INPUT_TAG;
    self.verifyCodeTextField.keyboardType = UIKeyboardTypeNumberPad;

    self.verifyCodeTextField.layer.borderColor = HEXACOLOR(0xffffff, 0.5).CGColor;
    self.verifyCodeTextField.layer.borderWidth= 1.0f;
    [self.verifyCodeTextField setValue:HEXACOLOR(0xffffff, 0.1f) forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.verifyCodeBtn setTitleColor:HEXACOLOR(0xffffff, 0.8) forState:UIControlStateNormal];
    [self.verifyCodeBtn setTitleColor:HEXACOLOR(0xffffff, 0.2) forState:UIControlStateHighlighted];
    [self.verifyCodeBtn setTitleColor:HEXACOLOR(0xffffff, 0.2) forState:UIControlStateDisabled];
    
    [self.verifyCodeBtn setBackgroundImage:imageWithColor(HEXACOLOR(0xffffff, 0.1)) forState:UIControlStateNormal];
    [self.verifyCodeBtn setBackgroundImage:imageWithColor(HEXACOLOR(0xffffff, 0.15)) forState:UIControlStateHighlighted];
    [self.verifyCodeBtn setBackgroundImage:imageWithColor(HEXACOLOR(0xffffff, 0.15)) forState:UIControlStateDisabled];
    
    
//    [self.loginBtn setTitleColor:HEXCOLOR(0x593535) forState:UIControlStateHighlighted];
    [self.loginBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xe1bb78)) forState:UIControlStateNormal];
    [self.loginBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xd6aa5e)) forState:UIControlStateHighlighted];
    

    UIControl *touchBg = [[UIControl alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:touchBg];
    [self.view sendSubviewToBack:touchBg];
    [touchBg addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
//    // 键盘高度变化通知，ios5.0新增的
//#ifdef __IPHONE_5_0
//    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
//    if (version >= 5.0) {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
//    }
//#endif
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [notification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's coordinate system. The bottom of the text view's frame should align with the top of the keyboard's final position.
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardHeight = keyboardRect.size.height;
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
//    [self moveInputBarWithKeyboardHeight:keyboardRect.size.height withDuration:animationDuration];
    __block CGRect oriFrame = self.view.frame;
    if(oriFrame.origin.y >= 0 && [self.verifyCodeTextField isFirstResponder])
    {
        [UIView animateWithDuration:animationDuration animations:^{
            oriFrame.origin.y -= keyboardHeight;
            self.view.frame = oriFrame;
        }];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary* userInfo = [notification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
//    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's coordinate system. The bottom of the text view's frame should align with the top of the keyboard's final position.
//    CGRect keyboardRect = [aValue CGRectValue];
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    __block CGRect oriFrame1 = self.view.frame;
    if(oriFrame1.origin.y < 0)
    {
        [UIView animateWithDuration:animationDuration animations:^{
            oriFrame1.origin.y += keyboardHeight;
            self.view.frame = oriFrame1;
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loadLastUserInfo
{
    self.phoneNoTextField.text = [Utils loadDefaultConfigWith:CONFIG_INFO_USER_PHONE_KEY];
}

- (IBAction)getVerifyCode:(id)sender {
    
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    [params setObject:self.phoneNoTextField.text forKey:@"phone"];
    [params setObject:@"1" forKey:@"type"];
    [self.verifyCodeBtn setHighlighted:YES];
    [self fadingTime];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_SEND_SMS completion:^(NSMutableDictionary *responseContent) {

        NSLog(@"Success content: %@",responseContent);
        if([responseContent isKindOfClass:[NSDictionary class]])
        {
            NSString* result = [responseContent objectForKey:@"result"];
            if([result intValue] != 1)
            {
                [self showTips:[NSString stringWithFormat:@"验证短信发送失败！请重试：%@", result]];
                [self cancelTimeFading];
            }
            else
            {
                
            }
            
        }
    } error:^(NSString *errorInfo) {
        NSLog(@"Error content: %@",errorInfo);
        [self showTips:@"网络错误，验证短信发送失败！"];
    }];
}

-(void)hideKeyBoard
{
    if([self.phoneNoTextField isFirstResponder])
        [self.phoneNoTextField resignFirstResponder];
    if([self.verifyCodeTextField isFirstResponder])
        [self.verifyCodeTextField resignFirstResponder];
}

- (IBAction)loginAction:(id)sender {
    if(![self isValidPhoneNO:self.phoneNoTextField.text])
    {
        [self showTips:@"手机号码输入错误！"];
        return;
    }
    
    if(![self isValidDynamicCode])
    {
        return;
    }
    
    [self hideKeyBoard];
    
    
    [self loginToServer:self.phoneNoTextField.text verifyCode:self.verifyCodeTextField.text];
}
static int iLoginTimes = 0;
-(void)loginToServer:(NSString*)userPhone verifyCode:(NSString*)verifyCode
{
    isStartingLogin = YES;
    if(iLoginTimes == 0)
        [self showLoginLoadingState];
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    [params setObject:userPhone forKey:@"userPhone"];
    [params setObject:verifyCode forKey:@"dynamicCode"];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_LOGIN completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"Success content: %@",responseContent);
        NSString* state = [responseContent objectForKey:@"StateCode"];
        if(state != nil && state.intValue !=0)
        {
            if(iLoginTimes==0)
            {
                iLoginTimes++;
                NSArray *uAndC = [NSArray arrayWithObjects:userPhone, verifyCode, nil];
                [self performSelector:@selector(loginToServerWithArrayParam:) withObject:uAndC afterDelay:0.5];
                return;
            }
            iLoginTimes = 0;
            [self closeHud];
            [self showTips:@"登录失败：用户名或动态码错！"];
            return;
        }
        iLoginTimes = 0;
        [self closeHud];
        AppDelegate* cbsiApp = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [cbsiApp initMainView];
        [Utils saveLoginInfo:[responseContent objectForKey:@"userid"] corpId:[responseContent objectForKey:@"corpid"] hasLogin:YES phone:self.phoneNoTextField.text];
        isStartingLogin = NO;
    } error:^(NSString *errorInfo) {
        if(iLoginTimes==0)
        {
            iLoginTimes++;
            NSArray *uAndC = [NSArray arrayWithObjects:userPhone, verifyCode, nil];
            [self performSelector:@selector(loginToServerWithArrayParam:) withObject:uAndC afterDelay:0.5];
            return;
        }
        [self closeHud];
        [self showTips:@"登录失败：网络错误！"];
        NSLog(@"Failed content: %@",errorInfo);
        isStartingLogin = NO;
        iLoginTimes = 0;
    }];

}

-(void)loginToServerWithArrayParam:(NSArray*)userAndCode
{
    [self loginToServer:userAndCode[0] verifyCode:userAndCode[1]];
}

- (IBAction)textHasChanged:(id)sender {
    UITextField *tf = (UITextField*)sender;

    if(tf.tag == PHONE_INPUT_TAG)
    {
        if(tf.text.length > 11)
        {
//            [self showTips:@"输入错误，手机号超过11位"];
            tf.text = [tf.text substringToIndex:11];
        }
    }
    else if(tf.tag == VCODE_INPUT_TAG)
    {
        if(tf.text.length > 4)
        {
//            [self showTips:@"输入错误，动态密码超过4位"];
            tf.text = [tf.text substringToIndex:4];
        }
    }
}

-(BOOL)isValidPhoneNO:(NSString*)phoneNO
{
    BOOL isValidNumber = YES;
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188,1705
     * 联通：130,131,132,152,155,156,185,186,1709
     * 电信：133,1349,153,180,189,1700
     */
    //    NSString * MOBILE = @"^1((3//d|5[0-35-9]|8[025-9])//d|70[059])\\d{7}$";//总况
    
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188，1705
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d|705)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186,1709
     17         */
    NSString * CU = @"^1((3[0-2]|5[256]|8[56])\\d|709)\\d{7}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189,1700
     22         */
    NSString * CT = @"^1((33|53|8[09])\\d|349|700)\\d{7}$";
    
    
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    
    //    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    NSPredicate *regextestphs = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",PHS];
    
    if (([regextestcm evaluateWithObject:phoneNO] == YES)
        || ([regextestct evaluateWithObject:phoneNO] == YES)
        || ([regextestcu evaluateWithObject:phoneNO] == YES)
        || ([regextestphs evaluateWithObject:phoneNO] == YES))
    {
        isValidNumber = YES;
    }
    else
    {
        isValidNumber = NO;
    }
    return isValidNumber;
}

-(BOOL)isValidDynamicCode
{
    BOOL isValidVerifyCode = YES;
    if(self.verifyCodeTextField.text.length == 0)
    {
        [self showTips:@"请输入动态密码！"];
        return NO;
    }
    isValidVerifyCode = (self.verifyCodeTextField.text.length <= 4);
    if(isValidVerifyCode == NO)
    {
        [self showTips:@"动态密码输入错误！"];
    }
    return isValidVerifyCode;
}

-(void)showTips:(NSString*)tips
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
//    HUD.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] autorelease];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
//    HUD.dimBackground = YES;
    
    HUD.delegate = self;
    HUD.labelText = tips;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.4];
}

-(void)showLoginLoadingState
{
    [self closeHud];
    
//    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.dimBackground = YES;
    
    HUD.delegate = self;
    HUD.labelText = @"登录中...";
    
    [HUD show:YES];
}

-(void)closeHud
{
    if(HUD)
    {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

-(void)fadingTime{
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    fadingTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(fadingTimer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(fadingTimer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            [self cancelTimeFading];
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                NSLog(@"____%@",strTime);
//                [verifyCodeBtn setTitle:[NSString stringWithFormat:@"如未收到短信，请%@秒后重新发送",strTime] forState:UIControlStateNormal];
                [verifyCodeBtn setTitle:[NSString stringWithFormat:@"如未收到短信，请%@秒后重新发送",strTime] forState:UIControlStateDisabled];
                verifyCodeBtn.enabled = NO;
                
            });
            timeout--;
            
        }
    });
    dispatch_resume(fadingTimer);
    
}

-(void)cancelTimeFading
{
    dispatch_source_cancel(fadingTimer);
    dispatch_async(dispatch_get_main_queue(), ^{
        //设置界面的按钮显示 根据自己需求设置
        [verifyCodeBtn setTitle:@"重新获取动态密码" forState:UIControlStateNormal];
        verifyCodeBtn.enabled = YES;
        [verifyCodeBtn setHighlighted:NO];
    });
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag == 1002)
    {
        __block CGRect oriFrame = self.view.frame;
        if(oriFrame.origin.y >= 0)
        {
            [UIView animateWithDuration:0.3 animations:^{
                oriFrame.origin.y -= keyboardHeight;
                self.view.frame = oriFrame;
            }];
        }

    }
    return YES;
}

#pragma mark - MBProgressHUDDelegate
-(void)hudWasHidden:(MBProgressHUD *)hud
{
    [self closeHud];
}
@end
