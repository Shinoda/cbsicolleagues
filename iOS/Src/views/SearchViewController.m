//
//  SearchViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/23.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "SearchViewController.h"
#import "ContactTableViewCell.h"
#import "UserDataModel.h"
#import "DetailViewController.h"
#import "ChineseInclude.h"
#import "PinYinForObjc.h"
#import "PortraitImageHandler.h"
#import <QuartzCore/QuartzCore.h>

@interface SearchViewController ()

@end

@implementation SearchViewController
@synthesize allContactArray, resultContactArray;
@synthesize searchInputer, resultTableView, clearBtn, clearBtnBg;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBar];
    
    [self initDataAndView];
}

-(void)initDataAndView
{
    self.resultContactArray = [[NSMutableArray alloc] init];
    self.resultTableView.rowHeight = 70.f;
    [clearBtn setHidden:YES];
    [clearBtnBg setHidden:YES];
    [self.searchInputer addTarget:self action:@selector(textFieldWithText:) forControlEvents:UIControlEventAllEvents];
    
    self.searchInputer.borderStyle = UITextBorderStyleNone;
    [searchInputer setValue:HEXACOLOR(0xffffff, 0.6f) forKeyPath:@"_placeholderLabel.textColor"];
    [searchInputer setValue:[UIFont boldSystemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    [searchInputer becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)initNavigationBar{
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    
    [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn setFrame:CGRectMake(0, 2, 28, 28)];
    
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_nor.png"] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_press.png"] forState:UIControlStateSelected];
    
    [leftBtn addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    
    [item setLeftBarButtonItem:leftButton];
    //    [item setTitle:@""];
    UILabel* titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    titleLab.textColor = [UIColor whiteColor];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    titleLab.text = @"搜  索";
    [item setTitleView:titleLab];
    
    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)Back:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clearTextEvent:(UIButton*)sender {
    searchInputer.text = @"";
    [clearBtn setHidden:YES];
    [clearBtnBg setHidden:YES];
    [self.resultContactArray removeAllObjects];
    [resultTableView reloadData];
}

#pragma mark - UITextFieldDelegate method
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length>0 || textField.text.length>1)
    {
        [clearBtn setHidden:NO];
        [clearBtnBg setHidden:NO];
    }
    else
    {
        [clearBtn setHidden:YES];
        [clearBtnBg setHidden:YES];
    }
    
    return YES;
}

-(void)textFieldWithText:(UITextField*)textField
{
    [self searchContactByInputString:textField.text];
}

-(void)searchContactByInputString:(NSString*)inStr
{
    self.resultContactArray = [[NSMutableArray alloc] init];
    if (inStr.length>0&&![ChineseInclude isIncludeChineseInString:inStr]) {
        for (int i=0; i<self.allContactArray.count; i++) {
            UserDataModel *user = [self.allContactArray objectAtIndex:i];
            
            //----Match name----//
            if ([ChineseInclude isIncludeChineseInString:user.user_name]) {
                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:user.user_name];
                NSRange titleResult=[tempPinYinStr rangeOfString:inStr options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [resultContactArray addObject:user];
                    continue;
                }
                NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:user.user_name];
                NSRange titleHeadResult=[tempPinYinHeadStr rangeOfString:inStr options:NSCaseInsensitiveSearch];
                if (titleHeadResult.length>0) {
                    [resultContactArray addObject:user];
                    continue;
                }
            }
            else {
                NSRange titleResult=[user.user_name rangeOfString:inStr options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [resultContactArray addObject:user];
                    continue;
                }
            }
            //----Match position----//
            if ([ChineseInclude isIncludeChineseInString:user.user_position]) {
                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:user.user_position];
                NSRange titleResult=[tempPinYinStr rangeOfString:inStr options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [resultContactArray addObject:user];
                    continue;
                }
                NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:user.user_position];
                NSRange titleHeadResult=[tempPinYinHeadStr rangeOfString:inStr options:NSCaseInsensitiveSearch];
                if (titleHeadResult.length>0) {
                    [resultContactArray addObject:user];
                    continue;
                }
            }
            else {
                NSRange titleResult=[user.user_position rangeOfString:inStr options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [resultContactArray addObject:user];
                    continue;
                }
            }
        }
    } else if (inStr.length>0&&[ChineseInclude isIncludeChineseInString:inStr]) {
        for (UserDataModel* u in self.allContactArray) {
            NSString *tempStr = u.user_name;
            NSRange titleResult=[tempStr rangeOfString:inStr options:NSCaseInsensitiveSearch];
            if (titleResult.length>0) {
                [resultContactArray addObject:u];
                continue;
            }
            //----Match position----//
            tempStr = u.user_position;
            titleResult=[tempStr rangeOfString:inStr options:NSCaseInsensitiveSearch];
            if (titleResult.length>0) {
                [resultContactArray addObject:u];
                continue;
            }
        }
    }
    
    [resultTableView reloadData];
}

#pragma mark UITableViewDelegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.resultContactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserDataModel* user = nil;
    if(self.resultContactArray.count >0)
    {
        user = [self.resultContactArray objectAtIndex:indexPath.row];
    }
    NSString *identifier = user.user_id;
    
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    if (!cell) {
        BOOL nibsRegistered = NO;
        if (!nibsRegistered) {
            UINib *nib = [UINib nibWithNibName:NSStringFromClass([ContactTableViewCell class]) bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:identifier];
            nibsRegistered = YES;
        }
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell updateViewForCell:user.user_name userTitle:user.user_position];
        [cell.detailBtn setTag:indexPath.row];
        [cell.detailBtn addTarget:self action:@selector(openDetail:) forControlEvents:UIControlEventTouchUpInside];
        [[PortraitImageHandler sharedPIHandler] getPortraitImageWithUserId:user.user_id sectorId:user.sector_id complete:^(NSString *userId, UIImage *portrait) {
            if([user.user_id isEqualToString:userId] && portrait!=nil)
            {
                [cell.portrait setImage:portrait];
            }
        }];
    }
    cell.isCellDarkBg = (indexPath.row%2==0);
    [cell setBackgroundColor:(cell.isCellDarkBg?HEXCOLOR(0x3e3e46):HEXCOLOR(0x53545f))];
    return cell;
}

-(void)openDetail:(UIButton*)sender
{
    UserDataModel* user = [self.resultContactArray objectAtIndex:sender.tag];
    DetailViewController* detailVC = [[DetailViewController alloc] init];
    detailVC.detailUser = user;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIButton* btn = [[UIButton alloc] init];
    [btn setTag:indexPath.row];
    [self openDetail:btn];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([searchInputer isFirstResponder])
    {
        [searchInputer resignFirstResponder];
    }
}
@end
