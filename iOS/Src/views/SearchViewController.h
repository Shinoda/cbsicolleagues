//
//  SearchViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/23.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) NSMutableArray *allContactArray;
@property (strong, nonatomic) NSMutableArray *resultContactArray;
@property (strong, nonatomic) IBOutlet UITextField *searchInputer;
@property (strong, nonatomic) IBOutlet UIButton *clearBtn;
@property (strong, nonatomic) IBOutlet UIButton *clearBtnBg;
@property (strong, nonatomic) IBOutlet UITableView *resultTableView;
@end
