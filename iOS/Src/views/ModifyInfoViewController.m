//
//  ModifyInfoViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/20.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "ModifyInfoViewController.h"
#import "Utils.h"
#import "CBSiHttpConnection.h"
#import "CBSiDataManager.h"

@interface ModifyInfoViewController ()

@end

@implementation ModifyInfoViewController
@synthesize modifyType, modifyTextField, currentUser;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBar];
    
    [self initContentView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)initNavigationBar{
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    
    [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setFrame:CGRectMake(0, 2, 60, 28)];
    [leftBtn setTitle:@"取 消" forState:UIControlStateNormal];
    [leftBtn setTitleColor:HEXACOLOR(0xffffff, 0.6f) forState:UIControlStateNormal];
    [leftBtn setTitleColor:HEXACOLOR(0xfce0b8, 0.6f) forState:UIControlStateHighlighted];
    [leftBtn addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    [item setLeftBarButtonItem:leftButton];
    
    saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn setFrame:CGRectMake(0, 2, 48, 28)];
    [saveBtn setTitle:@"保 存" forState:UIControlStateNormal];
    [saveBtn setTitleColor:HEXACOLOR(0xffffff, 0.6f) forState:UIControlStateNormal];
    [saveBtn setTitleColor:HEXACOLOR(0xfce0b8, 0.6f) forState:UIControlStateHighlighted];
    [saveBtn addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    [item setRightBarButtonItem:rightItem];
    
    UILabel* titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    titleLab.textColor = [UIColor whiteColor];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    switch (self.modifyType) {
        case EMODIFY_TYPE_MAIL:
            titleLab.text = @"邮箱";
            break;
        case EMODITY_TYPE_FIXED_TEL:
            titleLab.text = @"固话";
            break;
        case EMODITY_TYPE_TEL_SUB:
            titleLab.text = @"分机";
            break;
        default:
            break;
    }
    [item setTitleView:titleLab];
    
    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)Back:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveAction:(UIButton*)sender
{
    if(self.modifyTextField.isFirstResponder)
    {
        [self.modifyTextField resignFirstResponder];
    }
    NSString* modifyKey = @"";
    switch (self.modifyType) {
        case EMODIFY_TYPE_MAIL:
            modifyKey = FIELD_USER_EMAIL;
            if(![self checkMail:self.modifyTextField.text])
            {
                [Utils showTips:@"邮箱地址格式输入不正确！" view:self.view];
                return;
            }
            currentUser.user_email = self.modifyTextField.text;
            break;
        case EMODITY_TYPE_FIXED_TEL:
            modifyKey = FIELD_USER_TEL;
            currentUser.user_tel = self.modifyTextField.text;
            break;
        case EMODITY_TYPE_TEL_SUB:
            modifyKey = FIELD_USER_TEL_SUB;
            currentUser.user_tel_sub = self.modifyTextField.text;
            break;
        default:
            break;
    }
    [self uploadPersonalInfo:modifyKey modifyValue:self.modifyTextField.text];
}

-(BOOL)checkMail:(NSString*)mail
{
    if(mail.length == 0)
    {
        return YES;
    }
//    BOOL isTrue = NO;
//    ^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$
    NSString *re = @"(?<=@)((?:[A-Za-z0-9]+(?:[\\-|\\.][A-Za-z0-9]+)*)+\\.[A-Za-z]{2,6})$";
    NSRange range = [mail rangeOfString:re options:NSRegularExpressionSearch];
    if (range.location != NSNotFound) {
        return YES;
    }
    else {
        return NO;
    }
}

-(void)initContentView
{
    currentUser = [CBSiDataManager sharedDataManager].currentUser;
    modifyTextField.borderStyle = UITextBorderStyleNone;
    [modifyTextField becomeFirstResponder];
    [modifyTextField setValue:HEXACOLOR(0xffffff, 0.6f) forKeyPath:@"_placeholderLabel.textColor"];
    [modifyTextField setValue:[UIFont boldSystemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    switch (self.modifyType) {
        case EMODIFY_TYPE_MAIL:
            self.modifyTextField.text = currentUser.user_email;
            self.modifyTextField.keyboardType = UIKeyboardTypeEmailAddress;
            modifyTextField.placeholder = @"请输入修改的邮箱地址";
            break;
        case EMODITY_TYPE_FIXED_TEL:
            self.modifyTextField.text = currentUser.user_tel;
            modifyTextField.placeholder = @"请输入修改的固话";
            self.modifyTextField.keyboardType = UIKeyboardTypePhonePad;
            break;
        case EMODITY_TYPE_TEL_SUB:
            self.modifyTextField.text = currentUser.user_tel_sub;
            modifyTextField.placeholder = @"请输入修改的分机号";
            self.modifyTextField.keyboardType = UIKeyboardTypeNumberPad;
            break;
        default:
            break;
    }
}

-(void)uploadPersonalInfo:(NSString*)modifyKey modifyValue:(NSString*)mValue
{
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    [params setObject:currentUser.user_id forKey:@"userid"];
    [params setObject:mValue forKey:modifyKey];
    [self showUploadingMaskState];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_MODIFYUSERINFO completion:^(NSMutableDictionary *responseContent) {
        
        NSLog(@"uploadPersonalInfo Success content: %@",responseContent);
        [self closeHud];
        if([responseContent isKindOfClass:[NSDictionary class]])
        {
            NSString* stateCode = [responseContent objectForKey:@"StateCode"];
            if([stateCode isEqualToString:@"1"])
            {
                [UserDAO saveUserToDB:currentUser];
                [self Back:nil];
                [self syncUserInfo];
            }
            else
            {
                [self showTips:[NSString stringWithFormat:@"上传资料失败! %@", (stateCode==nil?@"Unknow":stateCode)]];
            }
            
        }
    } error:^(NSString *errorInfo) {
        NSLog(@"uploadPersonalInfo Error content: %@",errorInfo);
        [self showTips:@"网络错误，请检查网络！"];
    }];
}

-(void)showTips:(NSString*)tips
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelText = tips;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.0];
}

-(void)showUploadingMaskState
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.dimBackground = YES;
    
    HUD.labelText = @"正在保存...";
    
    [HUD show:YES];
}

-(void)closeHud
{
    if(HUD)
    {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

-(void)syncUserInfo
{
    NSString* userId = [Utils loadDefaultConfigWith:CONFIG_INFO_USERID_KEY];
    UserDataModel* curUser = nil;
    for(UserDataModel* u in [CBSiDataManager sharedDataManager].allContactsArray)
    {
        if([u.user_id isEqualToString:userId])
        {
            curUser = u;
            break;
        }
    }
    
    switch (self.modifyType) {
        case EMODIFY_TYPE_MAIL:
            curUser.user_email = self.modifyTextField.text;
            break;
        case EMODITY_TYPE_FIXED_TEL:
            curUser.user_tel = self.modifyTextField.text;
            break;
        case EMODITY_TYPE_TEL_SUB:
            curUser.user_tel_sub = self.modifyTextField.text;
            break;
        default:
            break;
    }
}
@end
