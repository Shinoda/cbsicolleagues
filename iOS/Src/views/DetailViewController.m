//
//  DetailViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/13.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "DetailViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "PortraitImageHandler.h"
#import "AppDelegate.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize detailUser;
@synthesize detailNameLabel, detailPortrait, sectorNameLabel, positionLabel;

@synthesize phoneLabel, telLabel, mailLabel, sectorLabel;
@synthesize phoneBtn, telBtn, mailBtn;
@synthesize popShareBg;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:HEXCOLOR(0x3b3b43)];
    [self initNavigationBar];
    [self buildView];
}

- (void)initNavigationBar{
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    
    [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn setFrame:CGRectMake(0, 2, 28, 28)];
    
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_nor.png"] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_press.png"] forState:UIControlStateSelected];
    
    [leftBtn addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    
    [item setLeftBarButtonItem:leftButton];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setFrame:CGRectMake(0, 2, 28, 28)];
    [rightBtn setImage:[UIImage imageNamed:@"share_normal.png"] forState:UIControlStateNormal];
    [rightBtn setImage:[UIImage imageNamed:@"share_press.png"] forState:UIControlStateSelected];
    [rightBtn addTarget:self action:@selector(sharedInfo:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [item setRightBarButtonItem:rightItem];
    
    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)Back:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)sharedInfo:(UIButton*)sender
{
    [self secondShareMethod];
    return;
    NSString *imagePath = (NSString*)[[NSBundle mainBundle] pathForResource:@"ShareSDK" ofType:@"png"];
    /*
     "联系人："+curContact.getUser_name()
     +"\n手机："+curContact.getUser_phone()
     +"\n邮件："+curContact.getUser_email()
     +"\n座机号："+curContact.getUser_tel()
     +"\n分机："+curContact.getUser_tel_sub()
     */
    NSString* content = [NSString stringWithFormat:@"联系人：%@\n手机：%@",
                         detailUser.user_name,
                         detailUser.user_phone];
    if(detailUser.user_email.length > 0)
        content = [content stringByAppendingFormat:@"\n邮件：%@", detailUser.user_email];
    if(detailUser.user_tel.length > 0)
        content = [content stringByAppendingFormat:@"\n固话：%@", detailUser.user_tel];
    if(detailUser.user_tel_sub.length > 0)
        content = [content stringByAppendingFormat:@"\n分机：%@", detailUser.user_tel_sub];
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:content//@"分享内容"
                                       defaultContent:@"测试一下"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"云同事"
                                                  url:@"http://www.cbsi.com.cn"
                                          description:@"CBSi shared!!!"
                                            mediaType:SSPublishContentMediaTypeText];
    
    //定制微信好友信息
    [publishContent addWeixinSessionUnitWithType:INHERIT_VALUE
                                         content:INHERIT_VALUE
                                           title:NSLocalizedString(@"TEXT_HELLO_WECHAT_SESSION", @"Hello 微信好友!")
                                             url:INHERIT_VALUE
                                      thumbImage:[ShareSDK imageWithUrl:@"http://img1.bdstatic.com/img/image/67037d3d539b6003af38f5c4c4f372ac65c1038b63f.jpg"]
                                           image:INHERIT_VALUE
                                    musicFileUrl:nil
                                         extInfo:nil
                                        fileData:nil
                                    emoticonData:nil];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
}

-(void)secondShareMethod
{
    popShareBg = [[UIControl alloc] initWithFrame:self.view.frame];
    [self.view addSubview:popShareBg];
    [popShareBg addTarget:self action:@selector(hideShareView) forControlEvents:UIControlEventTouchUpInside];
    [popShareBg setBackgroundColor:[UIColor blackColor]];
    popShareBg.alpha = 0.4;
    
    shareView = [[SharePopView alloc] initWithFrame:CGRectMake(0, CBS_SCREEN_HEIGHT, CBS_SCREEN_WIDTH, SHARE_VIEW_HEIGHT)];
    [self.view addSubview:shareView];
    
    [shareView.cancelBtn addTarget:self action:@selector(hideShareView) forControlEvents:UIControlEventTouchUpInside];
    [shareView.weixinBtn addTarget:self action:@selector(goToShare:) forControlEvents:UIControlEventTouchUpInside];
    [shareView.qqBtn addTarget:self action:@selector(goToShare:) forControlEvents:UIControlEventTouchUpInside];
    [shareView.mailBtn addTarget:self action:@selector(goToShare:) forControlEvents:UIControlEventTouchUpInside];
    [shareView.smsBtn addTarget:self action:@selector(goToShare:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView animateWithDuration:0.3 animations:^{
        shareView.frame = CGRectMake(0, CBS_SCREEN_HEIGHT-SHARE_VIEW_HEIGHT, CBS_SCREEN_WIDTH, SHARE_VIEW_HEIGHT);
    }];
}

-(void)hideShareView
{
    [UIView animateWithDuration:0.3 animations:^{
        shareView.frame = CGRectMake(0, CBS_SCREEN_HEIGHT, CBS_SCREEN_WIDTH, SHARE_VIEW_HEIGHT);
    } completion:^(BOOL finished) {
        [popShareBg removeFromSuperview];
        popShareBg = nil;
        [shareView removeFromSuperview];
        shareView = nil;
    }];
}

-(void)goToShare:(UIButton*)sender
{
    ShareType sType;
    switch (sender.tag) {
        case 100:
            sType = ShareTypeWeixiSession;
            break;
        case 101:
            sType = ShareTypeQQ;
            break;
        case 102:
            sType = ShareTypeMail;
            break;
        case 103:
            sType = ShareTypeSMS;
            break;
        default:
            break;
    }
    
    NSString* content = [NSString stringWithFormat:@"联系人：%@\n手机：%@",
                         detailUser.user_name,
                         detailUser.user_phone];
    if(detailUser.user_email.length > 0)
        content = [content stringByAppendingFormat:@"\n邮件：%@", detailUser.user_email];
    if(detailUser.user_tel.length > 0)
        content = [content stringByAppendingFormat:@"\n固话：%@", detailUser.user_tel];
    if(detailUser.user_tel_sub.length > 0)
        content = [content stringByAppendingFormat:@"\n分机：%@", detailUser.user_tel_sub];
    
    NSString *imagePath = (NSString*)[[NSBundle mainBundle] pathForResource:@"ShareSDK" ofType:@"png"];
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:content//@"分享内容"
                                       defaultContent:@"测试一下"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"云同事"
                                                  url:@"http://www.cbsi.com.cn"
                                          description:@"CBSi shared!!!"
                                            mediaType:SSPublishContentMediaTypeText];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    [ShareSDK showShareViewWithType:sType container:container content:publishContent statusBarTips:YES authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        
        if (state == SSResponseStateSuccess)
        {
            NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
        }
        else if (state == SSResponseStateFail)
        {
            NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
        }
    }];
    [self hideShareView];
}

-(void)buildView
{
    [self.mailBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.mailBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    [self.telBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.telBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    [self.phoneBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.phoneBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    
    self.detailNameLabel.text = detailUser.user_name;
    self.sectorNameLabel.text = detailUser.departmentName;
    self.positionLabel.text = detailUser.user_position;
    
    self.phoneLabel.text = detailUser.user_phone;
    self.telLabel.text = [NSString stringWithFormat:@"%@ (分机：%@)",detailUser.user_tel, detailUser.user_tel_sub];
    self.mailLabel.text = detailUser.user_email;
    self.sectorLabel.text = detailUser.departmentName;
    
    [[PortraitImageHandler sharedPIHandler] getPortraitImageWithUserId:detailUser.user_id sectorId:detailUser.sector_id complete:^(NSString *userId, UIImage *portrait) {
        if(portrait)
        {
            [self.detailPortrait setImage:portrait];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)writeSMS:(id)sender {
    //    The MFMessageComposeViewController class is only available in iPhone OS 4.0 or later.
    //    So, we must verify the existence of the above class and log an error message for devices
    //        running earlier versions of the iPhone OS. Set feedbackMsg if device doesn’t support
    //        MFMessageComposeViewController API.
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    
    if (messageClass != nil) {
        // Check whether the current device is configured for sending SMS messages
        if ([messageClass canSendText]) {
            [self displaySMSComposerSheet];
        }
        else {
            [self alertWithMessage:@"设备没有短信功能"];
        }
    }
    else {
        [self alertWithMessage:@"iOS版本过低,iOS4.0以上才支持程序内发送短信"];
    }
}

- (IBAction)phoneCall:(id)sender {
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.detailUser.user_phone];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (IBAction)telephoneCalled:(id)sender {
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.detailUser.user_tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (IBAction)writeMail:(id)sender {
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (!mailClass) {
        [self alertWithMessage:@"当前系统版本不支持应用内发送邮件功能，您可以使用mailto方法代替"];
        return;
    }
    if (![mailClass canSendMail]) {
        [self alertWithMessage:@"用户没有设置邮件账户"];
        return;
    }
    [self displayMailPicker];
}


-(void)displaySMSComposerSheet
{
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.recipients = [NSArray arrayWithObject:detailUser.user_phone];

//    [self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    
    switch (result)
    {
        case MessageComposeResultCancelled:
            NSLog(@"Result: SMS sending canceled");
            break;
        case MessageComposeResultSent:
            NSLog(@"Result: SMS sent");
            break;
        case MessageComposeResultFailed:
        {
            [self alertWithMessage:@"短信发送失败"];
        }
            break;
        default:
            NSLog(@"Result: SMS not sent");
            break;
    }
//    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//调出邮件发送窗口
- (void)displayMailPicker
{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    
    //设置主题
    [mailPicker setSubject: @""];
    //添加收件人
    NSArray *toRecipients = [NSArray arrayWithObject: self.detailUser.user_email];
    [mailPicker setToRecipients: toRecipients];
    //添加抄送
//    NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil];
//    [mailPicker setCcRecipients:ccRecipients];
    //添加密送
//    NSArray *bccRecipients = [NSArray arrayWithObjects:@"fourth@example.com", nil];
//    [mailPicker setBccRecipients:bccRecipients];
    
    // 添加一张图片
//    UIImage *addPic = [UIImage imageNamed: @"Icon@2x.png"];
//    NSData *imageData = UIImagePNGRepresentation(addPic);            // png
//    [mailPicker addAttachmentData: imageData mimeType: @"" fileName: @"Icon.png"];
    
    //添加一个pdf附件
//    NSString *file = [self fullBundlePathFromRelativePath:@"filename.pdf"];
//    NSData *pdf = [NSData dataWithContentsOfFile:file];
//    [mailPicker addAttachmentData: pdf mimeType: @"" fileName: @"filename.pdf"];
    
//    NSString *emailBody = @"<font color='red'>eMail</font> 正文";
//    [mailPicker setMessageBody:emailBody isHTML:YES];
//    [self presentModalViewController: mailPicker animated:YES];
    [self presentViewController:mailPicker animated:YES completion:^{
        
    }];
}

#pragma mark - 实现 MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    //关闭邮件发送窗口
//    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    NSString *msg;
    switch (result) {
        case MFMailComposeResultCancelled:
            msg = @"取消编辑邮件";
            break;
        case MFMailComposeResultSaved:
            msg = @"成功保存邮件";
            break;
        case MFMailComposeResultSent:
            msg = @"用户点击发送，将邮件放到队列中，还没发送";
            break;
        case MFMailComposeResultFailed:
            msg = @"用户试图保存或者发送邮件失败";
            break;
        default:
            msg = @"";
            break;
    }
    if(result == MFMailComposeResultSaved || result == MFMailComposeResultFailed)
    {
        [self alertWithMessage:msg];
    }
}

-(void)alertWithMessage:(NSString*)msg
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
    [alert show];
}
@end
