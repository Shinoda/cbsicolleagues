//
//  PersonalViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/17.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "PersonalViewController.h"
#import "UserDAO.h"
#import "Utils.h"
#import "QuartzCore/QuartzCore.h"
#import "AppDelegate.h"
#import "SettingViewController.h"
#import "ModifyInfoViewController.h"
#import "CBSiDataManager.h"
#import "CBSiHttpConnection.h"
#import "PortraitImageHandler.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>

@interface PersonalViewController ()

@end

@implementation PersonalViewController

@synthesize phoneLabel, mailLabel, telLabel, telsubLabel, nameLabel, sectorPosLabel, portraitImgView, portraitBgImgView;
@synthesize phoneBtn, mailBtn, telBtn,telSubBtn, settingBtn,exitBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBar];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshViewByData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)initNavigationBar{
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    
    [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn setFrame:CGRectMake(0, 2, 28, 28)];
    
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_nor.png"] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_press.png"] forState:UIControlStateSelected];
    
    [leftBtn addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    
    [item setLeftBarButtonItem:leftButton];
//    [item setTitle:@""];
    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)refreshViewByData
{
    [self.mailBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.mailBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    [self.telBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.telBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    [self.telSubBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.telSubBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    [self.settingBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x53545f)) forState:UIControlStateNormal];
    [self.settingBtn setBackgroundImage:imageWithColor(HEXCOLOR(0x42434c)) forState:UIControlStateHighlighted];
    
    [self.exitBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xf06658)) forState:UIControlStateNormal];
    [self.exitBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xeb5647)) forState:UIControlStateHighlighted];
//    [portraitImgView setImage:[UIImage imageNamed:@"photo_test"]];
    float f = 39.0f;
    portraitImgView.layer.cornerRadius = f;
    portraitImgView.layer.masksToBounds = YES;
    portraitImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    portraitImgView.layer.borderWidth = 2.0f;
    
    UserDataModel* cUser = [CBSiDataManager sharedDataManager].currentUser;
    if(cUser==nil)
    {
        [[CBSiDataManager sharedDataManager] reloadCurrentUser];
        cUser = [CBSiDataManager sharedDataManager].currentUser;
    }
    if(cUser == nil)
    {
        [self Back:nil];
        return;
    }
    self.nameLabel.text = cUser.user_name;
    self.sectorPosLabel.text = [NSString stringWithFormat:@"%@    %@",cUser.departmentName, cUser.user_position];
    self.phoneLabel.text = cUser.user_phone;
    self.telLabel.text = cUser.user_tel;
    self.telsubLabel.text = cUser.user_tel_sub;
    self.mailLabel.text = cUser.user_email;
    [portraitBgImgView setFramesCount:20];
    [portraitBgImgView setBlurAmount:0.7];
    
    [[PortraitImageHandler sharedPIHandler] getPortraitImageWithUserId:cUser.user_id
                                                              sectorId:cUser.sector_id
                                                              complete:^(NSString *userId, UIImage *portrait) {
        if([cUser.user_id isEqualToString:userId] && portrait!=nil)
        {
            [portraitImgView setImage:portrait];
            [self refreshBackgroundPortrait:portrait];
        }
    }];
}

-(void)Back:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
}

- (IBAction)modifyAction:(UIButton*)sender {
    if(/*sender.tag == 101 ||*/ sender.tag == 102 || sender.tag == 103 || sender.tag == 104)
    {
        ModifyInfoViewController* mVC = [[ModifyInfoViewController alloc] init];
        mVC.modifyType = (KModifyType)sender.tag;
        [self.navigationController pushViewController:mVC animated:YES];
    }
    else if (sender.tag == 105)
    {
        SettingViewController *setVC = [[SettingViewController alloc] init];
        [self.navigationController pushViewController:setVC animated:YES];
    }
}

- (IBAction)viewSelected:(UIButton*)sender {
    if(sender.tag == 101)
    {
//        NSLog(@"phoneBtn has click");
    }
}

- (IBAction)logoutAction:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:^{
//        AppDelegate* cbsiApp = (AppDelegate*)[UIApplication sharedApplication].delegate;
//        [cbsiApp initLoginView];
//    }];
    AppDelegate* cbsiApp = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [cbsiApp initLoginView];
}

- (IBAction)portraitChangeAction:(id)sender {
//    UIActionSheet* actionSheet = [[UIActionSheet alloc]
//                                  initWithTitle:nil
//                                  delegate:self
//                                  cancelButtonTitle:@"取消"
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"照相机",@"本地相簿",nil];
//    [actionSheet showInView:self.view];
    if(popView == nil)
    {
        popView = [[CameraPopView alloc] initWithParentView:self.view];
        [popView.cameraBtn addTarget:self action:@selector(openCamera) forControlEvents:UIControlEventTouchUpInside];
        [popView.albumBtn addTarget:self action:@selector(openAlbum) forControlEvents:UIControlEventTouchUpInside];
    }
    [popView showPopView];
}

-(void)openCamera
{
    [popView hidePopView];
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        
//        NSLog(@"相机权限受限");
        [Utils showTips:@"请在系统设置中\n打开“相机”的访问权限！" view:self.view];
        return;
    }
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    //            [self presentModalViewController:imagePicker animated:YES];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)openAlbum
{
    [popView hidePopView];
    ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
    if(authStatus == ALAuthorizationStatusRestricted || authStatus == ALAuthorizationStatusDenied){
        
        //        NSLog(@"相机权限受限");
        [Utils showTips:@"请在系统设置中\n打开“照片”的访问权限！" view:self.view];
        return;
    }
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //            [self presentModalViewController:imagePicker animated:YES];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex = [%d]", (int)buttonIndex);
    switch (buttonIndex) {
        case 0://照相机
        {
            [self openCamera];
        }
            break;
        case 1://本地相簿
        {
            [self openAlbum];
        }
            break;
        
        default:
            break;
    }
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    [picker dismissViewControllerAnimated:YES completion:nil];
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        portraitImg = [UIImage imageWithData:[Utils compressedImageAndScalingSize:portraitImg targetSize:CGSizeMake(240, 240) maxDataLen:10*1024]];
        [self uploadPortraitImage:portraitImg];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //    [picker dismissModalViewControllerAnimated:YES];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Upload Image
-(void)uploadPortraitImage:(UIImage*)image
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[CBSiDataManager sharedDataManager].currentUser.user_id forKey:@"userid"];
    NSString* imgBase64Code = [Utils base64EncodedStringFrom:UIImageJPEGRepresentation(image,1.0f)];
    [params setObject:imgBase64Code forKey:@"photoContent"];
    [params setObject:@"head" forKey:@"photoName"];
    
    [Utils showLoadingTips:@"头像上传中……" view:self.view];
    
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_UPDALOAD_PORTRAIT completion:^(NSMutableDictionary *responseContent) {
        [Utils closeHud];
        NSLog(@"uploadPortraitImage connected success: %@",responseContent);
        if([responseContent isKindOfClass:[NSDictionary class]])
        {
            [portraitImgView setImage:image];
            [self refreshBackgroundPortrait:image];
            UserDataModel* cUser = [CBSiDataManager sharedDataManager].currentUser;
            [[PortraitImageHandler sharedPIHandler] removePortraitWithUserId:cUser.user_id sectorId:cUser.sector_id];
            [[PortraitImageHandler sharedPIHandler] savePortraitToCacheWithUserId:cUser.user_id sectorId:cUser.sector_id portrait:image];
            
//            NSNotification * notiObj = [[NSNotification alloc] init];
//            [notiObj object];
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationUpdateContactCell object:cUser.user_id];
        }
    } error:^(NSString *errorInfo) {
        [Utils closeHud];
        NSLog(@"uploadPortraitImage connected error: %@",errorInfo);
    }];
}

-(void)refreshBackgroundPortrait:(UIImage*)image
{
    [portraitBgImgView setBaseImage:image];

    [portraitBgImgView generateBlurFramesWithCompletion:^{
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//        });
        // Blur with duration, also has a version w/ a callback.
        [portraitBgImgView blurInAnimationWithDuration:0.25f];
    }];
}
@end
