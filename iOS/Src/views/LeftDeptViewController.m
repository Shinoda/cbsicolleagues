//
//  LeftDeptViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/2.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "LeftDeptViewController.h"
#import "CBSiHttpConnection.h"
#import "SectorDataModel.h"
#import "SectorItemTableViewCell.h"
#import "UIViewController+MMDrawerController.h"
#import "ContactListViewController.h"
#import "SectorDAO.h"
#import "PersonalViewController.h"
#import "Utils.h"
#import "UserDAO.h"
#import "CBSiDataManager.h"

@interface LeftDeptViewController ()
@property(strong,nonatomic) NSMutableArray* allSectorArray;
//@property(strong,nonatomic) NSArray *displaySectorArray;
@end

@implementation LeftDeptViewController
@synthesize allSectorArray;
@synthesize corporationBtn, corporationNameLabel;
@synthesize personalLabel;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.view setBackgroundColor:[UIColor yellowColor]];
    [corporationNameLabel setTextColor:HEXCOLOR(0x6B424C)];
}

-(void)updateCorporationName:(NSString *)corpName
{
    corporationNameLabel.text = corpName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    return self.allSectorArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *indentifier = @"sectorItemCell";
    
    SectorDataModel* sector = [self.allSectorArray objectAtIndex:indexPath.row];
    
    
    SectorItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil){
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"SectorItemTableViewCell" owner:self options:nil];
        cell = [arr objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if(sector)
    {
        cell.sectorNameLabel.text = [NSString stringWithFormat:@"%@%@",
                                 (sector.sLevel<=1?@"":(sector.sLevel==2?@"      ":@"            ")),
                                 sector.sector_name];
    }
    return cell;
    
}



/*---------------------------------------
 cell高度默认为50
 --------------------------------------- */
//-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
//{
//    return 50;
//}

/*---------------------------------------
 处理cell选中事件，需要自定义的部分
 --------------------------------------- */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    currentSelectIndex = indexPath;

    [corporationNameLabel setTextColor:HEXCOLOR(0xffffff)];
    ContactListViewController *contactVC = (ContactListViewController*)self.mm_drawerController.centerViewController;
    [contactVC.contactListTableView setContentOffset:CGPointMake(0, 0)];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        SectorDataModel *sector = [self.allSectorArray objectAtIndex:indexPath.row];
        contactVC.titleNameLabel.text = sector.sector_name;
        [contactVC refreshAllSectorContactList:sector.sector_id];
    }];
}


-(SectorDataModel*)getParentSector:(SectorDataModel*)sector
{
    SectorDataModel* parentS = nil;
    if(sector.parent_sector_id == nil || sector.parent_sector_id.intValue == 0)
    {
        return nil;
    }
    for(SectorDataModel *s in self.allSectorArray)
    {
        if([s.sector_id isEqualToString:sector.parent_sector_id])
        {
            parentS = s;
            break;
        }
    }
    return parentS;
}

-(void)refreshDeptView
{
//    NSArray* priorityArr = [currentUser.priority_sector componentsSeparatedByString:@","];
    
    self.allSectorArray = [CBSiDataManager sharedDataManager].allSectorsArray;//[SectorDAO getSectorsArrayByPriority:currentUser.priority_sector];//[SectorDAO loadAllSectorFromDB];
    
    [self.allSectorsTableView reloadData];
}

- (IBAction)openAllContact:(UIButton*)sender {

    [corporationNameLabel setTextColor:HEXCOLOR(0x6B424C)];
    
    SectorItemTableViewCell *cell = (SectorItemTableViewCell*)[self.allSectorsTableView cellForRowAtIndexPath:currentSelectIndex];
    [cell.sectorNameLabel setTextColor:HEXCOLOR(0xffffff)];
    ContactListViewController *contactVC = (ContactListViewController*)self.mm_drawerController.centerViewController;
    [contactVC.contactListTableView setContentOffset:CGPointMake(0, 0)];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        [contactVC.titleNameLabel setText:[Utils loadDefaultConfigWith:CONFIG_INFO_CORPNAME_KEY]];
        [contactVC refreshAllContactsList];
    }];
}

- (IBAction)openProfileView:(id)sender {
    [self.personalLabel setTextColor:HEXCOLOR(0xffffff)];
    PersonalViewController* pVC = [[PersonalViewController alloc] init];
    [self.mm_drawerController.navigationController pushViewController:pVC animated:YES];
//    [self.mm_drawerController.navigationController presentViewController:pVC animated:YES completion:^{
//        
//    }];
}
- (IBAction)touchProfileBtn:(id)sender {
    [self.personalLabel setTextColor:HEXCOLOR(0x6a424c)];
}
- (IBAction)drag:(id)sender {
    [self.personalLabel setTextColor:HEXCOLOR(0xffffff)];
}

@end
