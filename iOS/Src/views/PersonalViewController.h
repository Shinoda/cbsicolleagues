//
//  PersonalViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/17.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANBlurredImageView.h"
#import "CameraPopView.h"

@interface PersonalViewController : UIViewController<UIActionSheetDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    CameraPopView *popView;
}

@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;
@property (strong, nonatomic) IBOutlet UILabel *mailLabel;
@property (strong, nonatomic) IBOutlet UILabel *telLabel;
@property (strong, nonatomic) IBOutlet UILabel *telsubLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectorPosLabel;

@property (strong, nonatomic) IBOutlet UIButton *mailBtn;
@property (strong, nonatomic) IBOutlet UIButton *telBtn;
@property (strong, nonatomic) IBOutlet UIButton *telSubBtn;
@property (strong, nonatomic) IBOutlet UIButton *phoneBtn;
@property (strong, nonatomic) IBOutlet UIButton *settingBtn;
@property (strong, nonatomic) IBOutlet UIButton *exitBtn;
@property (strong, nonatomic) IBOutlet UIImageView *portraitImgView;
@property (strong, nonatomic) IBOutlet ANBlurredImageView *portraitBgImgView;

@end
