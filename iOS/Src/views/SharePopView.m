//
//  SharePopView.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/4/10.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "SharePopView.h"

#define SHARE_BTN_LABEL_FONT_SIZE 16.0
#define SHARE_BTN_HOR_DISTANCE  46.0
#define SHARE_BTN_SIZE_WIDTH    59.0

@implementation SharePopView
@synthesize weixinBtn, qqBtn, mailBtn, smsBtn, cancelBtn;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self buildView];
    }
    return self;
}

-(void)buildView
{
    self.backgroundColor = HEXACOLOR(0xffffff, 0.9);
    CGRect btnFrame = CGRectMake(0, 0, SHARE_BTN_SIZE_WIDTH, SHARE_BTN_SIZE_WIDTH);
    weixinBtn = [[UIButton alloc] initWithFrame:btnFrame];
    mailBtn = [[UIButton alloc] initWithFrame:btnFrame];
    smsBtn = [[UIButton alloc] initWithFrame:btnFrame];
    qqBtn = [[UIButton alloc] initWithFrame:btnFrame];
    [self addSubview:weixinBtn];
    [self addSubview:qqBtn];
    [self addSubview:mailBtn];
    [self addSubview:smsBtn];
    if(NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1)
    {
        [weixinBtn setImage:[UIImage imageNamed:@"share_btn_wx@3x.png"] forState:UIControlStateNormal];
        [qqBtn setImage:[UIImage imageNamed:@"share_btn_qq@3x.png"] forState:UIControlStateNormal];
        [mailBtn setImage:[UIImage imageNamed:@"share_btn_mail@3x.png"] forState:UIControlStateNormal];
        [smsBtn setImage:[UIImage imageNamed:@"share_btn_sms@3x.png"] forState:UIControlStateNormal];
    }
    else
    {
        [weixinBtn setImage:[UIImage imageNamed:@"share_btn_wx.png"] forState:UIControlStateNormal];
        [qqBtn setImage:[UIImage imageNamed:@"share_btn_qq.png"] forState:UIControlStateNormal];
        [mailBtn setImage:[UIImage imageNamed:@"share_btn_mail.png"] forState:UIControlStateNormal];
        [smsBtn setImage:[UIImage imageNamed:@"share_btn_sms.png"] forState:UIControlStateNormal];
    }
    
    qqBtn.center = CGPointMake(self.center.x, 14 + btnFrame.size.height/2);
    weixinBtn.frame = CGRectMake(28, CGRectGetMinY(qqBtn.frame), SHARE_BTN_SIZE_WIDTH, SHARE_BTN_SIZE_WIDTH);
    mailBtn.frame = CGRectMake(CGRectGetWidth(self.frame)-28-SHARE_BTN_SIZE_WIDTH, CGRectGetMinY(qqBtn.frame), SHARE_BTN_SIZE_WIDTH, SHARE_BTN_SIZE_WIDTH);
    smsBtn.frame = CGRectMake(28, CGRectGetMaxY(weixinBtn.frame)+2*SHARE_BTN_LABEL_FONT_SIZE+5, SHARE_BTN_SIZE_WIDTH, SHARE_BTN_SIZE_WIDTH);
    
    
    weixinBtn.tag = 100;
    qqBtn.tag = 101;
    mailBtn.tag = 102;
    smsBtn.tag = 103;
    
    //layout text 4 btn----<Begin>--//
    UILabel* weixinLab = [self generateLabel:@"微信"];
    weixinLab.center = CGPointMake(weixinBtn.center.x, weixinBtn.center.y+SHARE_BTN_SIZE_WIDTH/2+SHARE_BTN_LABEL_FONT_SIZE);
    [self addSubview:weixinLab];
    
    UILabel *qqLab = [self generateLabel:@"QQ "];
    qqLab.center = CGPointMake(qqBtn.center.x, qqBtn.center.y+SHARE_BTN_SIZE_WIDTH/2+SHARE_BTN_LABEL_FONT_SIZE);
    [self addSubview:qqLab];
    
    UILabel *mailLab = [self generateLabel:@"邮件"];
    mailLab.center = CGPointMake(mailBtn.center.x, mailBtn.center.y+SHARE_BTN_SIZE_WIDTH/2+SHARE_BTN_LABEL_FONT_SIZE);
    [self addSubview:mailLab];
    
    UILabel *smsLab = [self generateLabel:@"短信"];
    smsLab.center = CGPointMake(smsBtn.center.x, smsBtn.center.y+SHARE_BTN_SIZE_WIDTH/2+SHARE_BTN_LABEL_FONT_SIZE);
    [self addSubview:smsLab];
    //layout text 4 btn----<End>--//
    
    cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH-56, 51)];
    cancelBtn.center = CGPointMake(self.center.x, CGRectGetHeight(self.frame)-(51/2)-5);
    [cancelBtn setTitle:@"取  消" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [cancelBtn setTitleColor:HEXCOLOR(0x656565) forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:imageWithColor([UIColor clearColor]) forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xd0d0d0)) forState:UIControlStateHighlighted];
    [cancelBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xd0d0d0)) forState:UIControlStateSelected];
    [self addSubview:cancelBtn];
    
    UIImageView *lineSplit = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(cancelBtn.frame), CGRectGetMinY(cancelBtn.frame)-3, CGRectGetWidth(cancelBtn.frame), 1)];
    [lineSplit setBackgroundColor:HEXCOLOR(0xacadb2)];
    [self addSubview:lineSplit];
}

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        [self buildView];
    }
    return self;
}

-(UILabel*)generateLabel:(NSString*)t
{
    UIFont *labFont = [UIFont systemFontOfSize:SHARE_BTN_LABEL_FONT_SIZE];
    CGSize labelSize = [t sizeWithFont:[UIFont boldSystemFontOfSize:SHARE_BTN_LABEL_FONT_SIZE]
                       constrainedToSize:CGSizeMake(100, 100)
                           lineBreakMode:NSLineBreakByCharWrapping];
    UILabel* lab = [[UILabel alloc] init];
    lab.frame = CGRectMake(0, 0, labelSize.width, labelSize.height);
    [lab setBackgroundColor:[UIColor clearColor]];
    [lab setFont:labFont];
    [lab setText:t];
    [lab setTextColor:HEXCOLOR(0x656565)];
    [lab setTextAlignment:NSTextAlignmentCenter];
    return lab;
}
@end
