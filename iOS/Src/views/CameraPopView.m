//
//  CameraPopView.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/4/13.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "CameraPopView.h"

#define CAMERAPOP_BTN_HEIGHT 52.0f
#define CAMERAPOP_VIEW_HEIGHT 160.0f
#define CAMERAPOP_FRAME CGRectMake(0, CBS_SCREEN_HEIGHT, CBS_SCREEN_WIDTH, CAMERAPOP_VIEW_HEIGHT)

@implementation CameraPopView
@synthesize cameraBtn, albumBtn, cancelBtn;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithParentView:(UIView *)pView
{
    return [self initWithFrame:CAMERAPOP_FRAME pView:pView];
}

- (instancetype)initWithFrame:(CGRect)frame pView:(UIView *)pView
{
    self = [super initWithFrame:frame];
    if (self) {
        parentView = pView;
        [self initView];
    }
    return self;
}

-(void)initView
{
    popBg = [[UIControl alloc] initWithFrame:parentView.frame];
    [parentView addSubview:popBg];
    [popBg addTarget:self action:@selector(hidePopView) forControlEvents:UIControlEventTouchUpInside];
    [popBg setBackgroundColor:[UIColor blackColor]];
    popBg.alpha = 0.4;
    [popBg setHidden:YES];
    
    self.backgroundColor = [UIColor clearColor];
    [parentView addSubview:self];
    
    cameraBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, CAMERAPOP_BTN_HEIGHT)];
    [cameraBtn setBackgroundColor:HEXACOLOR(0xffffff, 0.9)];
    [cameraBtn setTitle:@"拍照" forState:UIControlStateNormal];
    cameraBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [cameraBtn setTitleColor:HEXCOLOR(0x656565) forState:UIControlStateNormal];
    [cameraBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xd0d0d0)) forState:UIControlStateHighlighted];
    [self addSubview:cameraBtn];
    
    albumBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CAMERAPOP_BTN_HEIGHT+1, CBS_SCREEN_WIDTH, CAMERAPOP_BTN_HEIGHT)];
    [albumBtn setBackgroundColor:HEXACOLOR(0xffffff, 0.9)];
    [albumBtn setTitle:@"从手机相册选择" forState:UIControlStateNormal];
    albumBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [albumBtn setTitleColor:HEXCOLOR(0x656565) forState:UIControlStateNormal];
    [albumBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xd0d0d0)) forState:UIControlStateHighlighted];
    [self addSubview:albumBtn];
    
    cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 2*CAMERAPOP_BTN_HEIGHT+7, CBS_SCREEN_WIDTH,CAMERAPOP_BTN_HEIGHT)];
    [cancelBtn addTarget:self action:@selector(hidePopView) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setBackgroundColor:HEXACOLOR(0xffffff, 0.9)];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [cancelBtn setTitleColor:HEXCOLOR(0x656565) forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:imageWithColor(HEXCOLOR(0xd0d0d0)) forState:UIControlStateHighlighted];
    [self addSubview:cancelBtn];
}

-(void)showPopView
{
    [popBg setHidden:NO];
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, CBS_SCREEN_HEIGHT-CAMERAPOP_VIEW_HEIGHT, CBS_SCREEN_WIDTH, CAMERAPOP_VIEW_HEIGHT);
    }];
}

-(void)hidePopView
{
    [popBg setHidden:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = CGRectMake(0, CBS_SCREEN_HEIGHT, CBS_SCREEN_WIDTH, CAMERAPOP_VIEW_HEIGHT);
    } completion:^(BOOL finished) {
        
    }];
}
@end
