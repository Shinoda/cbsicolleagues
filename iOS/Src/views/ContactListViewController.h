//
//  ContactListViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/2.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class UserDataModel;

@interface ContactListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource
,MBProgressHUDDelegate,MFMessageComposeViewControllerDelegate>
{
    MBProgressHUD *HUD;
    UIButton* leftBtn;
    
    UserDataModel* selectedUser;
}
@property (strong, nonatomic) IBOutlet UITableView *contactListTableView;
@property(nonatomic, strong) NSMutableArray* allContactArray;
@property (nonatomic, retain) NSMutableArray *sectionHeadsKeys;
@property (strong, nonatomic) UILabel* titleNameLabel;

-(void)refreshAllContactsList;
-(void)refreshAllSectorContactList:(NSString*)sectorId;
@end
