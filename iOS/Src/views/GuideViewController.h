//
//  GuideViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/4/8.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideViewController : UIViewController

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guideTotalViewWidth;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guide1ViewWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guide2ViewWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guide3ViewWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guide2Leading;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *guide3Leading;

@end
