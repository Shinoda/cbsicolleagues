//
//  CameraPopView.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/4/13.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraPopView : UIView
{
    UIView *parentView;
    UIControl *popBg;
}
@property (strong, nonatomic) UIButton* cameraBtn;
@property (strong, nonatomic) UIButton* albumBtn;
@property (strong, nonatomic) UIButton* cancelBtn;

-(instancetype)initWithParentView:(UIView*)pView;

-(void)showPopView;
-(void)hidePopView;
@end
