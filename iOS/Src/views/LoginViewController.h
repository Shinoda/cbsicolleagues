//
//  LoginViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/6.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,
MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    dispatch_source_t fadingTimer;
    BOOL isStartingLogin;
    float keyboardHeight;
}

@property (strong, nonatomic) IBOutlet UITextField *verifyCodeTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneNoTextField;

@property (strong, nonatomic) IBOutlet UIButton *verifyCodeBtn;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;

-(void)loginToServer:(NSString*)userPhone verifyCode:(NSString*)verifyCode;
@end
