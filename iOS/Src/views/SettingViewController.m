//
//  SettingViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/19.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "SettingViewController.h"
#import "SettingTableViewCell.h"
#import "AboutViewController.h"
#import "Utils.h"
#import "CBSiHttpConnection.h"
#import "SectorDAO.h"
#import "UserDAO.h"
#import "CBSiDataManager.h"

@interface SettingViewController ()

@end

@implementation SettingViewController
@synthesize optionTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBar];
    [optionTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initNavigationBar{
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    
    [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn setFrame:CGRectMake(0, 2, 28, 28)];
    
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_nor.png"] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_press.png"] forState:UIControlStateSelected];
    
    [leftBtn addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    
    [item setLeftBarButtonItem:leftButton];
    //    [item setTitle:@""];
    UILabel* titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    titleLab.textColor = [UIColor whiteColor];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    titleLab.text = @"设  置";
    [item setTitleView:titleLab];
    
    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)Back:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 73;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *indentifier = @"settingItemCell";
    
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil){
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"SettingTableViewCell" owner:self options:nil];
        cell = [arr objectAtIndex:0];
//        cell.contentMode = UIViewContentModeCenter;
//        cell.contentView.frame = CGRectMake(14, 0, CBS_SCREEN_WIDTH-28, 73);
//        [cell setNeedsDisplay];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = HEXCOLOR(0x2D2C34);
        
//        if(indexPath.section == 1)
//        {
//            UIImageView* newIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"version_new.png"]];
//            [newIcon setFrame:CGRectMake(CGRectGetMaxX(cell.titleLabel.frame), CGRectGetMinY(cell
//                                                                                             .titleLabel.frame), newIcon.frame.size.width, newIcon.frame.size.height)];
//            [cell.contentView addSubview:newIcon];
//        }
    }
    
    switch (indexPath.section) {
        case 0:
            cell.titleLabel.text = @"更新通讯录";
            break;
        case 1:
//            cell.titleLabel.text = @"检查新版本";
            cell.titleLabel.text = @"关于";
            break;
//        case 2:
//            cell.titleLabel.text = @"关于";
//            break;
        default:
            break;
    }
    
    [cell setNeedsDisplay];
    return cell;
    
}

/*---------------------------------------
 处理cell选中事件，需要自定义的部分
 --------------------------------------- */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.section) {
        case 0:
            [self getSectorFromServer];
            break;
//        case 1:
//            [self showConnecttingState:@"检查新版本..."];
//            [self performSelector:@selector(showTips:) withObject:@"App已经是最新版本！" afterDelay:2.5];
//            break;
        case 1:
        {
            AboutViewController* abtVC = [[AboutViewController alloc] init];
            [self.navigationController pushViewController:abtVC animated:YES];
        }
            break;
        default:
            break;
    }
}

-(void)showTips:(NSString*)tips
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
    HUD.delegate = self;
    HUD.labelText = tips;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.0];
}

-(void)showConnecttingState:(NSString*)message
{
    [self closeHud];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.dimBackground = YES;
    
    HUD.delegate = self;
    HUD.labelText = message;
    
    [HUD show:YES];
    //----Function is uncompleted.
//    [HUD hide:YES afterDelay:2.4];
}

-(void)closeHud
{
    if(HUD)
    {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

-(void)getSectorFromServer
{
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    NSString* corpId = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
    [params setObject:corpId forKey:@"corpid"];
    [self showConnecttingState:@"下载通讯录..."];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_LOADSECTOR completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"getSectorFromServer response content: %@",responseContent);
        if([responseContent isKindOfClass:[NSArray class]])
        {
            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:(NSArray*)responseContent];
            NSMutableArray *allSectorsArr = [[NSMutableArray alloc] init];
            for(NSDictionary* d in array)
            {
                SectorDataModel* s = [[SectorDataModel alloc] initWithDict:d];
                if(s)
                    [allSectorsArr addObject:s];
            }
            [SectorDAO saveToDataBase:allSectorsArr];
            [self getContactFromServer];
            return;
        }
        
        [self closeHud];
        
    } error:^(NSString *errorInfo) {
        NSLog(@"getSectorFromServer response Failed: %@",errorInfo);
        [self showTips:@"服务器连接失败！"];
    }];
}

-(void)getContactFromServer
{
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:0];
    NSString* corpId = [Utils loadDefaultConfigWith:CONFIG_INFO_CORPID_KEY];
    [params setObject:corpId forKey:@"corpid"];
    [[CBSiHttpConnection sharedInstance] httpRequestViaPOST:params apiUrl:CBSI_SERVER_INTERFACE_LOADCONTACT completion:^(NSMutableDictionary *responseContent) {
        NSLog(@"getContactFromServer response content: %@",responseContent);
        [self closeHud];
        
        if([responseContent isKindOfClass:[NSArray class]])
        {
            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:(NSArray*)responseContent];
            NSMutableArray *allContactArray = [[NSMutableArray alloc] init];
            for(NSDictionary* d in array)
            {
                UserDataModel* u = [[UserDataModel alloc] initWithDict:d];
                if(u)
                {
                    SectorDataModel* s = [SectorDAO findSectorFromDB:u.sector_id];
                    u.departmentName = s.sector_name;
                    [allContactArray addObject:u];
                }
            }
            BOOL isSaved = [UserDAO saveToDataBase:allContactArray];
            NSLog(@"Save contacts to db: %@",isSaved?@"success":@"failed");
        
            [[CBSiDataManager sharedDataManager] reloadAllContactsData];
            [[CBSiDataManager sharedDataManager] reloadAllSectorsData];
            return;
        }
        
        
    } error:^(NSString *errorInfo) {
        NSLog(@"getContactFromServer response Failed: %@",errorInfo);
        [self showTips:@"服务器连接失败！"];
    }];
}

@end
