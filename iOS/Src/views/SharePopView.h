//
//  SharePopView.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/4/10.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SHARE_VIEW_HEIGHT   270.0

@interface SharePopView : UIView

@property (strong, nonatomic) UIButton *weixinBtn;
@property (strong, nonatomic) UIButton *qqBtn;
@property (strong, nonatomic) UIButton *mailBtn;
@property (strong, nonatomic) UIButton *smsBtn;
@property (strong, nonatomic) UIButton *cancelBtn;

@end
