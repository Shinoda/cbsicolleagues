//
//  DetailViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/13.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataModel.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SharePopView.h"

@interface DetailViewController : UIViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>
{
//    UIAlertView *exceptionAlert;
    SharePopView *shareView;
}
@property(nonatomic, strong) UserDataModel* detailUser;
@property (strong, nonatomic) IBOutlet UIImageView *detailPortrait;
@property (strong, nonatomic) IBOutlet UILabel *detailNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectorNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionLabel;


@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;
@property (strong, nonatomic) IBOutlet UILabel *telLabel;
@property (strong, nonatomic) IBOutlet UILabel *mailLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectorLabel;

@property (strong, nonatomic) IBOutlet UIButton *phoneBtn;
@property (strong, nonatomic) IBOutlet UIButton *telBtn;
@property (strong, nonatomic) IBOutlet UIButton *mailBtn;


@property (strong, nonatomic) UIControl *popShareBg;
@end
