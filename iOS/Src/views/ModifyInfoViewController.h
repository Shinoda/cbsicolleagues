//
//  ModifyInfoViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/20.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDAO.h"

typedef enum
{
    EMODIFY_TYPE_MAIL = 102,
    EMODITY_TYPE_FIXED_TEL = 103,
    EMODITY_TYPE_TEL_SUB =104
}KModifyType;

@interface ModifyInfoViewController : UIViewController
{
    UIButton* saveBtn;
    MBProgressHUD *HUD;
}

@property (strong, nonatomic) IBOutlet UITextField *modifyTextField;
@property (nonatomic, assign) KModifyType modifyType;
@property (strong , nonatomic) UserDataModel* currentUser;

@end
