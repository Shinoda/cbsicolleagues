//
//  AboutViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/20.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UILabel *networkEnv;
@property (strong, nonatomic) IBOutlet UILabel *crLabel;

@end
