//
//  AboutViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/20.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "AboutViewController.h"
#import "Utils.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize versionLabel, networkEnv, crLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBar];
    self.networkEnv.text = [NSString stringWithFormat:@"Env: %@", [CBSI_SERVER_URL substringToIndex:[CBSI_SERVER_URL rangeOfString:@"/"].location]];
    self.crLabel.text = @"智徳典康 版权所有© 2015";//[NSString stringWithFormat:@"%@ 版权所有© 2015", [Utils loadDefaultConfigWith:CONFIG_INFO_CORPNAME_KEY]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)initNavigationBar{
    
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CBS_SCREEN_WIDTH, 64)];
    
    [bar setBackgroundImage:[UIImage imageNamed:@"navigation_title.png"] forBarMetrics:UIBarMetricsDefault];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [leftBtn setFrame:CGRectMake(0, 2, 28, 28)];
    
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_nor.png"] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"navi_back_btn_press.png"] forState:UIControlStateSelected];
    
    [leftBtn addTarget:self action:@selector(Back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    
    [item setLeftBarButtonItem:leftButton];
    
    UILabel* titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    titleLab.textColor = [UIColor whiteColor];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    titleLab.text = @"关  于";
    [item setTitleView:titleLab];

    [bar pushNavigationItem:item animated:NO];
    
    [self.view addSubview:bar];
}

-(void)Back:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)changVersionDisplayAction:(UIButton*)sender {
//    if(sender.isSelected)
//    {
//        self.versionLabel.text = [NSString stringWithFormat:@"v %@",COLLEAGUE_APP_VERSION];
//        [self.networkEnv setHidden:YES];
//    }
//    else
//    {
//        self.versionLabel.text = [NSString stringWithFormat:@"build %@",COLLEAGUE_APP_BUILD_VERSION];
//        [self.networkEnv setHidden:NO];
//    }
//    sender.selected = !sender.selected;
}
@end
