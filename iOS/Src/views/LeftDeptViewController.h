//
//  LeftDeptViewController.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/2.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftDeptViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSIndexPath* currentSelectIndex;
}
@property (strong, nonatomic) IBOutlet UITableView *allSectorsTableView;
@property (strong, nonatomic) IBOutlet UIButton *corporationBtn;
@property (strong, nonatomic) IBOutlet UILabel *corporationNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *personalLabel;

-(void)refreshDeptView;
-(void)updateCorporationName:(NSString *)corpName;
@end
