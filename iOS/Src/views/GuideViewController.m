//
//  GuideViewController.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/4/8.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "GuideViewController.h"
#import "AppDelegate.h"
#import "Utils.h"

@interface GuideViewController ()

@end

@implementation GuideViewController

@synthesize guideTotalViewWidth;
@synthesize guide1ViewWidth, guide2ViewWidth, guide3ViewWidth;
@synthesize guide2Leading, guide3Leading;
//@synthesize centerY_viewOfScroll;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewConstraints
{
    [super updateViewConstraints];
    self.guideTotalViewWidth.constant = CBS_SCREEN_WIDTH * 3;
    self.guide1ViewWidth.constant = self.guide2ViewWidth.constant = self.guide3ViewWidth.constant = CBS_SCREEN_WIDTH;
    
    self.guide2Leading.constant = CBS_SCREEN_WIDTH;
    self.guide3Leading.constant = CBS_SCREEN_WIDTH *2;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)enterToLogin:(id)sender {
    AppDelegate* cbsiApp = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [cbsiApp appLaunchedByLogin];
    [Utils saveDefaultConfigWith:CONFIG_APP_IS_FIRST_RUN value:@"1"];
}

@end
