//
//  CorporationDataModel.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/12.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CorporationDataModel : NSObject
@property(nonatomic, strong) NSString *corporation_id;
@property(nonatomic, strong) NSString *corporation_name;
@property(nonatomic, strong) NSString *corporation_maintain_time;

@end
