//
//  UserDataModel.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/2/28.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "UserDataModel.h"

@implementation UserDataModel

@synthesize user_id,
user_name,
user_position,
user_phone,
user_email,
user_tel,
user_tel_sub,
sector_id,
departmentName,
priority_sector,
db_createtime,
db_updatetime,
db_delete,
pinyinKey;

-(UserDataModel*)initWithDict:(NSDictionary *)dataDict
{
    if(dataDict == nil || dataDict.count == 0)
    {
        return nil;
    }
    UserDataModel* userDM = [[UserDataModel alloc] init];
    userDM.user_id = [dataDict valueForKey:@"user_id"];
    userDM.user_name = [dataDict valueForKey:@"user_name"];
    userDM.user_position = [dataDict valueForKey:@"user_position"];
    userDM.departmentName = @"";
    userDM.user_phone = [dataDict valueForKey:@"user_phone"];
    userDM.user_email = [dataDict valueForKey:@"user_email"];
    userDM.user_tel = [dataDict valueForKey:@"user_tel"];
    userDM.user_tel_sub = [dataDict valueForKey:@"user_tel_sub"];
    userDM.sector_id = [dataDict valueForKey:@"sector_id"];
    userDM.priority_sector = [dataDict valueForKey:@"priority_sector"];
    userDM.db_createtime = [dataDict valueForKey:@"db_createtime"];
    userDM.db_updatetime = [dataDict valueForKey:@"db_updatetime"];
    userDM.db_delete = [dataDict valueForKey:@"db_delete"];
    return userDM;
}
@end
