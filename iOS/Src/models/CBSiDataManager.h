//
//  CBSiDataManager.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/19.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDAO.h"
#import "SectorDAO.h"

@interface CBSiDataManager : NSObject
@property(strong,nonatomic) NSMutableArray* allSectorsArray;
@property(strong,nonatomic) NSMutableArray* allContactsArray;
@property(strong,nonatomic) UserDataModel* currentUser;

+(CBSiDataManager*)sharedDataManager;

-(void)reloadCurrentUser;

-(void)reloadAllContactsData;

-(void)reloadAllSectorsData;

-(void)reloadAllData;
@end
