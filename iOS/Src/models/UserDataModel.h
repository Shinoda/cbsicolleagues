//
//  UserDataModel.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/2/28.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataModel : NSObject
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *user_name;
@property (nonatomic, strong) NSString *user_position;
@property (nonatomic, strong) NSString *user_phone;
@property (nonatomic, strong) NSString *user_email;
@property (nonatomic, strong) NSString *user_tel;
@property (nonatomic, strong) NSString *user_tel_sub;
@property (nonatomic, strong) NSString *sector_id;
@property (nonatomic, strong) NSString *departmentName;
@property (nonatomic, strong) NSString *priority_sector;
@property (nonatomic, strong) NSString *db_createtime;
@property (nonatomic, strong) NSString *db_updatetime;
@property (nonatomic, strong) NSString *db_delete;

@property (nonatomic, strong) NSString *pinyinKey;

-(UserDataModel*)initWithDict:(NSDictionary *)dataDict;
@end
