//
//  SectorDataModel.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/2/28.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "SectorDataModel.h"

@implementation SectorDataModel

-(SectorDataModel*)initWithDict:(NSDictionary *)dataDict
{
    if(dataDict == nil || dataDict.count == 0)
    {
        return nil;
    }
    SectorDataModel* sector = [[SectorDataModel alloc] init];
    sector.sector_id = [dataDict valueForKey:@"sector_id"];
    sector.sector_name = [dataDict valueForKey:@"sector_name"];
    sector.corporation_id = [dataDict valueForKey:@"corporation_id"];
    sector.parent_sector_id = [dataDict valueForKey:@"parent_sector_id"];
    sector.db_createtime = [dataDict valueForKey:@"db_createtime"];
    sector.db_updatetime = [dataDict valueForKey:@"db_updatetime"];
    sector.db_delete = [dataDict valueForKey:@"db_delete"];
    return sector;
}
@end
