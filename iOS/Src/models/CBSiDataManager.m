//
//  CBSiDataManager.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/19.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "CBSiDataManager.h"
#import "Utils.h"

static CBSiDataManager* pInstance;



@implementation CBSiDataManager
@synthesize allContactsArray, allSectorsArray, currentUser;
+(CBSiDataManager*)sharedDataManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(pInstance == nil)
        {
            pInstance = [[CBSiDataManager alloc] init];
            [pInstance reloadAllData];
        }
    });
    return pInstance;
}

-(void)reloadCurrentUser
{
    NSString* curUserId = [Utils loadDefaultConfigWith:CONFIG_INFO_USERID_KEY];
    if(curUserId)
    {
        currentUser = [UserDAO findUserFromDB:curUserId];
    }
    else
    {
        NSLog(@"Reload current User is Failed! Maybe the ID of login User has not saved!");
    }

}

-(void)reloadAllData
{
    [self reloadCurrentUser];
    
    [self reloadAllContactsData];
    
    [self reloadAllSectorsData];
}

-(void)reloadAllContactsData
{
    NSString* pSector = currentUser.priority_sector;
    if(pSector == nil || pSector.length==0)
    {
        pSector = currentUser.sector_id;
    }

    allContactsArray = [UserDAO loadUsersBySectorId:pSector];
}


-(void)reloadAllSectorsData
{
    NSString* pSector = currentUser.priority_sector;
    if(pSector == nil || pSector.length==0)
    {
        pSector = currentUser.sector_id;
    }
    
    allSectorsArray = [SectorDAO getSectorsArrayByPriority:pSector];
}
@end
