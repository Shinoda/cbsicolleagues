//
//  CorporationDataModel.m
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/3/12.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import "CorporationDataModel.h"

@implementation CorporationDataModel
@synthesize corporation_id,
corporation_name,
corporation_maintain_time;
@end
