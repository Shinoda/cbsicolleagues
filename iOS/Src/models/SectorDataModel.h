//
//  SectorDataModel.h
//  CBSiColleague
//
//  Created by Heq.Shinoda on 15/2/28.
//  Copyright (c) 2015年 CBSi.Ltd.Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SectorDataModel : NSObject
@property (nonatomic, strong) NSString *sector_id;
@property (nonatomic, strong) NSString *sector_name;
@property (nonatomic, strong) NSString *corporation_id;
@property (nonatomic, strong) NSString *parent_sector_id;
@property (nonatomic, strong) NSString *db_createtime;
@property (nonatomic, strong) NSString *db_updatetime;
@property (nonatomic, strong) NSString *db_delete;

@property (nonatomic, assign) NSInteger sLevel;

-(SectorDataModel*)initWithDict:(NSDictionary *)dataDict;
@end
